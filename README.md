# activeVision

#### 介绍
节卡机器人主动视觉机构

#### 软件架构
软件架构说明
active_vision中为主动视觉代码
ftservoControl为ros信息转到飞特舵机控制信息的控制代码


#### 安装教程

ros功能包，依赖于Moveit, Eigen等

#### V1.x使用说明

1.  请在使用前先打开moveit仿真以及八叉树建图结点
```
roslaunch jakazu7_moveit_test_config demo.launch

```
```
rosrun jaka_moveit_action OctomapGene
//fake octomap

```
2.  打开飞特舵机控制程序，主动视觉机构初始化
```
rosrun ftservoControl servoControl

```

3.  打开主动视觉机构
```
rosrun active_vision_control activeSwarmControl

```
相机调试
```
rosrun active_vision_control activeSwarmTest

```
4. 机器人控制：可以使用jakaPathSearching中的testRobotTraj进行控制，已经适配了接口
#### V2.x使用说明

1.  请在使用前先打开moveit仿真
```
roslaunch jakazu7_moveit_test_config demo.launch

```
2.  打开飞特舵机控制程序，主动视觉机构初始化
```
rosrun ftservoControl servoControl

```

3.  打开主动视觉机构
```
rosrun active_vision_control activeVision2main

```
4. 主动视觉仿真分析：
直接打开仿真节点，该节点会主动发送各个障碍物的位姿，并且自动运行activeVision2main。
```
roslaunch active_vision_control peopleDetectionSim.launch

```

#### 相机标定

1. 摆放好aruco标定板以及主动视觉机构舵机（一个机构一个机构测）
2. 计算好各个角度，将其填入cameraCalibration.launch中（推荐不修改，采用垂直构型）
3. aruco板子选择：可以自行选择标准的aruco板，代码的更改在这里
```
    aruco_dict = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_1000);// You can change the dictionary to yours
```
4. 在deviceCalibration.cpp中写入参数
```
    //camera_matrix = ...; // Initialize with known camera matrix
    //dist_coeffs = ...; // Initialize with known distortion coefficients
    //acuro_board_pose = ...; // Initialize with known acuro board pose
```
注意，acuro_board_pose中坐标系z轴沿着acuro板的法线方向

5. 打开launch文件，会输出估计的相机角度（请先打开realsense，节点需要读取image信息）
```
    roslaunch active_vision_control cameraCalibration.launch 
```
如果报错请查看usb口连接端口号
```
    ls /dev/tty*
```
查看所有dev端口，寻找/dev/ttyUSB*，填入下面的port中
```
    roslaunch active_vision_control cameraCalibration.launch port:=xxxx
```
6. 完成一个标定后可更换下一个机构标定

#### 项目展示
1. 功能介绍
能够基于输入的圆柱包络，计算质心并且观测质心。
2. 舵机控制开启
```
    roslaunch active_vision_control activeVisionDisp.launch
```
3. 接口介绍
- **障碍物输入接口**：*/obsState*，消息类型为*std_msgs/Float64MultiArray*，和之前项目中障碍物输入接口相同。
- **TF树**：在接收到障碍物后会一直维护。

4. 一些建议
- 上面的舵机有限位，大约为0～70度之间
- 若坐标系不准，可能是各个关节长度的问题，修改launch文件中的*l1*和*l3*，其中*l1*对应第二个舵机的轴到地面的距离，*l3*对应第二个舵机的轴到相机的距离。



### 版本更新说明
- v1.0：实现了舵机离散最优决策
- v1.1：实现了空八叉树输入（但在实际应用中不支持空轨迹输入）的正确显示
- v1.2：实现了轨迹外部接口，代码优化
- v1.3：添加了标定接口
- v1.4：添加了相机调试接口
- v1.5：添加了云台调度轨迹插补
- v2.0：基于碰撞概率和不确定度的主动视觉机构控制，还需要实现语义地图的构建，在代码上暂时不可运行，测试请运行testSafeMDP
- v2.1：已经调整好接口，实现持续主动视觉控制，更新仿真环境
- v2.2：解决内存泄漏的问题，现在可以执行仿真环境仿真

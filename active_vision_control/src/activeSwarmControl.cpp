#include "active_vision_control/activeVision.h"
#include "active_vision_control/moveitRobot.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
ros::Publisher servo1pub;
ros::Publisher servo2pub;
ros::Publisher servo3pub;
ros::Publisher servo4pub;

MoveitRobot testRobot;

std::vector<Eigen::Vector3d>curTraj;
std::vector<robotCylinder>curRobotCover;
octomap::OcTree * sub_octree;

ActiveVision visionDevice1(servoState(0,0), Eigen::Vector3d(0,-0.42,-0.78), 1);
ActiveVision visionDevice2(servoState(0,0), Eigen::Vector3d(0,0.42,-0.8), 2);

bool hasMap=false;
bool hasTraj=false;
/*
    八叉树地图回调函数
*/
void octomapCallback(const octomap_msgs::Octomap & octo_map);
/*
    末端轨迹回调函数
*/
void trajsCallback(const std_msgs::Float64MultiArray & trajmsg);
/*
    集群接口，下一时刻集群的State
*/
void nextBestViewOfVisionSwarm(servoState &v1state, servoState &v2state);
/*
    更新机器人位置坐标
*/
void updateRobot();
/*
    发布消息
*/
void pubState();


int main(int argc, char** argv){
    ros::init(argc, argv, "ActiveSwarmNode");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    ros::Rate looprate(10);
    spinner.start();
    servo1pub = nh.advertise<std_msgs::Float64> ("servo1", 10);  
    servo2pub = nh.advertise<std_msgs::Float64> ("servo2", 10); 
    servo3pub = nh.advertise<std_msgs::Float64> ("servo3", 10);  
    servo4pub = nh.advertise<std_msgs::Float64> ("servo4", 10); 
    ros::Subscriber octomapSub=nh.subscribe("/octomap_full", 10, octomapCallback);
    ros::Subscriber trajSub=nh.subscribe("/activeTraj", 10, trajsCallback);
    static const std::string PLANNING_GROUP = "arm";
    moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    testRobot.setRobot(arm, PLANNING_GROUP);
    ros::spinOnce();
    updateRobot();
    servoState nextState1,nextState2;
    while(ros::ok()){
        nextBestViewOfVisionSwarm(nextState1,nextState2);
        visionDevice1.setVisionState(nextState1);
        visionDevice2.setVisionState(nextState2);
        visionDevice1.broadcastTF();
        visionDevice2.broadcastTF();
        pubState();
        ros::spinOnce();//接收初始的数据
        updateRobot();//测试用
        looprate.sleep();
    }
    return 0;
}

void octomapCallback(const octomap_msgs::Octomap & octo_map){
    octomap::AbstractOcTree* sTree=octomap_msgs::fullMsgToMap(octo_map);
    sub_octree = new octomap::OcTree(octo_map.resolution);
    sub_octree = dynamic_cast<octomap::OcTree*>(sTree);
    hasMap=true;
}

void trajsCallback(const std_msgs::Float64MultiArray & trajmsg){
    hasTraj=true;
    curTraj.clear();
    int trajNum=trajmsg.data.size()/6;
    //std::cout<<"trajNum is "<<trajNum<<std::endl;
    for(int i=0;i<trajNum;i++){
        std::vector<double> jointValue;
        for(int j=0;j<6;j++){
            jointValue.push_back(trajmsg.data.at(i*6+j));
        }
        Eigen::Matrix4d endp=testRobot.forwordKineEnd(jointValue);
        Eigen::Vector3d endt=endp.block<3,1>(0,3);
        
        curTraj.push_back(endt);
    }
}
void nextBestViewOfVisionSwarm(servoState &v1state, servoState &v2state){
    std::vector<servoState> vision1AlterState;
    // for(auto it:curTraj){
        
    //     std::cout<<it(0)<<' '<<it(1)<<' '<<it(2)<<std::endl;
    //     std::cout<<std::endl;
    // }
    std::vector<double>vision1Profit=visionDevice1.profitOfEachDecision(curTraj, curRobotCover, sub_octree, vision1AlterState);
    std::vector<servoState> vision2AlterState;
    std::vector<double>vision2Profit=visionDevice2.profitOfEachDecision(curTraj, curRobotCover, sub_octree, vision2AlterState);
    
    //for(auto ss:vision2AlterState)
        //std::cout<<ss.getAngle1()<<' '<<ss.getAngle2()<<std::endl;
    
    //策略，找出最大的profit后剩下的另一个vision看他最大的一个
    int maxProfitChoice1=0;
    double maxProfit1=0;
    for(int i=0;i<vision1AlterState.size();i++){
        if(vision1Profit[i]>maxProfit1){
            maxProfit1=vision1Profit[i];
            maxProfitChoice1=i;
        }
        //std::cout<<"Device1's "<<i<<" choice profit is "<<vision1Profit[i]<<std::endl;
    }
    int maxProfitChoice2=0;
    double maxProfit2=0;
    for(int i=0;i<vision2AlterState.size();i++){
        if(vision2Profit[i]>maxProfit2){
            maxProfit2=vision2Profit[i];
            maxProfitChoice2=i;
        }
        //std::cout<<"Device2's "<<i<<" choice profit is "<<vision2Profit[i]<<std::endl;
    }

    if(maxProfit1>maxProfit2){
        v1state.setAngle1(vision1AlterState[maxProfitChoice1].getAngle1());
        v1state.setAngle2(vision1AlterState[maxProfitChoice1].getAngle2());
        //std::cout<<"Device1's choice is "<<maxProfitChoice1<<std::endl;
        if(maxProfitChoice1!=maxProfitChoice2){
            v2state.setAngle1(vision2AlterState[maxProfitChoice2].getAngle1());
            v2state.setAngle2(vision2AlterState[maxProfitChoice2].getAngle2());
        }
        else{
            int maxProfitChoice22=0;
            double maxProfit22=0;
            for(int i=0;i<vision2AlterState.size();i++){
                if(i==maxProfit2)continue;
                if(vision2Profit[i]>maxProfit22){
                    maxProfit22=vision2Profit[i];
                    maxProfitChoice22=i;
                }
            }
            if(vision2Profit[maxProfitChoice22]>1e-5)
                maxProfitChoice2=maxProfitChoice22;
            v2state.setAngle1(vision2AlterState[maxProfitChoice2].getAngle1());
            v2state.setAngle2(vision2AlterState[maxProfitChoice2].getAngle2());
        }
    }
    else{
        v2state.setAngle1(vision2AlterState[maxProfitChoice2].getAngle1());
        v2state.setAngle2(vision2AlterState[maxProfitChoice2].getAngle2());
        //std::cout<<"Device2's choice is "<<maxProfitChoice1<<std::endl;
        if(maxProfitChoice1!=maxProfitChoice2){
            v1state.setAngle1(vision1AlterState[maxProfitChoice1].getAngle1());
            v1state.setAngle2(vision1AlterState[maxProfitChoice1].getAngle2());
        }
        else{
            int maxProfitChoice11=0;
            double maxProfit11=0;
            for(int i=0;i<vision1AlterState.size();i++){
                if(i==maxProfit1)continue;
                if(vision1Profit[i]>maxProfit11){
                    maxProfit11=vision1Profit[i];
                    maxProfitChoice11=i;
                }
            }
            if(vision1Profit[maxProfitChoice11]>1e-5)
                maxProfitChoice1=maxProfitChoice11;
            v1state.setAngle1(vision1AlterState[maxProfitChoice1].getAngle1());
            v1state.setAngle2(vision1AlterState[maxProfitChoice1].getAngle2());
        }
    }
}
//测试用
void updateRobot(){
    testRobot.genCurrentCover();
    curRobotCover=testRobot.getCover();
}
void pubState(){
    std_msgs::Float64 data1,data2, data3, data4;
    data1.data=visionDevice1.getVisionState().getAngle1()*180.0/3.1415926535;
    data2.data=visionDevice1.getVisionState().getAngle2()*180.0/3.1415926535;
    data3.data=visionDevice2.getVisionState().getAngle1()*180.0/3.1415926535;
    data4.data=visionDevice2.getVisionState().getAngle2()*180.0/3.1415926535;


    // std::cout<<data1.data<<' '<<data2.data<<std::endl;
    servo1pub.publish(data1);
    servo2pub.publish(data2);
    servo3pub.publish(data3);
    servo4pub.publish(data4);
}
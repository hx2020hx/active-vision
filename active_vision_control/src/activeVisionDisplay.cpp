#include "active_vision_control/activeVision.h"
#include "active_vision_control/envelopeShape.h"
#include "active_vision_control/movingObs.h"
#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>

std::vector<envelopeShape::movingObs> curObsList;

ActiveVision* curVision;

bool has_obs(false);
Eigen::Vector3d curObsCenter;

void obsStateCallback(const std_msgs::Float64MultiArray msg);

int main(int argc, char** argv){
    ros::init(argc, argv, "activeVisionDisplay");
    ros::AsyncSpinner spinner(1);//一定要用这个
    ros::NodeHandle nh;
    spinner.start();
    double l1,l2,l3;
    nh.getParam("l1", l1);
    nh.getParam("l2", l2);
    nh.getParam("l3", l3);
    ros::Subscriber obsStateSub=nh.subscribe("/obsState", 10, obsStateCallback);
    // ros::Subscriber RobotStateSub=nh.subscribe("/rbstate", 10, rbStateCallback);
    //一些初始化
    curVision=new ActiveVision(servoState(0,0),Eigen::Vector3d(0,0,0), 1, nh);
    curVision->setl1(l1);
    curVision->setl2(l2);
    curVision->setl3(l3);
    ros::Rate looprate(20);
    // curObsCenter << 3,0,3;
    while(ros::ok()){
        if(!has_obs) continue;
        servoState targetState = curVision->visionInverseKine(curObsCenter);
        // std::cout<<targetState.getAngle1()<<" "<<targetState.getAngle2()<<std::endl;
        curVision->goToState(targetState);
        looprate.sleep();
        ros::spinOnce();
    }
    return 0;
}


void obsStateCallback(const std_msgs::Float64MultiArray msg){
    curObsList.clear();
    int trajNum=msg.data.size()/14;
    if(trajNum == 0)return;
    has_obs = true;
    // std::cout<<"have obs"<<std::endl;
    curObsCenter.fill(0);
    for(int i=0; i<trajNum; i++){
        
        envelopeShape::Cylinder* cyl;
        Eigen::Vector3d center;
        center << msg.data[0+14*i], msg.data[1+14*i], msg.data[2+14*i];
        Eigen::Vector3d zAxis;
        zAxis << msg.data[3+14*i], msg.data[4+14*i], msg.data[5+14*i];
        // 将旋转向量转化为AngleAxisd对象
        Eigen::AngleAxisd rotationAngleAxis(zAxis.norm(), zAxis.normalized());
        // 将AngleAxisd对象转化为旋转矩阵
        Eigen::Matrix3d rotationMatrix = rotationAngleAxis.toRotationMatrix();

        std::vector<double> paramm;
        paramm.push_back(msg.data[6+14*i]);
        paramm.push_back(msg.data[7+14*i]);
        cyl=new envelopeShape::Cylinder (paramm[0], paramm[1]);
        cyl->setPose(center, rotationMatrix);
        Eigen::Vector3d velo;
        velo << msg.data[8+14*i], msg.data[9+14*i], msg.data[10+14*i];
        Eigen::Vector3d Anglevelo;
        Anglevelo << msg.data[11+14*i], msg.data[12+14*i], msg.data[13+14*i];
        envelopeShape::movingObs cy(cyl,velo,Anglevelo,1,false);

        curObsList.push_back(cy);

        curObsCenter = curObsCenter + center;

    }

    curObsCenter = curObsCenter/static_cast<double>(trajNum);
}
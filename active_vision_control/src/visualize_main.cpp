#include "active_vision_control/visualization.h"

int main(int argc, char** argv){
    ros::init(argc, argv, "dataOut");
    ros::AsyncSpinner spinner(1);//一定要用这个
    ros::NodeHandle nh;
    spinner.start();
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    int mode;
    nh.param<int>("mode", mode, 1);
    Visualization myvisual(nh, arm, PLANNING_GROUP, mode);
    myvisual.spin();

    return 0;
}
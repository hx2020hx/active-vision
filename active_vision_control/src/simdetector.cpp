#include "active_vision_control/simdetector.h"
#include <visualization_msgs/Marker.h>
detector::detector(ros::NodeHandle &nh,moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP){
    body = nullptr;
    right_arm = nullptr;
    right_hand = nullptr;
    left_arm = nullptr;
    left_hand = nullptr;
    this->nh = nh;
    //std::cout<<"SSS0"<<std::endl;
    spinner.reset(new ros::AsyncSpinner(1));
    testRobot = new MoveitRobot(arm, PLANNING_GROUP);
    simStatePub = this->nh.advertise<std_msgs::Float64MultiArray>("/obsState", 10);
    peopleListener =nh.subscribe("/odomState", 10, &detector::peopleStateCallback, this);
}


void detector::peopleStateCallback(const std_msgs::Float64MultiArray msg){
    for(int i=0; i<5; i++){
        envelopeShape::movingObs* cy;
        envelopeShape::Cylinder* cyl;
        Eigen::Vector3d center;
        center << msg.data[0+14*i], msg.data[1+14*i], msg.data[2+14*i];
        Eigen::Vector3d zAxis;
        zAxis << msg.data[3+14*i], msg.data[4+14*i], msg.data[5+14*i];
        // 将旋转向量转化为AngleAxisd对象
        Eigen::AngleAxisd rotationAngleAxis(zAxis.norm(), zAxis.normalized());
        // 将AngleAxisd对象转化为旋转矩阵
        Eigen::Matrix3d rotationMatrix = rotationAngleAxis.toRotationMatrix();

        std::vector<double> paramm;
        paramm.push_back(msg.data[6+14*i]);
        paramm.push_back(msg.data[7+14*i]);
        cyl=new envelopeShape::Cylinder (paramm[0], paramm[1]);
        cyl->setPose(center, rotationMatrix);
        Eigen::Vector3d velo;
        velo << msg.data[8+14*i], msg.data[9+14*i], msg.data[10+14*i];
        Eigen::Vector3d Anglevelo;
        Anglevelo << msg.data[11+14*i], msg.data[12+14*i], msg.data[13+14*i];
        cy = new envelopeShape::movingObs (cyl,velo,Anglevelo,1,false);

        if(i==0)body=cy;
        else if(i==1)right_arm=cy;
        else if(i==2)left_arm=cy;
        else if(i==3)right_hand=cy;
        else left_hand=cy;
    }
    // std::cout<<"body is "<<body->getCenter()<<std::endl;
    // std::cout<<"right_arm is "<<right_arm->getCenter()<<std::endl;
    // std::cout<<"left_arm is "<<left_arm->getCenter()<<std::endl;
    // std::cout<<"right_hand is "<<right_hand->getCenter()<<std::endl;
    // std::cout<<"left_hand is "<<left_hand->getCenter()<<std::endl;
}

std::vector<Eigen::Vector3d> detector::genPointsInCylinder(envelopeShape::movingObs* myCylinder, int num_points){
    Eigen::Vector3d center;
    Eigen::Matrix3d orientation;
    std::vector<double> param;
    double radius;
    double height;
    myCylinder->getPose(center, orientation);
    myCylinder->getShape()->getShapeParameters(param);
    radius=param[0];
    height=param[1];
    std::vector<Eigen::Vector3d> points;
    //std::cout<<orientation<<std::endl;
    // 生成圆柱侧面的均匀分布点
    int numOfz = num_points/10;
    for(int j=0; j<numOfz; j++){
        for (int i = 0; i < 10; ++i) {
            double angle = 2.0 * M_PI * static_cast<double>(i) / static_cast<double>(10);
            double x = radius * std::cos(angle);
            double y = radius * std::sin(angle);
            double z = static_cast<double>(j) / static_cast<double>(numOfz - 1) * height - (height / 2.0);
            Eigen::Vector3d point(x, y, z);
            points.push_back(orientation * point + center);
        }
    }
    // 生成圆柱顶部和底部的点
    Eigen::Vector3d top_center = center + (orientation * Eigen::Vector3d(0.0, 0.0, height / 2.0));
    Eigen::Vector3d bottom_center = center + (orientation * Eigen::Vector3d(0.0, 0.0, -height / 2.0));
    
    points.push_back(top_center); // 顶部中心点
    points.push_back(bottom_center); // 底部中心点

    return points;
}

bool detector::isVisiblePoint(Eigen::Vector3d cyp, Eigen::Matrix4d endvision, std::vector<robotCylinder>&nextCover){
    double visionAngle=2.0/3.0*M_PI;
    Eigen::Vector4d blockpp(cyp(0),cyp(1),cyp(2),1);
    // std::cout<<"blockpp is"<<std::endl;
    // std::cout<<blockpp<<std::endl;
    Eigen::Vector3d endInWorld=endvision.block<3,1>(0,3);
    // std::cout<<"endInWorld is"<<std::endl;
    // std::cout<<endvision<<std::endl;
    Eigen::Vector4d blockInEnd=endvision.inverse()*blockpp;
    // std::cout<<"blockInEnd is"<<std::endl;
    // std::cout<<blockInEnd<<std::endl;
    //std::cout<<blockInEnd<<std::endl;
    if(blockInEnd(2)<0)return false;
    if(sqrt(blockInEnd(1)*blockInEnd(1)+blockInEnd(0)*blockInEnd(0))/blockInEnd(2)>tan(visionAngle/2.0))return false;
    int idd=0;

    for(auto cylin:nextCover){
        //获得遮挡面积以及表示
        idd++;
        Eigen::Matrix3d wcylinPose=cylin.getRotation();
        Eigen::Matrix3d endTransRotation=endvision.block<3,3>(0,0).transpose();
        Eigen::Matrix3d endcylinPose=endTransRotation*wcylinPose;
        Eigen::Vector3d worldInEnd=-endTransRotation*endInWorld;
        Eigen::Vector3d CylinWorldinEnd=endTransRotation*cylin.getCenter_trans();
        Eigen::Vector3d CylininEnd=worldInEnd+CylinWorldinEnd;
        Eigen::Vector3d zInEnd=endcylinPose.block<3,1>(0,2);
        
        double zangle=0;
        if(abs(zInEnd(2))<1e-8)zangle=3.1415926/2.0;
        else   zangle=abs(atan(sqrt(zInEnd(0)*zInEnd(0)+zInEnd(1)*zInEnd(1))/zInEnd(2)));
        
        double cylinWidth=cylin.getHeight()*sin(zangle)+cylin.getRadius()*cos(zangle)*2;
        
        double cylinlength=2.0*cylin.getRadius();
        

        if(blockInEnd(2)>CylininEnd(2)){
            double plantAngle=atan2(zInEnd(1),zInEnd(0));
           
            Eigen::Vector2d plantPoint(blockInEnd(0)*CylininEnd(2)/blockInEnd(2)-CylininEnd(0),blockInEnd(1)*CylininEnd(2)/blockInEnd(2)-CylininEnd(1));
            
            Eigen::Matrix2d TT;
            TT<<cos(plantAngle),sin(plantAngle),-sin(plantAngle),cos(plantAngle);
            Eigen::Vector2d plantPointInPlant=TT*plantPoint;

            if(abs(plantPointInPlant(0))<cylinWidth/2.0 && abs(plantPointInPlant(1))<cylinlength/2.0){
                return false;
            }
            
        }     
    }
    return true;
    
}

double detector::isVisible(envelopeShape::movingObs* underDetectCylinder, std::vector<robotCylinder>& nextCover){
    int num_sampling=50;
    std::vector<Eigen::Vector3d> cypoints = genPointsInCylinder(underDetectCylinder, num_sampling);
    int seeNum = 0;
    for(auto cyp: cypoints){
        if(isVisiblePoint(cyp, left_camera, nextCover)) seeNum++;
        //std::cout<<"BARK"<<std::endl;
    }
    double visionProb = static_cast<double>(seeNum) / static_cast<double>(num_sampling+2);
    return visionProb;
}
Eigen::Matrix4d detector::getTransformMatrix(const std::string& target_frame, const std::string& source_frame) {
    tf::TransformListener tf_listener;

    // 等待TF变换
    tf::StampedTransform transform;
    try {
        tf_listener.waitForTransform(target_frame, source_frame, ros::Time(0), ros::Duration(5.0));
        tf_listener.lookupTransform(target_frame, source_frame, ros::Time(0), transform);
    } catch (tf::TransformException& ex) {
        ROS_ERROR("TF变换错误: %s", ex.what());
        return Eigen::Matrix4d::Identity(); // 返回单位矩阵表示失败
    }

    // 将TF变换信息转换为Eigen::Matrix4d
    Eigen::Affine3d temp;
    tf::transformTFToEigen(transform, temp);
    temp.translation().z()-=0.8;
    Eigen::Matrix4d transform_matrix = temp.matrix().cast<double>();
    return transform_matrix;
}
//统一发送编码
//中心位置（3），旋转向量（3），半径，高度，速度（3），角速度（3）
std_msgs::Float64MultiArray detector::genMsgFromCylinder(envelopeShape::movingObs* cy){
    Eigen::Vector3d center = cy->getCenter();
    Eigen::Vector3d zAxis = cy->getz();
    //模拟噪声
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis(-0.002, 0.002); // 范围在-5cm到+5cm之间
    std::vector<double> paramm;
    cy->getShape()->getShapeParameters(paramm);
    Eigen::Vector3d velo = cy->getVelocity();
    Eigen::Vector3d Anglevelo = cy->getAngularVelocity();
    // std::cout<<"genCenter is "<<center<<std::endl;
    std_msgs::Float64MultiArray pubmsg;
    pubmsg.data.push_back(center(0)+ dis(gen));
    pubmsg.data.push_back(center(1)+ dis(gen));
    pubmsg.data.push_back(center(2)+ dis(gen));
    pubmsg.data.push_back(zAxis(0)+ dis(gen));
    pubmsg.data.push_back(zAxis(1)+ dis(gen));
    pubmsg.data.push_back(zAxis(2)+ dis(gen));
    pubmsg.data.push_back(paramm[0]+ dis(gen));
    pubmsg.data.push_back(paramm[1]+ dis(gen));
    pubmsg.data.push_back(velo(0)+ dis(gen));
    pubmsg.data.push_back(velo(1)+ dis(gen));
    pubmsg.data.push_back(velo(2)+ dis(gen));
    pubmsg.data.push_back(Anglevelo(0)+ dis(gen));
    pubmsg.data.push_back(Anglevelo(1)+ dis(gen));
    pubmsg.data.push_back(Anglevelo(2)+ dis(gen));
    return pubmsg;
}
void detector::updateVision(){
    left_camera=getTransformMatrix("base_link", "cam_1_link");
    right_camera=getTransformMatrix("base_link","cam_2_link");
    
}
void detector::publishCylinder(std::vector<bool> pubWhich){
    std_msgs::Float64MultiArray concatenatedArray;
    if(pubWhich[0]){
        std_msgs::Float64MultiArray bodymsg = genMsgFromCylinder(body);
        concatenatedArray.data.insert(concatenatedArray.data.end(), bodymsg.data.begin(), bodymsg.data.end());
    }
    if(pubWhich[1]){
        std_msgs::Float64MultiArray right_armmsg = genMsgFromCylinder(right_arm);
        concatenatedArray.data.insert(concatenatedArray.data.end(), right_armmsg.data.begin(), right_armmsg.data.end());
    }
    if(pubWhich[2]){
        std_msgs::Float64MultiArray left_armmsg = genMsgFromCylinder(left_arm);
        concatenatedArray.data.insert(concatenatedArray.data.end(), left_armmsg.data.begin(), left_armmsg.data.end());
    }
    if(pubWhich[3]){
        std_msgs::Float64MultiArray right_handmsg = genMsgFromCylinder(right_hand);
        concatenatedArray.data.insert(concatenatedArray.data.end(), right_handmsg.data.begin(), right_handmsg.data.end());
    }
    if(pubWhich[4]){
        std_msgs::Float64MultiArray left_handmsg = genMsgFromCylinder(left_hand);
        concatenatedArray.data.insert(concatenatedArray.data.end(), left_handmsg.data.begin(), left_handmsg.data.end());
    }
    simStatePub.publish(concatenatedArray);

}
void* detector::detectorThread(void *threadid){
    //initialize
    spinner->start();
    ros::Rate looprate(20);
    while(body == nullptr){
        ros::spinOnce();
        looprate.sleep();
    }
    double wholeTime = 20.0;//仿真时间20s
    auto startfromFirst = std::chrono::high_resolution_clock::now();
    auto start = std::chrono::high_resolution_clock::now();
    std::vector<double> timeSee;
    bool firstTime=true;
    bool hasRes = false;
    std::vector<bool> pubWhich;
    std::cout<<"Count Starts!"<<std::endl;
    
    while(ros::ok()){
        updateVision();
        if(!firstTime){
            auto end = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            auto durationFromFirst = std::chrono::duration_cast<std::chrono::milliseconds>(end - startfromFirst);
            for(int i=0;i<5;i++){
                
                if(pubWhich[i]){
                    timeSee[i]= timeSee[i]+ duration.count();
                    // std::cout<<"in the loop"<<std::endl;
                }
            }
            if(durationFromFirst.count()>wholeTime*1000.0){
                if(!hasRes){
                    for(int i=0;i<5;i++){
                        if(timeSee[i]/10.0/wholeTime>100){
                            std::cout<<"The link "<<i<<" was in the FOV "<<100<<" percent of whole time"<<std::endl;
                        }
                        else{
                            std::cout<<"The link "<<i<<" was in the FOV "<<timeSee[i]/10.0/wholeTime<<" percent of whole time"<<std::endl;
                        }
                        
                    }
                    hasRes = true;
                }
                
            }
            start = std::chrono::high_resolution_clock::now();
            pubWhich.clear();
        }
        else{
            pubWhich.clear();
            firstTime=false;
            for(int i=0;i<5;i++){
                timeSee.push_back(0);
            }
        }
        std::vector<robotCylinder> curCover = testRobot->getCover();
        isVisible(body, curCover)>0.4?pubWhich.push_back(true):pubWhich.push_back(false);
        isVisible(right_arm, curCover)>0.4?pubWhich.push_back(true):pubWhich.push_back(false);
        isVisible(right_hand, curCover)>0.4?pubWhich.push_back(true):pubWhich.push_back(false);
        isVisible(left_arm, curCover)>0.4?pubWhich.push_back(true):pubWhich.push_back(false);
        isVisible(left_hand, curCover)>0.4?pubWhich.push_back(true):pubWhich.push_back(false);
        // std::cout<<isVisible(body, curCover)<<std::endl;
        // std::cout<<isVisible(right_arm, curCover)<<std::endl;
        // std::cout<<isVisible(right_hand, curCover)<<std::endl;
        // std::cout<<isVisible(left_arm, curCover)<<std::endl;
        // std::cout<<isVisible(left_hand, curCover)<<std::endl;
        // for(int i=0;i<5;i++){
        //     std::cout<<"The link "<<i<<" was in the FOV "<<pubWhich[i]<<" percent of whole time"<<std::endl;
        // }
        publishCylinder(pubWhich);
        
        ros::spinOnce();
        looprate.sleep();
    }
    
    
    return 0;
}
#include "active_vision_control/Gaussian3D.h"
#include <Eigen/Dense>
#include <cmath>
#include <random>
#include <stdexcept>

Gaussian3D::Gaussian3D(const Eigen::VectorXd& mean, const Eigen::MatrixXd& covariance) {
    this->mean = mean;
    this->covariance = covariance;
}

void Gaussian3D::setMean(const Eigen::VectorXd& mean) {
    this->mean = mean;
}

void Gaussian3D::setCovariance(const Eigen::MatrixXd& covariance) {
    this->covariance = covariance;
}

Eigen::VectorXd Gaussian3D::getMean() const {
    return mean;
}

Eigen::MatrixXd Gaussian3D::getCovariance() const {
    return covariance;
}

double Gaussian3D::probabilityInBall(const Eigen::VectorXd& point, double radius) const {
    int dim = mean.size();

    // Check if the point and the mean have the same dimension
    if (point.size() != dim) {
        throw std::invalid_argument("The point must have the same dimension as the Gaussian mean.");
    }

    // Check if the covariance matrix is square and has the correct dimensions
    if (covariance.rows() != dim || covariance.cols() != dim) {
        throw std::invalid_argument("The covariance matrix must be square and have the same dimension as the mean.");
    }

    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::normal_distribution<> dist(0.0, 1.0);

    // Number of samples for Monte Carlo integration
    int num_samples = 100;
    int count_inside = 0;

    // Calculate the Cholesky decomposition of the covariance matrix
    Eigen::LLT<Eigen::MatrixXd> chol(covariance);
    Eigen::MatrixXd L = chol.matrixL();

    // Integrate the probability over the ball using Monte Carlo method
    for (int i = 0; i < num_samples; ++i) {
        Eigen::VectorXd sample(dim);
        for (int j = 0; j < dim; ++j) {
            sample(j) = dist(gen);
        }
        Eigen::VectorXd point_in_ball = mean + radius * (L * sample);
        Eigen::VectorXd diff = point_in_ball - point;
        if (diff.norm() <= radius) {
            count_inside++;
        }
    }

    // Calculate the probability as the ratio of points inside the ball to total samples
    // double volume = std::pow(M_PI, dim / 2.0) / std::tgamma(dim / 2 + 1) * std::pow(radius, dim);
    double probability = static_cast<double>(count_inside) / num_samples ;

    return probability;
}
double Gaussian3D::probabilityInTwoBall(const Eigen::VectorXd& point1, const Eigen::VectorXd& point2,double radius) const {
    int dim = mean.size();

    // Check if the point and the mean have the same dimension
    if (point1.size() != dim || point1.size() != dim) {
        throw std::invalid_argument("The point must have the same dimension as the Gaussian mean.");
    }

    // Check if the covariance matrix is square and has the correct dimensions
    if (covariance.rows() != dim || covariance.cols() != dim) {
        throw std::invalid_argument("The covariance matrix must be square and have the same dimension as the mean.");
    }

    // Create a random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::normal_distribution<> dist(0.0, 1.0);

    // Number of samples for Monte Carlo integration
    int num_samples = 10000;
    int count_inside = 0;

    // Calculate the Cholesky decomposition of the covariance matrix
    Eigen::LLT<Eigen::MatrixXd> chol(covariance);
    Eigen::MatrixXd L = chol.matrixL();

    // Integrate the probability over the ball using Monte Carlo method
    for (int i = 0; i < num_samples; ++i) {
        Eigen::VectorXd sample(dim);
        for (int j = 0; j < dim; ++j) {
            sample(j) = dist(gen);
        }
        Eigen::VectorXd point_in_ball = mean + radius * (L * sample);
        Eigen::VectorXd diff1 = point_in_ball - point1;
        Eigen::VectorXd diff2 = point_in_ball - point2;
        if (diff1.norm() <= radius && diff2.norm() <= radius) {
            count_inside++;
        }
    }

    // Calculate the probability as the ratio of points inside the ball to total samples
    double volume = std::pow(M_PI, dim / 2.0) / std::tgamma(dim / 2 + 1) * std::pow(radius, dim);
    double probability = static_cast<double>(count_inside) / num_samples ;

    return probability;
}

Eigen::MatrixXd Gaussian3D::updateVelocityCovariance(const Eigen::VectorXd& velocity,
                                         const Eigen::VectorXd& delta_velocity,
                                         const Eigen::MatrixXd& covariance_matrix) {
    // Check if the velocity and delta_velocity have the same dimension
    if (velocity.size() != delta_velocity.size()) {
        throw std::invalid_argument("Velocity and delta_velocity must have the same dimension.");
    }

    // Check if the covariance matrix is square and has the correct dimensions
    int dim = velocity.size();
    if (covariance_matrix.rows() != dim || covariance_matrix.cols() != dim) {
        throw std::invalid_argument("The covariance matrix must be square and have the same dimension as velocity.");
    }

    // Calculate the covariance matrix of delta_velocity (outer product of delta_velocity)
    Eigen::MatrixXd delta_velocity_covariance = delta_velocity * delta_velocity.transpose();

    // Scale the covariance matrix of delta_velocity by 1/3, assuming a uniform distribution in the range [-delta_velocity, delta_velocity]
    delta_velocity_covariance /= 3.0;

    // Calculate the new covariance matrix of the velocity after adding the random vector
    Eigen::MatrixXd new_covariance_matrix = covariance_matrix + delta_velocity_covariance;

    return new_covariance_matrix;
}

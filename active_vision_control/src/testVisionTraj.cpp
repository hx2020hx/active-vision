#include "active_vision_control/activeVision.h"
#include "active_vision_control/trajWithSpeed.h"
/*
    发布消息
*/
robotTraj* curTraj;
bool hasTraj = false;
JAKArobot* myrob;
void robotTrajCallback(const std_msgs::Float64MultiArray msg){
    hasTraj=true;
    curTraj->clearTraj();
    int trajNum=msg.data.size()/6;
    if(trajNum==0){robotConfig curConfig = myrob->getConfig();curTraj->addTraj(curConfig);}
    else{
        // std::cout<<"trajNum is "<<trajNum<<std::endl;
        for(int i=0;i<trajNum;i++){
            std::vector<double> jointValue;
            for(int j=0;j<6;j++){
                jointValue.push_back(msg.data.at(i*6+j));
            }
            robotConfig curj(jointValue);
            curTraj->addTraj(curj);
        }
    }
}
int main(int argc, char** argv){
    ros::init(argc, argv, "testVisionTraj");
    ros::AsyncSpinner spinner(1);//一定要用这个
    ros::NodeHandle nh;
    spinner.start();
    //机器人初始化
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    myrob=new JAKArobot(nh,arm, PLANNING_GROUP);
    ActiveVision visionDevice1(servoState(0,0), Eigen::Vector3d(-0.02,-0.37,-0.78), 1, nh);
    ActiveVision visionDevice2(servoState(0,0), Eigen::Vector3d(0.03,0.43,-0.78), 2, nh);
    ros::Subscriber robotTrajSub=nh.subscribe("/activeTraj", 10, robotTrajCallback);
    // servoState nextState1,nextState2;
    ros::Rate looprate(40);
    // visionDevice1.goToState(servoState(M_PI/2,M_PI/2));
    // std::cout<<visionDevice1.getVisionState().getAngle1()<<' '<<visionDevice1.getVisionState().getAngle2()<<std::endl;
    // visionDevice1.goToState(servoState(0.1,0.1));
    // bool isS = true;
    // while(ros::ok()){
    //     if(isS) visionDevice1.goToState(servoState(M_PI/2,M_PI/2));
    //     else visionDevice1.goToState(servoState(0.1,0.1));
    //     isS=!isS;
    //     looprate.sleep();
    // }
    visionTraj curvt1;
    visionTraj curvt2;
    while(ros::ok()){
        
        if(hasTraj){
            curvt1.clearTraj();
            curvt2.clearTraj();
            for(auto conf:curTraj->getTraj()){
                Eigen::Vector3d endp = myrob->forwardKineEnd(conf).block<3,1>(0,3);
                
                curvt1.addTraj(visionDevice1.visionInverseKine(endp));
                
                curvt2.addTraj(visionDevice2.visionInverseKine(endp));
            }
            
            visionDevice1.executeVisionTraj(curvt1);
        }
    }
    return 0;
}

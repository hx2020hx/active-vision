#include "active_vision_control/activeVision.h"
#include "active_vision_control/trajWithSpeed.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
ros::Publisher servo1pub;
ros::Publisher servo2pub;
ros::Publisher servo3pub;
ros::Publisher servo4pub;

JAKArobot *testRobot;

ActiveVision* visionDevice1;
ActiveVision* visionDevice2;
/*
    发布消息
*/
void pubState();


int main(int argc, char** argv){
    ros::init(argc, argv, "cylinderTest");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    ros::Rate looprate(10);
    spinner.start();
    servo1pub = nh.advertise<std_msgs::Float64> ("servo1", 10);  
    servo2pub = nh.advertise<std_msgs::Float64> ("servo2", 10); 
    servo3pub = nh.advertise<std_msgs::Float64> ("servo3", 10);  
    servo4pub = nh.advertise<std_msgs::Float64> ("servo4", 10); 
    static const std::string PLANNING_GROUP = "arm";
    moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    testRobot=new JAKArobot(nh, arm, PLANNING_GROUP);
    visionDevice1=new ActiveVision (servoState(0,0), Eigen::Vector3d(0.02,-0.45,-0.80), 1, nh);
    visionDevice2=new ActiveVision (servoState(0,0), Eigen::Vector3d(0.02,0.45,-0.80), 2, nh);
    ros::spinOnce();
    servoState nextState1,nextState2;
    while(ros::ok()){
        //std::cout<<endp<<std::endl;
        Eigen::Vector3d endt=Eigen::Vector3d(-0.6,0,-0.6);
        // nextState1=visionDevice1->visionInverseKine(endt+Eigen::Vector3d(0,0,0.1));
        nextState1=visionDevice1->visionInverseKine(endt);
        nextState2=visionDevice2->visionInverseKine(endt);
        visionDevice1->setVisionState(nextState1);
        visionDevice2->setVisionState(nextState2);
        visionDevice1->broadcastTF();
        visionDevice2->broadcastTF();
        pubState();
        ros::spinOnce();//接收初始的数据
        looprate.sleep();
    }
    return 0;
}
void pubState(){
    std_msgs::Float64 data1,data2, data3, data4;
    data1.data=visionDevice1->getVisionState().getAngle1()*180.0/3.1415926535;
    data2.data=visionDevice1->getVisionState().getAngle2()*180.0/3.1415926535;
    data3.data=visionDevice2->getVisionState().getAngle1()*180.0/3.1415926535;
    data4.data=visionDevice2->getVisionState().getAngle2()*180.0/3.1415926535;
    //std::cout<<data2.data<<std::endl;
    servo1pub.publish(data1);
    servo2pub.publish(data2);
    servo3pub.publish(data3);
    servo4pub.publish(data4);
}
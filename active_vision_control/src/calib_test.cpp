#include <ros/ros.h>
#include <Eigen/Dense>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/PoseStamped.h>
#include "active_vision_control/activeVision.h"
#include "active_vision_control/trajWithSpeed.h"

std::vector<Eigen::Matrix4d> zn_list;
std::vector<servoState> servolist;
std::vector<Eigen::Matrix4d> endplist;
bool isReturn =true;
bool locked = false;
void printServoState(servoState theta_i) {
    std::cout << theta_i.getAngle1() << ',' << theta_i.getAngle2() << std::endl;
}

void printMatrix4d(const Eigen::Matrix4d& matrix) {
    // Assuming you want to print the entire matrix

    std::cout << matrix(0,0)<< ','<<matrix(0,1)<< ','<<matrix(0,2)<< ','<<matrix(0,3)<< ','<< std::endl;
    std::cout << matrix(1,0)<< ','<<matrix(1,1)<< ','<<matrix(1,2)<< ','<<matrix(1,3)<< ','<< std::endl;
    std::cout << matrix(2,0)<< ','<<matrix(2,1)<< ','<<matrix(2,2)<< ','<<matrix(2,3)<< ','<< std::endl;
    std::cout << matrix(3,0)<< ','<<matrix(3,1)<< ','<<matrix(3,2)<< ','<<matrix(3,3)<< std::endl;
}
Eigen::MatrixXd logT(const Eigen::MatrixXd& T_SE3) {
    Eigen::Matrix3d R = T_SE3.block<3, 3>(0, 0);
    Eigen::Vector3d p = T_SE3.block<3, 1>(0, 3);
    double theta = acos(0.5 * (R.trace() - 1));

    Eigen::Matrix3d Omega;
    Eigen::Matrix3d V_inv;
    Eigen::Vector3d w(0, 0, 0);

    if (theta == 0) {
        Omega.setZero();
        V_inv.setIdentity();
    } else {
        double A = sin(theta) / theta;
        double B = (1 - cos(theta)) / (theta * theta);
        Omega = (theta / 2 / sin(theta)) * (R - R.transpose());
        V_inv = Eigen::Matrix3d::Identity() - 0.5 * Omega + 1 / (theta * theta) * (1 - A / 2 / B) * Omega * Omega;
        w << Omega(2, 1), Omega(0, 2), Omega(1, 0);
    }

    Eigen::MatrixXd T_se3(6, 1);
    T_se3.block<3, 1>(0, 0) = V_inv * p;
    T_se3.block<3, 1>(3, 0) = w;
    
    return T_se3;
}

double cal_en(const Eigen::VectorXd& promVar) {
    double ennorm = 0.0;
    // Eigen::MatrixXd data_array; // Load this from your file or source
    
    Eigen::Vector3d base_pose = promVar.segment<3>(0);
    Eigen::Vector2d deltaTheta = promVar.segment<2>(3);
    double dcur = promVar(5);

    // Initialize curVision (assumed class or function)
    // activeVisionBase curVision(base_pose, 0.069, 0.02, 0.061);
    ActiveVision curVision(servoState(0,0), base_pose, -1);
    // std::cout<<"T matrix is :";
    for (int i = 0; i < zn_list.size(); ++i) {
        Eigen::Matrix4d transform_matrix = Eigen::Matrix4d::Identity();
        transform_matrix.block<3, 1>(0, 3) = Eigen::Vector3d(0, 0, dcur);
        Eigen::MatrixXd Tn =  transform_matrix * endplist[i]*zn_list[i].inverse();

        Eigen::MatrixXd T_theta = curVision.visionForwardKine(servoState(servolist[i].getAngle1()+deltaTheta(0), servolist[i].getAngle2()+deltaTheta(1)));
        // printMatrix4d(T_theta);
        // std::cout<<std::endl;
        Eigen::MatrixXd T_error = T_theta.inverse() * Tn;

        Eigen::MatrixXd en = logT(T_error);
        ennorm += en.norm();
    }
    return ennorm;
}

void znCallback(geometry_msgs::PoseStamped msg){
    // Extract position and orientation from PoseStamped message
    Eigen::Vector3d position(msg.pose.position.x, msg.pose.position.y, msg.pose.position.z);
    Eigen::Quaterniond orientation(msg.pose.orientation.w, msg.pose.orientation.x,
                                   msg.pose.orientation.y, msg.pose.orientation.z);

    // Create a 4x4 transformation matrix
    Eigen::Matrix4d transform_matrix = Eigen::Matrix4d::Identity();

    // Set the top-left 3x3 submatrix to the rotation matrix
    transform_matrix.block<3, 3>(0, 0) = orientation.toRotationMatrix();

    // Set the top-right 3x1 submatrix to the translation vector
    transform_matrix.block<3, 1>(0, 3) = position;

    // Add the matrix to the vector
    if(!locked) {
        zn_list.push_back(transform_matrix);
        locked = true;
        std::cout<<"receive "<<zn_list.size()<<std::endl;
    }
    
    isReturn = false;
}
int main(int argc, char** argv) {
    //step1：采集数据
    ros::init(argc, argv, "calibration");
	ros::AsyncSpinner spinner(1);
	ros::NodeHandle nh;
    spinner.start();
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    JAKArobot* testRobot;
    if(argv[1]!=NULL){
        testRobot=new JAKArobot(nh,arm, PLANNING_GROUP, argv[1]);
    }
    else{
        testRobot=new JAKArobot(nh,arm, PLANNING_GROUP);
    }
    int test_id = 1;
    ros::Publisher commandPub = nh.advertise<std_msgs::Bool>("estimate_flag", 10);
    ros::Subscriber znSub = nh.subscribe("/cam_1/trans", 1, znCallback);
    ActiveVision cali_device(servoState(0,0), Eigen::Vector3d(-0.20,-0.4,-0.8), 1, nh);//--------------------init-----------------
    // ActiveVision cali_device(servoState(0,0), Eigen::Vector3d(-0.20 ,0.34 ,-0.8), 2, nh);//--------------------init-----------------
    
//     cali_device.setDeltas(Eigen::Vector2d(-1.51997, -0.800289));
    std::vector<robotConfig> calibrationList;
    

    //10个位姿
    std::vector<double>jv_1={0.506836,1.7887,1.19807,1.74217,5.04856,1.86366};
    robotConfig cali_1(jv_1);
    calibrationList.push_back(cali_1);

    std::vector<double>jv_2={0.323837,1.90178,1.19821,1.9192,5.31769,1.86364};
    robotConfig cali_2(jv_2);
    calibrationList.push_back(cali_2);

    std::vector<double>jv_3={0.431336,2.14167,1.19832,1.91913,5.38759,1.86366};
    robotConfig cali_3(jv_3);
    calibrationList.push_back(cali_3);

    std::vector<double>jv_4={0.301846,2.34402,1.33824,2.1172,5.74096,1.86368};
    robotConfig cali_4(jv_4);
    calibrationList.push_back(cali_4);

    std::vector<double>jv_5={0.418656,1.63176,1.05139,2.20107,5.08409,1.86366};
    robotConfig cali_5(jv_5);
    calibrationList.push_back(cali_5);

    std::vector<double>jv_6={0.422288,1.72022,1.21453,1.76347,5.53236,1.86367};
    robotConfig cali_6(jv_6);
    calibrationList.push_back(cali_6);

    std::vector<double>jv_7={0.115983,1.73613,1.32506,2.5999,5.82515,1.86368};
    robotConfig cali_7(jv_7);
    calibrationList.push_back(cali_7);

    std::vector<double>jv_8={6.26049,1.8929,1.09096,2.88575,5.5075,1.86368};
    robotConfig cali_8(jv_8);
    calibrationList.push_back(cali_8);

    std::vector<double>jv_9={6.26061,2.13206,1.14814,2.62383,5.50751,1.86368};
    robotConfig cali_9(jv_9);
    calibrationList.push_back(cali_9);

    std::vector<double>jv_10={0.0279588,2.18425,1.13898,2.43448,5.86494,1.86372};
    robotConfig cali_10(jv_10);
    calibrationList.push_back(cali_10);
        // std::vector<double>jv_1={0.212408,2.31021,1.17782,2.33133,4.09969,1.86376};
        // robotConfig cali_1(jv_1);
        // calibrationList.push_back(cali_1);

        // std::vector<double>jv_2={6.24222,2.31004,1.17779,2.40638,4.14382,1.86374};
        // robotConfig cali_2(jv_2);
        // calibrationList.push_back(cali_2);

        // std::vector<double>jv_3={6.24221,2.11597,1.1388,2.4064,4.21255,1.8637};
        // robotConfig cali_3(jv_3);
        // calibrationList.push_back(cali_3);

        // std::vector<double>jv_4={0.234459,2.11608,1.03796,2.40626,4.49569,1.86375};
        // robotConfig cali_4(jv_4);
        // calibrationList.push_back(cali_4);

        // std::vector<double>jv_5={0.234423,1.92581,1.00937,2.46135,4.24425,1.86375};
        // robotConfig cali_5(jv_5);
        // calibrationList.push_back(cali_5);

        // std::vector<double>jv_6={0.00295966,1.92572,1.00937,2.54196,4.33854,1.86369};
        // robotConfig cali_6(jv_6);
        // calibrationList.push_back(cali_6);

        // std::vector<double>jv_7={0.00298363,1.65949,1.00963,2.4295,4.23409,1.86373};
        // robotConfig cali_7(jv_7);
        // calibrationList.push_back(cali_7);

        // std::vector<double>jv_8={0.321956,1.53015,1.14608,2.50318,4.39912,1.86369};
        // robotConfig cali_8(jv_8);
        // calibrationList.push_back(cali_8);

        // std::vector<double>jv_9={0.0591777,1.39385,2.22983,1.35993,3.89279,1.86372};
        // robotConfig cali_9(jv_9);
        // calibrationList.push_back(cali_9);

        // std::vector<double>jv_10={0.0275753,2.15995,1.16612,2.46564,4.2423,1.86378};
        // robotConfig cali_10(jv_10);
        // calibrationList.push_back(cali_10);

    
    testRobot->goToConf(calibrationList[test_id], 2);
    locked = false;
    Eigen::Vector3d endp = testRobot->forwardKineEnd(calibrationList[test_id]).block<3,1>(0,3);
    servoState theta_i = cali_device.visionInverseKine(endp);
    cali_device.goToState(theta_i);
    usleep(1e6);
    std_msgs::Bool start_estimate;
    start_estimate.data = true;
    commandPub.publish(start_estimate);
    ros::Rate looprate(20);
    isReturn=true;
    while(ros::ok() && isReturn){
        std_msgs::Bool start_estimate;
        start_estimate.data = true;
        commandPub.publish(start_estimate);
        ros::spinOnce();
        looprate.sleep();
    }
    servolist.push_back(theta_i);
    endplist.push_back(testRobot->forwardKineEnd(calibrationList[test_id]));

    //step2:优化计算

    Eigen::Matrix4d transform_matrix = Eigen::Matrix4d::Identity();
    transform_matrix.block<3, 1>(0, 3) = Eigen::Vector3d(0, 0, 0.00836);
    Eigen::MatrixXd Tn =  transform_matrix * endplist[0] * zn_list[0].inverse();
    Eigen::MatrixXd T_theta = cali_device.visionForwardKine(servoState(servolist[0].getAngle1(), servolist[0].getAngle2()));
    
    std::cout << "T measure:" << std::endl;
    printMatrix4d(Tn);
    std::cout << "T cal:" << std::endl;
    printMatrix4d(T_theta);

    std::cout << "endp:" << std::endl;
    printMatrix4d(endplist[0]);

    std::cout<<"zn"<<std::endl;
    printMatrix4d(zn_list[0]);
    return 0;
}
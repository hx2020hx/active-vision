#include "active_vision_control/safeCircle.h"

safeCircle::safeCircle(const int dt):dividingtimes(dt),maxVelocity(1){
    for(int i=0;i<dt;i++){
        for(int j=0;j<dt;j++){
            safeZone.push_back(0);
            safeZoneState.push_back(0);
            infectionDepth.push_back(0.0);
        }
    }
    innerClock=0;
}

safeCircle::safeCircle(ActiveVision* visionDevice,const int dt):dividingtimes(dt),maxVelocity(1){
    this->visionDevice=visionDevice;
    for(int i=0;i<dt;i++){
        for(int j=0;j<dt;j++){
            safeZone.push_back(visionDevice->getVisionDepth());
            safeZoneState.push_back(0);
            infectionDepth.push_back(0.0);
        }
    }
    innerClock=0;
}

safeCircle::safeCircle(ActiveVision* visionDevice,const int dt, ros::NodeHandle& nh_):dividingtimes(dt),maxVelocity(1), nh(nh_){
    this->visionDevice=visionDevice;
    for(int i=0;i<dt;i++){
        for(int j=0;j<dt;j++){
            safeZone.push_back(visionDevice->getVisionDepth());
            safeZoneState.push_back(0);
            infectionDepth.push_back(0.0);
        }
    }
    innerClock=0;
    safeCirclePub = nh.advertise<visualization_msgs::MarkerArray>("visualization_marker_array"+std::to_string(visionDevice->getId()), 10);
    safeDataPub = nh.advertise<std_msgs::Float64MultiArray>("/safeData"+std::to_string(visionDevice->getId()), 10);
}

visualization_msgs::Marker safeCircle::createPolygonMarker(const std::vector<geometry_msgs::Point>& points, const std::string& frame_id, const int marker_id, double danger_ratio){
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame_id;
    marker.header.stamp = ros::Time::now();
    marker.ns = "polygons";
    marker.id = marker_id;
    marker.type = visualization_msgs::Marker::LINE_STRIP;
    marker.action = visualization_msgs::Marker::ADD;
    marker.scale.x = 0.01;  // line width

    // 计算四个点的平均值
    Eigen::Vector3d average_point(0.0, 0.0, 0.0);
    for (const auto& point : points) {
        average_point.x() += point.x;
        average_point.y() += point.y;
        average_point.z() += point.z;
    }
    average_point /= points.size();

    // 计算平均值向量的范数
    double norm = average_point.norm();

    // 根据范数值设置颜色
    marker.color.r = danger_ratio;  // 红色权重与范数反相关
    marker.color.g = 1-danger_ratio;        // 绿色权重与范数正相关
    marker.color.b = 0.0;
    marker.color.a = 0.5;  // 50%透明度

    // 添加多边形的点
    marker.points = points;

    // 封闭多边形
    marker.points.push_back(points[0]);

    // visualization_msgs::Marker vertexMarker;
    // vertexMarker.header.frame_id = frame_id;
    // vertexMarker.header.stamp = ros::Time::now();
    // vertexMarker.ns = "vertices";
    // vertexMarker.id = 1;
    // vertexMarker.type = visualization_msgs::Marker::POINTS;
    // vertexMarker.action = visualization_msgs::Marker::ADD;
    // vertexMarker.scale.x = 0.02;  // 点的大小
    // vertexMarker.scale.y = 0.02;
    // vertexMarker.color.r = 0.0;
    // vertexMarker.color.g = 0.0;
    // vertexMarker.color.b = 1.0;  // 蓝色
    // vertexMarker.color.a = 1.0;

    // // 添加四个顶点
    // vertexMarker.points = points;
    
    return marker;
}


void safeCircle::infectI(int id, double angrad, int center){
    safeZone[id]=safeZone[center];
    if(isInCircle(getLeft(id),angrad,center)){
        //被右边扩散
        safeZoneState[getLeft(id)]=4;
        infectionDepth[getLeft(id)]=safeZone[getLeft(id)]-safeZone[id];
        infectI(getLeft(id), angrad, center);
    }
    if(isInCircle(getRight(id),angrad,center)){
        //被左边扩散
        safeZoneState[getRight(id)]=3;
        infectionDepth[getRight(id)]=safeZone[getRight(id)]-safeZone[id];
        infectI(getRight(id), angrad, center);
    }
    if(isInCircle(getUp(id),angrad,center)){
        //被下边扩散
        safeZoneState[getUp(id)]=6;
        infectionDepth[getUp(id)]=safeZone[getUp(id)]-safeZone[id];
        infectI(getUp(id), angrad, center);
    }
    if(isInCircle(getDown(id),angrad,center)){
        //被上边扩散
        safeZoneState[getDown(id)]=5;
        infectionDepth[getDown(id)]=safeZone[getDown(id)]-safeZone[id];
        infectI(getDown(id), angrad, center);
    }
}
void safeCircle::infection(double time, int index){
    double infRad=time*maxVelocity/safeZone[index];
    infectI(index,infRad,index);
}

void safeCircle::updateSafeZone(double time, servoState s1){
    //清除state印记
    // bool hasBark=false;
    clearState();
    for(int i=0; i<safeZone.size(); i++){
        //判断是否在视野内
        servoState curs=idToAngle(i);
        if(curs.disCost(s1)<visionDevice->getVisionAngle()/2.0){
            safeZone[i]=visionDevice->getVisionDepth();
            safeZoneState[i]=1;
            continue;
        }
        
        //判断传染和收缩大小问题
        if(safeZoneState[i]!=0 && safeZoneState[i]!=1 && safeZoneState[i]!=2){
            double shrinkDepth=maxVelocity*time;
            if(shrinkDepth>infectionDepth[i]){
                safeZone[i]+=infectionDepth[i];
                safeZone[i]-=maxVelocity*time;
                if(safeZone[i]<0)safeZone[i]=0;
                safeZoneState[i]=2;
            }
            continue;
        }
        //判断是否已经被操作
        if(safeZoneState[i]!=0)continue;
        else{
            //自己是高的--收缩
            if(safeZone[i]>=safeZone[getLeft(i)]&&safeZone[i]>=safeZone[getRight(i)]&&safeZone[i]>=safeZone[getUp(i)]&&safeZone[i]>=safeZone[getDown(i)]){
                safeZone[i]-=maxVelocity*time;
                if(safeZone[i]<0)safeZone[i]=0;
                safeZoneState[i]=2;
            }
            //自己是有一面是低的--传染
            else{
                infection(time, i);
                safeZone[i]-=maxVelocity*time;
                if(safeZone[i]<0)safeZone[i]=0;
                safeZoneState[i]=2;
            }
        }
        
    }
    innerClock+=time;
}
bool safeCircle::isInDanger(Eigen::Vector3d dangerPoint){
    servoState dangerState=visionDevice->visionInverseKine(dangerPoint);
    int dangerid=angleToId(dangerState);
    Eigen::Vector3d baseUp=visionDevice->getBase()+Eigen::Vector3d(0,0,visionDevice->getl1());
    double depth = safeZone[dangerid];
    double dangerDepth=(dangerPoint - baseUp).norm();
    // std::cout<<"dangerDepth is"<<dangerDepth<<' '<<"depth is "<<depth<<std::endl;
    return depth<dangerDepth;
}
envelopeShape::movingObs safeCircle::simObsGene(Eigen::Vector3d dangerPoint, double time){
    servoState dangerState=visionDevice->visionInverseKine(dangerPoint);
    int dangerid=angleToId(dangerState);
    envelopeShape::movingObs* fakeObs;
    if(safeZoneState[dangerid]==1 || safeZoneState[dangerid]==0){
        envelopeShape::Shape* fakeObsShape=new envelopeShape::Cylinder (0,0);
        fakeObs=new envelopeShape::movingObs( fakeObsShape,Eigen::Vector3d(0,0,0), Eigen::Vector3d::Zero(),0, true);
    }
    else if(safeZoneState[dangerid]==2){
        Eigen::Vector3d baseUp=visionDevice->getBase()+Eigen::Vector3d(0,0,visionDevice->getl1());
        Eigen::Vector3d zway=visionDevice->visionForwardKine(dangerState).block<3,1>(0,2);
        Eigen::Matrix3d rota=visionDevice->visionForwardKine(dangerState).block<3,3>(0,0);
        double depth = safeZone[dangerid]+time*maxVelocity;
        Eigen::Vector3d transl= baseUp+depth*zway;

        envelopeShape::Shape* fakeObsShape=new envelopeShape::Cylinder (0.2,0.4);
        fakeObsShape->setPose(transl, rota);
        //假想障碍物
        //envelopeShape::movingObs fakeObs( fakeObsShape, -zway, Eigen::Vector3d::Zero(),0.8, true);
        fakeObs=new envelopeShape::movingObs( fakeObsShape,Eigen::Vector3d(0,0,0), Eigen::Vector3d::Zero(),0, true);
        
    }
    else{
        envelopeShape::Shape* fakeObsShape=new envelopeShape::Cylinder (0,0);
        fakeObs=new envelopeShape::movingObs( fakeObsShape,Eigen::Vector3d(0,0,0), Eigen::Vector3d::Zero(),0, true);
    }
    // std::cout<<"fakeObs.getCenter"<<fakeObs->getCenter()<<std::endl;
    return *fakeObs;
    
}

void safeCircle::visualize_safeCircle(){
    double dtheta1 = M_PI/double(dividingtimes);
    double dtheta2 = M_PI/double(dividingtimes)*2.0;
    visualization_msgs::MarkerArray marker_array;
    std_msgs::Float64MultiArray safeCircleMsg;
    for(int testID = 0; testID < safeZone.size(); testID++){
        servoState midtestState = idToAngle(testID);

        servoState State1;
        if(midtestState.getAngle1() - dtheta1 < 0){
            State1.setAngle1(midtestState.getAngle1());
        }
        else{
            State1.setAngle1(midtestState.getAngle1()- dtheta1);
        }
        if(midtestState.getAngle2() - dtheta2 < 0){
            State1.setAngle2(midtestState.getAngle2());
        }
        else{
            State1.setAngle2(midtestState.getAngle2()- dtheta2);
        }

        servoState State2;
        if(midtestState.getAngle1() + dtheta1 > M_PI*2.0){
            State2.setAngle1(midtestState.getAngle1());
        }
        else{
            State2.setAngle1(midtestState.getAngle1() + dtheta1);
        }
        if(midtestState.getAngle2() - dtheta2 < 0){
            State2.setAngle2(midtestState.getAngle2());
        }
        else{
            State2.setAngle2(midtestState.getAngle2()- dtheta2);
        }

        servoState State3;
        if(midtestState.getAngle1() - dtheta1 < 0){
            State3.setAngle1(midtestState.getAngle1());
        }
        else{
            State3.setAngle1(midtestState.getAngle1()- dtheta1);
        }
        if(midtestState.getAngle2() + dtheta2 > M_PI){
            State3.setAngle2(midtestState.getAngle2());
        }
        else{
            State3.setAngle2(midtestState.getAngle2() + dtheta2);
        }

        servoState State4;
        if(midtestState.getAngle1() + dtheta1 > M_PI*2.0){
            State4.setAngle1(midtestState.getAngle1());
        }
        else{
            State4.setAngle1(midtestState.getAngle1()+ dtheta1);
        }
        if(midtestState.getAngle2() + dtheta2 > M_PI){
            State4.setAngle2(midtestState.getAngle2());
        }
        else{
            State4.setAngle2(midtestState.getAngle2() + dtheta2);
        }
        
        Eigen::Vector3d baseUp=Eigen::Vector3d(0,0,visionDevice->getl1());
        Eigen::Vector3d zway=visionDevice->visionForwardKine(midtestState).block<3,1>(0,2);
        Eigen::Vector3d zway1=visionDevice->visionForwardKine(State1).block<3,1>(0,2);
        Eigen::Vector3d zway2=visionDevice->visionForwardKine(State2).block<3,1>(0,2);
        Eigen::Vector3d zway3=visionDevice->visionForwardKine(State3).block<3,1>(0,2);
        Eigen::Vector3d zway4=visionDevice->visionForwardKine(State4).block<3,1>(0,2);
        Eigen::Vector3d final_result = baseUp + zway * safeZone[testID];
        Eigen::Vector3d final_result_1 = baseUp + zway1 * safeZone[testID];
        Eigen::Vector3d final_result_2 = baseUp + zway2 * safeZone[testID];
        Eigen::Vector3d final_result_3 = baseUp + zway3 * safeZone[testID];
        Eigen::Vector3d final_result_4 = baseUp + zway4 * safeZone[testID];
        safeCircleMsg.data.push_back(final_result(0));
        safeCircleMsg.data.push_back(final_result(1));
        safeCircleMsg.data.push_back(final_result(2));
        // 将这些点转换为 geometry_msgs::Point 类型
        geometry_msgs::Point point_0;
        point_0.x = final_result.x();
        point_0.y = final_result.y();
        point_0.z = final_result.z();
        geometry_msgs::Point point_1;
        point_1.x = final_result_1.x();
        point_1.y = final_result_1.y();
        point_1.z = final_result_1.z();

        geometry_msgs::Point point_2;
        point_2.x = final_result_2.x();
        point_2.y = final_result_2.y();
        point_2.z = final_result_2.z();

        geometry_msgs::Point point_3;
        point_3.x = final_result_3.x();
        point_3.y = final_result_3.y();
        point_3.z = final_result_3.z();

        geometry_msgs::Point point_4;
        point_4.x = final_result_4.x();
        point_4.y = final_result_4.y();
        point_4.z = final_result_4.z();

        // 将这些点存储在 std::vector<geometry_msgs::Point> 中
        std::vector<geometry_msgs::Point> points_vector;
        // points_vector.push_back(point_0);
        points_vector.push_back(point_1);
        points_vector.push_back(point_2);
        points_vector.push_back(point_4);
        points_vector.push_back(point_3);

        std::string frame_id = "fix_link"+std::to_string(visionDevice->getId());

        double dr =(visionDevice->getVisionDepth() - safeZone[testID])/visionDevice->getVisionDepth();

        visualization_msgs::Marker marker1 = createPolygonMarker(points_vector, frame_id, testID, dr);
        marker_array.markers.push_back(marker1);
    }
    

    // ROS_INFO("Polygon Points:");
    // for (const auto& point : points_vector) {
    //     ROS_INFO("  x: %f, y: %f, z: %f", point.x, point.y, point.z);
    // }
    

    safeCirclePub.publish(marker_array);
    safeDataPub.publish(safeCircleMsg);

}
#include <ros/ros.h>
#include <tf/tf.h>
#include <geometry_msgs/PoseStamped.h>
#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>
#include <Eigen/Dense>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

class CameraCalibrationNode
{
public:
    CameraCalibrationNode() : nh("~")
    {
        image_subscriber = nh.subscribe("/camera/rgb/image_raw", 1, &CameraCalibrationNode::imageCallback, this);
        //pose_subscriber = nh.subscribe("/acuro_board_pose", 1, &CameraCalibrationNode::poseCallback, this);
        pose_publisher = nh.advertise<geometry_msgs::PoseStamped>("/camera_pose_world", 1);
        aruco_dict = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_100); // You can change the dictionary if needed
        
        //acuro_board_pose = ...; // Initialize with known acuro board pose

        //----------------------------------这里打开修改参数-----------------------------------------------------------
        //camera_matrix = ...; // Initialize with known camera matrix
        //dist_coeffs = ...; // Initialize with known distortion coefficients
        //这里写上l1和l2的值
        // acuro_pose << 0,0,1,-l1,
        //               1,0,0,0,
        //               0,1,0,l2,
        //               0,0,0,1;
        Eigen::Matrix3d rotationMatrix = acuro_pose.block<3, 3>(0, 0);

        // 将Eigen::Matrix4d的平移向量提取出来
        Eigen::Vector3d translationVector = acuro_pose.block<3, 1>(0, 3);

        // 将旋转矩阵部分转换为四元数
        Eigen::Quaterniond quaternion(rotationMatrix);

        // 将四元数和平移向量分别设置到geometry_msgs::PoseStamped对象中
        acuro_board_pose.pose.orientation.x = quaternion.x();
        acuro_board_pose.pose.orientation.y = quaternion.y();
        acuro_board_pose.pose.orientation.z = quaternion.z();
        acuro_board_pose.pose.orientation.w = quaternion.w();
        acuro_board_pose.pose.position.x = translationVector.x();
        acuro_board_pose.pose.position.y = translationVector.y();
        acuro_board_pose.pose.position.z = translationVector.z();

        // 设置其他需要的字段，如header
        acuro_board_pose.header.stamp = ros::Time::now(); // 假设您使用了ROS，并且已经初始化了节点
    }

    void imageCallback(const sensor_msgs::ImageConstPtr& msg)
    {
        try
        {
            cv_bridge::CvImagePtr cv_ptr;
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            processImage(cv_ptr->image);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
        }
    }

    // void poseCallback(const geometry_msgs::PoseStampedConstPtr& msg)
    // {
    //     acuro_board_pose = *msg;
    // }

    void processImage(const cv::Mat& image)
    {
        cv::Mat gray;
        cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);

        std::vector<int> marker_ids;
        std::vector<std::vector<cv::Point2f>> marker_corners;

        cv::aruco::detectMarkers(gray, aruco_dict, marker_corners, marker_ids);

        if (!marker_ids.empty())
        {
            std::vector<cv::Vec3d> rvecs, tvecs;
            cv::aruco::estimatePoseSingleMarkers(marker_corners, 0.05, camera_matrix, dist_coeffs, rvecs, tvecs);

            if (!rvecs.empty() && !tvecs.empty())
            {
                // Assuming the acuro board pose is available, compute the camera pose in the world coordinate system
                // Here, you'll use the known acuro board pose and the computed tvecs and rvecs from aruco marker detection to calculate the camera's pose

                // Use the first detected marker (you can modify this based on your setup)
                cv::Vec3d rvec = rvecs[0];
                cv::Vec3d tvec = tvecs[0];

                // Convert the rotation vector to a rotation matrix
                cv::Mat rot_mat;
                cv::Rodrigues(rvec, rot_mat);

                // Fill in the position and orientation of the camera
                geometry_msgs::PoseStamped camera_pose;
                camera_pose.header.stamp = ros::Time::now();
                camera_pose.header.frame_id = "world"; // Change this frame ID to your world coordinate frame

                // Fill in the orientation (rotation matrix to quaternion)
                tf::Quaternion q;
                tf::Matrix3x3 rot_mat_tf(rot_mat.at<double>(0, 0), rot_mat.at<double>(0, 1), rot_mat.at<double>(0, 2),
                                        rot_mat.at<double>(1, 0), rot_mat.at<double>(1, 1), rot_mat.at<double>(1, 2),
                                        rot_mat.at<double>(2, 0), rot_mat.at<double>(2, 1), rot_mat.at<double>(2, 2));

                // Perform the transformation from the camera's local coordinate system to the world coordinate system
                tf::Stamped<tf::Pose> acuro_to_world_tf;
                tf::poseStampedMsgToTF(acuro_board_pose, acuro_to_world_tf);
                tf::Transform camera_to_acuro_tf(rot_mat_tf, tf::Vector3(tvec[0], tvec[1], tvec[2]));
                tf::Transform camera_to_world_tf = acuro_to_world_tf * camera_to_acuro_tf.inverse();

                tf::Quaternion camera_rot = camera_to_world_tf.getRotation();
                tf::Vector3 camera_trans = camera_to_world_tf.getOrigin();

                camera_pose.pose.position.x = camera_trans.x();
                camera_pose.pose.position.y = camera_trans.y();
                camera_pose.pose.position.z = camera_trans.z();

                camera_pose.pose.orientation.x = camera_rot.x();
                camera_pose.pose.orientation.y = camera_rot.y();
                camera_pose.pose.orientation.z = camera_rot.z();
                camera_pose.pose.orientation.w = camera_rot.w();

                pose_publisher.publish(camera_pose);
            }
        }
    }

private:
    ros::NodeHandle nh;
    ros::Subscriber image_subscriber;
    ros::Subscriber pose_subscriber;
    ros::Publisher pose_publisher;
    cv::Ptr<cv::aruco::Dictionary> aruco_dict;
    cv::Mat camera_matrix;
    cv::Mat dist_coeffs;
    geometry_msgs::PoseStamped acuro_board_pose;
    Eigen::Matrix4d acuro_pose;
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "camera_calibration_node");
    CameraCalibrationNode node;
    ros::spin();
    return 0;
}
#include "active_vision_control/activeVision.h"
#include "active_vision_control/trajWithSpeed.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
ros::Publisher servo1pub;
ros::Publisher servo2pub;
ros::Publisher servo3pub;
ros::Publisher servo4pub;

JAKArobot *testRobot;

ActiveVision* visionDevice1;
ActiveVision* visionDevice2;
/*
    发布消息
*/
void pubState();


int main(int argc, char** argv){
    ros::init(argc, argv, "ActiveSwarmNode");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    ros::Rate looprate(10);
    spinner.start();
    servo1pub = nh.advertise<std_msgs::Float64> ("servo1", 10);  
    servo2pub = nh.advertise<std_msgs::Float64> ("servo2", 10); 
    servo3pub = nh.advertise<std_msgs::Float64> ("servo3", 10);  
    servo4pub = nh.advertise<std_msgs::Float64> ("servo4", 10); 
    static const std::string PLANNING_GROUP = "arm";
    moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    testRobot=new JAKArobot(nh, arm, PLANNING_GROUP);
    visionDevice1=new ActiveVision (servoState(0,0), Eigen::Vector3d(-0.20,-0.4,-0.82), 1, nh);
    visionDevice2=new ActiveVision (servoState(0,0), Eigen::Vector3d(-0.20 ,0.34 ,-0.84), 2, nh);
    // visionDevice2->setDeltas(Eigen::Vector2d(-1.51997, -0.800289));
    ros::spinOnce();
    servoState nextState1,nextState2;
    Eigen::Vector3d offset1(0,0,0.2); 
    Eigen::Vector3d offset2(0,0,0);
    while(ros::ok()){
        
        Eigen::Matrix4d endp=testRobot->getEnd();
        //std::cout<<endp<<std::endl;
        Eigen::Vector3d endt=endp.block<3,1>(0,3);
        nextState1=visionDevice1->visionInverseKine(endt+Eigen::Vector3d(0,0,0.1));
        if(endt(1)<-0.1){
            offset1 = Eigen::Vector3d(0,0,0); 
            offset2 = Eigen::Vector3d(0,0,0.2); 
        }
        else if(endt(1)>0.1){
            offset2 = Eigen::Vector3d(0,0,0); 
            offset1 = Eigen::Vector3d(0,0,0.2); 
        }
        else {
            double coff = endt(1)+0.1;
            offset1 = Eigen::Vector3d(0,0,coff); 
            offset2 = Eigen::Vector3d(0,0,0.2-coff); 
        }
        offset1 = Eigen::Vector3d(0,0,0);
        offset2 = Eigen::Vector3d(0,0,0);
        nextState1=visionDevice1->visionInverseKine(endt+offset1);
        nextState2=visionDevice2->visionInverseKine(endt+offset2);
        // nextState1.setAngle1(M_PI/2.0);
        // nextState1.setAngle2(M_PI/2.0);

        // nextState2.setAngle1(M_PI/2.0);
        // nextState2.setAngle2(M_PI/2.0);
        visionDevice1->setVisionState(nextState1);
        visionDevice2->setVisionState(nextState2);

        // std::cout<<"1:"<<std::endl;
        // std::cout<<visionDevice1->visionForwardKine(nextState1)<<std::endl;

        // std::cout<<"2:"<<std::endl;
        // std::cout<<visionDevice2->visionForwardKine(nextState2)<<std::endl;
        visionDevice1->broadcastTF();
        visionDevice2->broadcastTF();
        pubState();
        ros::spinOnce();//接收初始的数据
        looprate.sleep();
    }
    return 0;
}
void pubState(){
    std_msgs::Float64 data1,data2, data3, data4;
    data1.data=visionDevice1->getVisionState().getAngle1()*180.0/3.1415926535;
    data2.data=visionDevice1->getVisionState().getAngle2()*180.0/3.1415926535+0.074533;
    data3.data=visionDevice2->getVisionState().getAngle1()*180.0/3.1415926535+0.0349066;
    data4.data=visionDevice2->getVisionState().getAngle2()*180.0/3.1415926535+0.039626;
    //std::cout<<data2.data<<std::endl;
    servo1pub.publish(data1);
    servo2pub.publish(data2);
    servo3pub.publish(data3);
    servo4pub.publish(data4);
}
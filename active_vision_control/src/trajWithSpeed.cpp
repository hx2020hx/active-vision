#include "active_vision_control/trajWithSpeed.h"
JAKAZuRobot robot;

bool Robot_Connect_Flag = false;
bool Robot_Move_Flag = false;
bool Robot_State_Thread_Flag = false;

const int sleepTime=8*1000; //us
ros::Publisher action_pub;
ros::Publisher traj_pub;

int sgn(double x){
    return x>0?1:-1;
}
void* Robot_State_Thread(void *rob)
{    
    JointValue joint_now;
    std::cout << "Robot State Thread Start!" << std::endl;
    //JAKArobot* myrob = (JAKArobot*) rob;
    while(Robot_State_Thread_Flag)
    {
        sensor_msgs::JointState joint_states;
        RobotStatus status;

        //robot.get_joint_position(&joint_now);
        robot.get_robot_status(&status);
        //status.joint_position
        //myrob->updateCurConfig();
        
        joint_states.position.clear();
        for(int i = 0; i < 6; i++)
        {
            joint_states.position.push_back(status.joint_position[i]);
            int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
            joint_states.header.stamp = ros::Time::now();
        }
        //std::cout << "Publish Joints" << std::endl;

        action_pub.publish(joint_states);
        usleep(4 * 1000);
    }

    std::cout << "Robot State Thread End!" << std::endl;

    return 0;
}
void* Fake_State_Thread(void *rob)
{    
    JointValue joint_now;
    std::cout << "Fake State Thread Start!" << std::endl;
    JAKArobot* myrob = (JAKArobot*) rob;
    while(ros::ok()){
        myrob->updateCurConfig();
        usleep(4 * 1000);
    }
    std::cout << "Fake State Thread End!" << std::endl;
    return 0;
}

void RVIZ_Joint_Init(JointValue& joint)
{
    sensor_msgs::JointState joint_states;

    joint_states.position.clear();
    for(int i = 0; i < 6; i++)
    {
        joint_states.position.push_back(joint.jVal[i]);
        int j = i+1;
        joint_states.name.push_back("joint_"+ std::to_string(j));
        joint_states.header.stamp = ros::Time::now();
    }

    action_pub.publish(joint_states);
}
double robotConfig::norm1(robotConfig otherConf){
    double dis=0;
    double theta1= jointAngle[0];
    double theta2= jointAngle[1];
    double theta3= jointAngle[2];
    double theta4= jointAngle[3];
    double theta5= jointAngle[4];
    double theta6= jointAngle[5];
    double dis1=abs(theta1-otherConf.getAngle(0))>(2*PI-abs(theta1-otherConf.getAngle(0)))?(2*PI-abs(theta1-otherConf.getAngle(0))):abs(theta1-otherConf.getAngle(0));
    double dis2=abs(theta2-otherConf.getAngle(1))>(2*PI-abs(theta2-otherConf.getAngle(1)))?(2*PI-abs(theta2-otherConf.getAngle(1))):abs(theta2-otherConf.getAngle(1));
    double dis3=abs(theta3-otherConf.getAngle(2))>(2*PI-abs(theta3-otherConf.getAngle(2)))?(2*PI-abs(theta3-otherConf.getAngle(2))):abs(theta3-otherConf.getAngle(2));
    double dis4=abs(theta4-otherConf.getAngle(3))>(2*PI-abs(theta4-otherConf.getAngle(3)))?(2*PI-abs(theta4-otherConf.getAngle(3))):abs(theta4-otherConf.getAngle(3));
    double dis5=abs(theta5-otherConf.getAngle(4))>(2*PI-abs(theta5-otherConf.getAngle(4)))?(2*PI-abs(theta5-otherConf.getAngle(4))):abs(theta5-otherConf.getAngle(4));
    double dis6=abs(theta6-otherConf.getAngle(5))>(2*PI-abs(theta6-otherConf.getAngle(5)))?(2*PI-abs(theta6-otherConf.getAngle(5))):abs(theta6-otherConf.getAngle(5));
    dis=5*dis1+5*dis2+3*dis3+dis4+dis5+dis6;
    return dis;
}

double robotConfig::norm2(robotConfig otherConf){
    double dis=0;
    double theta1= jointAngle[0];double _theta1=otherConf.getAngle(0);
    double theta2= jointAngle[1];double _theta2=otherConf.getAngle(1);
    double theta3= jointAngle[2];double _theta3=otherConf.getAngle(2);
    double theta4= jointAngle[3];double _theta4=otherConf.getAngle(3);
    double theta5= jointAngle[4];double _theta5=otherConf.getAngle(4);
    double theta6= jointAngle[5];double _theta6=otherConf.getAngle(5);
    double dis1=abs(theta1-_theta1)>(2*PI-abs(theta1-_theta1))?pow(2*PI-abs(theta1-_theta1),2):pow(theta1-_theta1,2);
    double dis2=abs(theta2-_theta2)>(2*PI-abs(theta2-_theta2))?pow(2*PI-abs(theta2-_theta2),2):pow(theta2-_theta2,2);
    double dis3=abs(theta3-_theta3)>(2*PI-abs(theta3-_theta3))?pow(2*PI-abs(theta3-_theta3),2):pow(theta3-_theta3,2);
    double dis4=abs(theta4-_theta4)>(2*PI-abs(theta4-_theta4))?pow(2*PI-abs(theta4-_theta4),2):pow(theta4-_theta4,2);
    double dis5=abs(theta5-_theta5)>(2*PI-abs(theta5-_theta5))?pow(2*PI-abs(theta5-_theta5),2):pow(theta5-_theta5,2);
    double dis6=abs(theta6-_theta6)>(2*PI-abs(theta6-_theta6))?pow(2*PI-abs(theta6-_theta6),2):pow(theta6-_theta6,2);
    dis=dis1+dis2+dis3+dis4+dis5+dis6;
    return dis;
}

robotConfig robotConfig::displacement(robotConfig otherConf){
    double theta1= jointAngle[0];
    double theta2= jointAngle[1];
    double theta3= jointAngle[2];
    double theta4= jointAngle[3];
    double theta5= jointAngle[4];
    double theta6= jointAngle[5];
    double dis1=abs(theta1-otherConf.getAngle(0))>(2*PI-abs(theta1-otherConf.getAngle(0)))?(2*PI-abs(theta1-otherConf.getAngle(0))):abs(theta1-otherConf.getAngle(0));
    double dis2=abs(theta2-otherConf.getAngle(1))>(2*PI-abs(theta2-otherConf.getAngle(1)))?(2*PI-abs(theta2-otherConf.getAngle(1))):abs(theta2-otherConf.getAngle(1));
    double dis3=abs(theta3-otherConf.getAngle(2))>(2*PI-abs(theta3-otherConf.getAngle(2)))?(2*PI-abs(theta3-otherConf.getAngle(2))):abs(theta3-otherConf.getAngle(2));
    double dis4=abs(theta4-otherConf.getAngle(3))>(2*PI-abs(theta4-otherConf.getAngle(3)))?(2*PI-abs(theta4-otherConf.getAngle(3))):abs(theta4-otherConf.getAngle(3));
    double dis5=abs(theta5-otherConf.getAngle(4))>(2*PI-abs(theta5-otherConf.getAngle(4)))?(2*PI-abs(theta5-otherConf.getAngle(4))):abs(theta5-otherConf.getAngle(4));
    double dis6=abs(theta6-otherConf.getAngle(5))>(2*PI-abs(theta6-otherConf.getAngle(5)))?(2*PI-abs(theta6-otherConf.getAngle(5))):abs(theta6-otherConf.getAngle(5));
    std::vector<double> cofdis;
    cofdis.push_back(dis1);cofdis.push_back(dis2);cofdis.push_back(dis3);cofdis.push_back(dis4);cofdis.push_back(dis5);cofdis.push_back(dis6);
    return robotConfig(cofdis);
}
std::vector<double> robotConfig::substract(robotConfig otherConf){
    std::vector<double>res;
    for(int i=0;i<6;i++){
        double deltaAngle=jointAngle[i]-otherConf.getAngle(i);
        if(deltaAngle<-PI)deltaAngle=2*PI+deltaAngle;
        if(deltaAngle>PI)deltaAngle=-(2*PI-deltaAngle);
        res.push_back(deltaAngle);
    }
    return res;
}

CubicSplineInterpolation::CubicSplineInterpolation(robotTraj data) {
    data_ = data;
    n_ = data.sizeOfTraj();
    std::vector<robotConfig> jt = data_.getTraj();
    std::array<double, 6> initvalue = {0.0};
    processInWhole.push_back(initvalue);
    // std::cout<<"x is 0.0 ";
    for(int k=1;k<jt.size();k++){
        robotConfig disp = jt[k].displacement(jt[k-1]);
        // double offset = jt[k].norm2(jt[k-1]);
        std::array<double, 6> curdisp = {0.0};
        for(int i=0;i<6;i++){
            curdisp[i] = processInWhole[k-1][i]+disp.getAngle(i);
            // processInWhole.push_back(processInWhole[k-1]+offset);
        }
        processInWhole.push_back(curdisp);
        // std::cout<<processInWhole[k]<< ' ';
    }
    // std::cout<<std::endl;
    ComputeCoefficients();
}

double CubicSplineInterpolation::Interpolate(double x, int angleId) {
    if (x <= processInWhole[0][angleId]) {
        return data_.getStart().getAngle(angleId);
    }
    if (x >= processInWhole[processInWhole.size()-1][angleId]) {
        return data_.getTerminal().getAngle(angleId);
    }
    std::vector<robotConfig> jt = data_.getTraj();
    int i = 0;
    while (x > processInWhole[i+1][angleId]) {
        i++;
    }
    // double h = processInWhole[i+1] -processInWhole[i];
    double t = (x - processInWhole[i][angleId]);
    
    double a = coeffs_[i].a[angleId];
    double b = coeffs_[i].b[angleId];
    double c = coeffs_[i].c[angleId];
    double d = coeffs_[i].d[angleId];
    
    double result = a + b * t + c * t * t + d * t * t * t;
    return result;
}

std::vector<robotConfig> CubicSplineInterpolation::GetInterpolatedPoints(double step_v) {
    std::vector<robotConfig> interpolatedPoints;
    double x = 0.0;
    double maxx = 0.0;
    for(int i=0;i<6;i++){
        if(processInWhole[processInWhole.size()-1][i]>maxx)maxx = processInWhole[processInWhole.size()-1][i];
    }
    // double a = coeffs_[0].a[0];
    // double b = coeffs_[0].b[0];
    // double c = coeffs_[0].c[0];
    // double d = coeffs_[0].d[0];
    // std::cout<<"a="<<a<<' '<<"b="<<b<<' '<<"c="<<c<<' '<<"d="<<d<<std::endl;
    // std::cout<<"max x is "<<processInWhole[processInWhole.size()-1]<<std::endl;
    // double t = processInWhole[processInWhole.size()-1]/2.0;
    // double result = a + b * t + c * t * t + d * t * t * t;
    // std::cout<<"mid is "<<result<<std::endl;
    // t = processInWhole[processInWhole.size()-1]*8.0/9.0;
    // result = a + b * t + c * t * t + d * t * t * t;
    // std::cout<<"8/9 is "<<result<<std::endl;
    // t = processInWhole[processInWhole.size()-1]*18.0/19.0;
    // result = a + b * t + c * t * t + d * t * t * t;
    // std::cout<<"18/19 is "<<result<<std::endl;
    // t = processInWhole[processInWhole.size()-1];
    // result = a + b * t + c * t * t + d * t * t * t;
    // std::cout<<"result is "<<result<<std::endl;
    while (x <= maxx) {
        // std::cout<<"x is "<<x<<std::endl;
        robotConfig curAngle;
        for(int i = 0;i<6;i++){
            double y = Interpolate(x, i);
            curAngle.setAngle(i,y);
        }
        interpolatedPoints.push_back(curAngle);
        x += step_v*pubTime;
        // std::cout<<"???"<<std::endl;
    }
    // std::cout<<"???"<<std::endl;
    robotConfig lcurAngle;
    for(int i = 0;i<6;i++){
        double y = Interpolate(processInWhole[processInWhole.size()-1][i], i);
        lcurAngle.setAngle(i,y);
    }
    interpolatedPoints.push_back(lcurAngle);
    return interpolatedPoints;
}
double CubicSplineInterpolation::angleSub(double suber, double subee){
    double deltaAngle=suber-subee;
    if(deltaAngle<-PI)deltaAngle=2*PI+deltaAngle;
    if(deltaAngle>PI)deltaAngle=-(2*PI-deltaAngle);
    return deltaAngle;
}
void CubicSplineInterpolation::ComputeCoefficients() {
    coeffs_.resize(n_);
    std::vector<robotConfig> jt = data_.getTraj();
    for(int angleId = 0; angleId < 6; angleId++) {
        
        for (int i = 0; i < n_; i++) {
            coeffs_[i].a[angleId] = jt[i].getAngle(angleId);
            coeffs_[i].b[angleId] = 0.0;
            coeffs_[i].c[angleId] = 0.0;
            coeffs_[i].d[angleId] = 0.0;
        }

        // Calculate intermediate values
        std::vector<double> h(n_);
        std::vector<double> alpha(n_);
        
        for (int i = 0; i < n_ - 1; i++) {
            h[i] = processInWhole[i + 1][angleId] - processInWhole[i][angleId];
            if (std::abs(h[i]) < std::numeric_limits<double>::epsilon()) {
                h[i] = std::numeric_limits<double>::epsilon();
            }
        }
        for (int i = 1; i < n_ - 1; i++) {
            alpha[i] = (3.0 / h[i]) * (angleSub(jt[i + 1].getAngle(angleId), jt[i].getAngle(angleId)))-(3.0 / h[i-1]) * (angleSub(jt[i ].getAngle(angleId), jt[i-1].getAngle(angleId)));
        }
        
        
        // Calculate coefficients for the tridiagonal matrix
        std::vector<double> l(n_);
        std::vector<double> mu(n_);
        std::vector<double> z(n_);
        l[0] = 1.0;
        mu[0] = 0.0;
        z[0] = 0.0;

        for (int i = 1; i < n_ - 1; i++) {
            l[i] = 2.0 * (processInWhole[i + 1][angleId] - processInWhole[i][angleId]) - h[i - 1] * mu[i - 1];
            if (std::abs(l[i]) < std::numeric_limits<double>::epsilon()) {
                l[i] = std::numeric_limits<double>::epsilon();
            }
            mu[i] = h[i] / l[i];
            z[i] = (alpha[i] - h[i - 1] * z[i - 1]) / l[i];
        }

        l[n_ - 1] = 1.0;
        z[n_ - 1] = 0.0;
        coeffs_[n_ - 1].c[angleId] = 0.0;

        // Calculate coefficients c and b
        for (int j = n_ - 2; j >= 0; j--) {
            coeffs_[j].c[angleId] = z[j] - mu[j] * coeffs_[j + 1].c[angleId];
            coeffs_[j].b[angleId] = (angleSub(jt[j + 1].getAngle(angleId), jt[j].getAngle(angleId))) / h[j] - h[j] * (coeffs_[j + 1].c[angleId] + 2.0 * coeffs_[j].c[angleId]) / 3.0;
            coeffs_[j].d[angleId] = (coeffs_[j + 1].c[angleId] - coeffs_[j].c[angleId]) / (3.0 * h[j]);
        }
    }
}


std::vector<robotConfig> CubicSplineInterpolation::GetTrapInterpolatedPoints(double step_v) {
        std::vector<robotConfig> interpolatedPoints;
        double x = 0.0;
        double maxx = 0.0;
        for(int i=0;i<6;i++){
            if(processInWhole[processInWhole.size()-1][i]>maxx)maxx = processInWhole[processInWhole.size()-1][i];
        }
        double init_v = 0;
        double acceleration = step_v / 2.0 ;  // 加速度
        if(acceleration>0.05) acceleration=0.05;
        double maxSpeed = step_v;
        double minSpeed = 0.01;  // 设置最小速度（根据需求调整）
        
        double timeToMaxSpeed = maxSpeed / acceleration;  // 达到最大速度所需的时间
        double distanceToMaxSpeed = 0.5 * acceleration * timeToMaxSpeed * timeToMaxSpeed;  // 达到最大速度所需的距离
        
        if(distanceToMaxSpeed<maxx){
            distanceToMaxSpeed = maxx/2.0;
        }
        double last_x = 0;
        robotConfig lastConfig;
        for (int i = 0; i < 6; i++) {
            double y = Interpolate(x, i);
            lastConfig.setAngle(i, y);
        }
        
        while (x <= maxx) {
            robotConfig curAngle;
            
            for (int i = 0; i < 6; i++) {
                double y = Interpolate(x, i);
                curAngle.setAngle(i, y);
            }
            while(curAngle.norm1(lastConfig)>0.1*step_v/6){
                x = (last_x+x)/2;
                for (int i = 0; i < 6; i++) {
                    double y = Interpolate(x, i);
                    curAngle.setAngle(i, y);
                }
            }
            interpolatedPoints.push_back(curAngle);

            // 调整速度和加速度以实现梯形速度插值
            if (x < distanceToMaxSpeed) {
                // 在此段距离内加速到最大速度
                init_v += acceleration * pubTime;
            } else if (x > (maxx - distanceToMaxSpeed)) {
                // 在最后一段距离内减速到最小速度
                init_v -= acceleration * pubTime;
                if (init_v < minSpeed) {
                    init_v = minSpeed;
                }
            }
            last_x = x;
            x += init_v * pubTime;
            lastConfig = curAngle;
            
        }
        
        return interpolatedPoints;
    }

std::vector<robotConfig> robotTraj::CuttingTo(double norm1Off){
    std::vector<robotConfig> afterDilitingPath;
    //先生成step
    double step=norm1Off/12.0;
    afterDilitingPath.push_back(jtraj[0]);
    for(int i=1;i<jtraj.size();i++){
        robotConfig frontNode=jtraj[i-1];
        robotConfig backNode=jtraj[i];
        //float step=stepSize/10.0;
        //double delta1,delta2,delta3,delta4,delta5,delta6;
        std::vector<double>dangle=backNode.substract(frontNode);
        double divideTime[]={abs(dangle[0])/step, abs(dangle[1])/step,abs(dangle[2])/step,abs(dangle[3])/step,abs(dangle[4])/step,abs(dangle[5])/step};
        int ddTime[]={int(divideTime[0]),int(divideTime[1]),int(divideTime[2]),int(divideTime[3]),int(divideTime[4]),int(divideTime[5])};
        int maxIt=ddTime[0];
        std::vector<double> curJoint=frontNode.getAngle();
        for(int i=1;i<6;i++){
            if(ddTime[i]>maxIt)maxIt=ddTime[i];
        }
        for(int i=0;i<maxIt;i++){
            for(int j=0;j<6;j++){
                if(ddTime[j]>0){
                    curJoint[j]=curJoint[j]+step*sgn(dangle[j]);
                    if(curJoint[j]>3.1415926)curJoint[j]-=2*3.1415926;
                    else if(curJoint[j]<-3.1415926)curJoint[j]+=2*3.1415926;
                    ddTime[j]=ddTime[j]-1;
                }
            }
            robotConfig curNewNode(curJoint);
            afterDilitingPath.push_back(curNewNode);
            //std::cout<<curNewNode.getAngle(0)<<" "<<curNewNode.getAngle(1)<<' '<<curNewNode.getAngle(2)<<std::endl;
        }
        afterDilitingPath.push_back(backNode);
    }
    return afterDilitingPath;
}

std::vector<robotConfig> robotTraj::Diliting(){
    return CuttingTo(0.005);//以1范数的0.005为标准进行标准剪裁
}
std::vector<robotConfig> robotTraj::SmoothCuttingTo(double norm1Off){
    // // double Kp=1;
    // std::vector<robotConfig> afterDilitingPath;
    // //先生成step
    // double step=norm1Off/12.0;
    // afterDilitingPath.push_back(jtraj[0]);
    // bool isCon=false;
    // for(int s=1;s<jtraj.size();s++){
    //     // if(i>jtraj.size()*double(endPath-1)/double(endPath) && norm1Off>1.0){
    //     //     step = (0.005+(double(jtraj.size())-double(i))/(double(jtraj.size()))*double(endPath)*(norm1Off-0.005))/12.0;
    //     // }
    //     robotConfig frontNode=jtraj[s-1];
    //     robotConfig backNode=jtraj[s];
    //     //float step=stepSize/10.0;
    //     //double delta1,delta2,delta3,delta4,delta5,delta6;
    //     std::vector<double>dangle=backNode.substract(frontNode);
    //     double divideTime[]={abs(dangle[0])/step, abs(dangle[1])/step,abs(dangle[2])/step,abs(dangle[3])/step,abs(dangle[4])/step,abs(dangle[5])/step};
    //     int ddTime[]={int(divideTime[0]),int(divideTime[1]),int(divideTime[2]),int(divideTime[3]),int(divideTime[4]),int(divideTime[5])};
    //     int maxIt=ddTime[0];
    //     std::vector<double> curJoint=frontNode.getAngle();
    //     for(int i=1;i<6;i++){
    //         if(ddTime[i]>maxIt)maxIt=ddTime[i];
    //     }
    //     if(s!=jtraj.size()-1){
    //         for(int i=0;i<maxIt;i++){
    //             for(int j=0;j<6;j++){
    //                 if(ddTime[j]>0){
    //                     curJoint[j]=curJoint[j]+step*sgn(dangle[j]);
    //                     if(curJoint[j]>3.1415926)curJoint[j]-=2*3.1415926;
    //                     else if(curJoint[j]<-3.1415926)curJoint[j]+=2*3.1415926;
    //                     ddTime[j]=ddTime[j]-1;
    //                 }
    //             }
    //             robotConfig curNewNode(curJoint);
    //             afterDilitingPath.push_back(curNewNode);
    //             //std::cout<<curNewNode.getAngle(0)<<" "<<curNewNode.getAngle(1)<<' '<<curNewNode.getAngle(2)<<std::endl;
    //         }
    //     }
    //     else{
    //         int addPoint = maxIt/endPath;
    //         // std::cout<<"addPoint is "<<addPoint<<std::endl;
    //         for(int i=0;i<maxIt+addPoint*(addPoint-1);i++){
    //             if(i<maxIt-addPoint){
    //                 for(int j=0;j<6;j++){
    //                     if(ddTime[j]>0){
    //                         curJoint[j]=curJoint[j]+step*sgn(dangle[j]);
    //                         if(curJoint[j]>3.1415926)curJoint[j]-=2*3.1415926;
    //                         else if(curJoint[j]<-3.1415926)curJoint[j]+=2*3.1415926;
    //                         ddTime[j]=ddTime[j]-1;
    //                     }
    //                 }
    //             }
                
    //             else{
    //                 if(!isCon){
    //                     for(int j=0;j<6;j++){
    //                         if(ddTime[j]<=0)ddTime[j]-=addPoint*(addPoint-1);
    //                     }
    //                     isCon=true;
    //                 }
    //                 for(int j=0;j<6;j++){
    //                     if(ddTime[j]+addPoint*(addPoint-1)>0){
    //                         curJoint[j]=curJoint[j]+step/double(addPoint)*sgn(dangle[j]);
    //                         if(curJoint[j]>3.1415926)curJoint[j]-=2*3.1415926;
    //                         else if(curJoint[j]<-3.1415926)curJoint[j]+=2*3.1415926;
    //                         ddTime[j]=ddTime[j]-1;
    //                     }
    //                 }
    //             }
                
    //             robotConfig curNewNode(curJoint);
    //             afterDilitingPath.push_back(curNewNode);
    //             //std::cout<<curNewNode.getAngle(0)<<" "<<curNewNode.getAngle(1)<<' '<<curNewNode.getAngle(2)<<std::endl;
    //         }
    //     }
        
    //     afterDilitingPath.push_back(backNode);
    // }
    // return afterDilitingPath;
    std::vector<robotConfig> noInfluTraj;
    std::vector<robotConfig> noInfDiliting;
    if(jtraj.size()>2){
        for(int i=0;i<jtraj.size()-1;i++)noInfluTraj.push_back(jtraj[i]);
        robotTraj noInfTraj(noInfluTraj);
        noInfDiliting = noInfTraj.CuttingTo(norm1Off);
    }
    
    robotTraj endTraj(jtraj[jtraj.size()-2],jtraj[jtraj.size()-1]);
    std::vector<robotConfig> firstDiliting = endTraj.CuttingTo(norm1Off);
    int sizeOfD = firstDiliting.size()/endPath;
    robotConfig slowP =firstDiliting [firstDiliting.size()/endPath*(endPath-2)];
    robotConfig slowP2 =firstDiliting [firstDiliting.size()/endPath*(endPath-1)];
    robotTraj firTraj(jtraj[jtraj.size()-2], slowP);
    robotTraj secondTraj(slowP ,slowP2);
    robotTraj thirdTraj(slowP2 ,jtraj[jtraj.size()-1]);

    std::vector<robotConfig> firDiliting=firTraj.CuttingTo(norm1Off);
    std::vector<robotConfig> secDiliting=secondTraj.CuttingTo(norm1Off/3.0*2.0);
    std::vector<robotConfig> ThirdDiliting=thirdTraj.CuttingTo(norm1Off/3.0);

    std::vector<robotConfig> resDiliting;
    resDiliting.insert(resDiliting.end(), noInfDiliting.begin(), noInfDiliting.end());
    resDiliting.insert(resDiliting.end(), firDiliting.begin(), firDiliting.end());
    resDiliting.insert(resDiliting.end(), secDiliting.begin(), secDiliting.end());
    resDiliting.insert(resDiliting.end(), ThirdDiliting.begin(), ThirdDiliting.end());
    return resDiliting;
}
std::vector<robotConfig> robotTraj::Slow_CuttingTo(double norm1Off){
    std::vector<robotConfig> slowDilitingPath;
    std::vector<robotConfig> startSlowPath;
    if(jtraj.size()>3){
        robotTraj myMidTraj;
        for(int i=1;i<=jtraj.size()-2;i++){
            myMidTraj.addTraj(jtraj[i]);
        }
        std::vector<robotConfig> midDilitingPath = myMidTraj.CuttingTo(norm1Off/2.0);
        // slowDilitingPath.insert(slowDilitingPath.end(), firDilitingPath.begin(), firDilitingPath.end());
        robotConfig startN1 = jtraj[0];
        robotConfig endN1 = jtraj[1];
        robotConfig startN2 = jtraj[jtraj.size()-1];
        robotConfig endN2 = jtraj[jtraj.size()-2];
        bool isCon =false;
        std::vector<double>dangle=endN1.substract(startN1);
        robotConfig lastEnd;
        //speed up
        double wayProcess1 = 0;
        std::vector<double> curJoint=startN1.getAngle();
        while(true){
            robotConfig curConfig(curJoint);
            robotConfig curDisp = curConfig.displacement(endN1);
            double step = (norm1Off*wayProcess1+0.01*(double(endPath*40) - wayProcess1))/12.0/double(endPath*40);
            // double step = norm1Off/12.0;
            // std::cout<<"step is "<< step<<std::endl;
            isCon = true;
            for(int i=0;i<6;i++){
                if(curDisp.getAngle(i)>step+0.001){
                    curJoint[i]+=step*sgn(dangle[i]);
                    if(curJoint[i]>3.1415926)curJoint[i]-=2*3.1415926;
                    else if(curJoint[i]<-3.1415926)curJoint[i]+=2*3.1415926;
                    isCon = false;
                }
            }
            if(isCon) {
                lastEnd.setAngle(curJoint);
                
                break;
            }
            slowDilitingPath.push_back(robotConfig(curJoint));
            if(wayProcess1<double(endPath*40))wayProcess1++;
        }
        slowDilitingPath.push_back(lastEnd);
        robotTraj lastSmooth(lastEnd, endN1);
        std::vector<robotConfig> lastSmoothTraj = lastSmooth.Diliting();
        slowDilitingPath.insert(slowDilitingPath.end(),lastSmoothTraj.begin(), lastSmoothTraj.end() );
        slowDilitingPath.insert(slowDilitingPath.end(),midDilitingPath.begin(), midDilitingPath.end() );
        //speed down
        isCon =false;
        double wayProcess2 = 0;
        curJoint=startN2.getAngle();
        dangle=endN2.substract(startN2);
        while(true){
            robotConfig curConfig(curJoint);
            robotConfig curDisp = curConfig.displacement(endN2);
            double step = (norm1Off*(double(endPath*40) - wayProcess2)+0.01*wayProcess2)/12.0/double(endPath*40);
            // double step = norm1Off/12.0;
            // std::cout<<"step is "<< step<<std::endl;
            isCon = true;
            for(int i=0;i<6;i++){
                if(curDisp.getAngle(i)>step+0.001){
                    curJoint[i]+=step*sgn(dangle[i]);
                    if(curJoint[i]>3.1415926)curJoint[i]-=2*3.1415926;
                    else if(curJoint[i]<-3.1415926)curJoint[i]+=2*3.1415926;
                    isCon = false;
                }
            }
            if(isCon) {
                lastEnd.setAngle(curJoint);
                
                break;
            }
            slowDilitingPath.push_back(robotConfig(curJoint));
            if(wayProcess2<double(endPath*40))wayProcess2++;
        }
        slowDilitingPath.push_back(lastEnd);
        robotTraj lastSmooth2(lastEnd, endN2);
        std::vector<robotConfig> lastSmoothTraj2 = lastSmooth2.Diliting();
        slowDilitingPath.insert(slowDilitingPath.end(),lastSmoothTraj2.begin(), lastSmoothTraj2.end() );
        
    }
    else if(jtraj.size()==2 || jtraj.size()==3){
        robotConfig startN = jtraj[0];
        robotConfig endN = jtraj[jtraj.size()-1];
        bool isCon =false;
        robotConfig midN;
        if(jtraj.size()==2){
            std::vector<robotConfig>wholeD=CuttingTo(norm1Off);
            midN=wholeD[wholeD.size()/2];
        }
        else{
            midN = jtraj[1];
        }
        robotConfig lastEnd;
        //speed up
        double wayProcess1 = 0;
        std::vector<double> curJoint=startN.getAngle();
        std::vector<double>dangle1=midN.substract(startN);
        while(true){
            robotConfig curConfig(curJoint);
            robotConfig curDisp = curConfig.displacement(midN);
            double step = (norm1Off*wayProcess1+0.005*(double(endPath*40) - wayProcess1))/12.0/double(endPath*40);
            // double step = norm1Off/12.0;
            // std::cout<<"step is "<< step<<std::endl;
            isCon = true;
            for(int i=0;i<6;i++){
                if(curDisp.getAngle(i)>step+0.0001){
                    curJoint[i]+=step*sgn(dangle1[i]);
                    if(curJoint[i]>3.1415926)curJoint[i]-=2*3.1415926;
                    else if(curJoint[i]<-3.1415926)curJoint[i]+=2*3.1415926;
                    isCon = false;
                }
                // std::cout<<curDisp.getAngle(i)<<std::endl;
            }
            if(isCon) {
                lastEnd.setAngle(curJoint);
                // std::cout<<"I out"<<std::endl;
                break;
            }
            slowDilitingPath.push_back(robotConfig(curJoint));
            if(wayProcess1<double(endPath*40))wayProcess1++;
        }
        slowDilitingPath.push_back(lastEnd);
        robotTraj lastSmooth(lastEnd, midN);
        std::vector<robotConfig> lastSmoothTraj = lastSmooth.CuttingTo(norm1Off);
        slowDilitingPath.insert(slowDilitingPath.end(),lastSmoothTraj.begin(), lastSmoothTraj.end() );

        //speed down
        isCon =false;
        double wayProcess2 = 0;
        curJoint=midN.getAngle();
        std::vector<double>dangle2=endN.substract(midN);
        while(true){
            robotConfig curConfig(curJoint);
            robotConfig curDisp = curConfig.displacement(endN);
            double step = (norm1Off*(double(endPath*40) - wayProcess2)+0.01*wayProcess2)/12.0/double(endPath*40);
            // double step = norm1Off/12.0;
            // std::cout<<"step is "<< step<<std::endl;
            isCon = true;
            for(int i=0;i<6;i++){
                if(curDisp.getAngle(i)>step+0.0001){
                    curJoint[i]+=step*sgn(dangle2[i]);
                    if(curJoint[i]>3.1415926)curJoint[i]-=2*3.1415926;
                    else if(curJoint[i]<-3.1415926)curJoint[i]+=2*3.1415926;
                    isCon = false;
                }
            }
            if(isCon) {
                lastEnd.setAngle(curJoint);
                break;
            }
            slowDilitingPath.push_back(robotConfig(curJoint));
            if(wayProcess2<double(endPath*40))wayProcess2++;
        }
        slowDilitingPath.push_back(lastEnd);
        robotTraj lastSmooth2(lastEnd, endN);
        std::vector<robotConfig> lastSmoothTraj2 = lastSmooth2.Diliting();
        slowDilitingPath.insert(slowDilitingPath.end(),lastSmoothTraj2.begin(), lastSmoothTraj2.end() );
    }
    // if(jtraj.size()>2){
    //     robotTraj myFirstTraj;
    //     for(int i=0;i<=jtraj.size()-2;i++){
    //         myFirstTraj.addTraj(jtraj[i]);
    //     }
    //     std::vector<robotConfig> firDilitingPath = myFirstTraj.CuttingTo(norm1Off/2.0);
    //     slowDilitingPath.insert(slowDilitingPath.end(), firDilitingPath.begin(), firDilitingPath.end());
    // }
    // robotConfig frontNode=jtraj[jtraj.size()-2];
    // robotConfig backNode=jtraj[jtraj.size()-1];
    // std::vector<double>dangle=backNode.substract(frontNode);
    // double wayProcess = 0;
    // std::vector<double> curJoint=frontNode.getAngle();
    // bool isCon =false;
    // slowDilitingPath.push_back(frontNode);
    // robotConfig lastEnd;
    // while(true){
    //     robotConfig curConfig(curJoint);
    //     // double curError = curConfig.norm2(backNode);
    //     // std::cout<<"curError is "<< curError<<std::endl;
    //     // if(curError<0.01)break;
    //     robotConfig curDisp = curConfig.displacement(backNode);
    //     double step = (norm1Off*(double(endPath*40) - wayProcess)+0.01*wayProcess)/12.0/double(endPath*40);
    //     // double step = norm1Off/12.0;
    //     // std::cout<<"step is "<< step<<std::endl;
    //     isCon = true;
    //     for(int i=0;i<6;i++){
    //         if(curDisp.getAngle(i)>step+0.0001){
    //             curJoint[i]+=step*sgn(dangle[i]);
    //             if(curJoint[i]>3.1415926)curJoint[i]-=2*3.1415926;
    //             else if(curJoint[i]<-3.1415926)curJoint[i]+=2*3.1415926;
    //             isCon = false;
    //         }
    //     }
    //     if(isCon) {
    //         lastEnd.setAngle(curJoint);
    //         break;
    //     }
    //     slowDilitingPath.push_back(robotConfig(curJoint));
    //     if(wayProcess<double(endPath*40))wayProcess++;
    // }
    // robotTraj lastSmooth(lastEnd, backNode);
    // std::vector<robotConfig> lastSmoothTraj = lastSmooth.Diliting();
    // slowDilitingPath.insert(slowDilitingPath.end(),lastSmoothTraj.begin(), lastSmoothTraj.end() );
    return slowDilitingPath;

}
std::vector<robotConfig> robotTraj::SmoothDiliting(double norm1Off){
    std::vector<robotConfig> noInfluTraj;
    std::vector<robotConfig> noInfDiliting;
    if(jtraj.size()>2){
        for(int i=0;i<jtraj.size()-1;i++)noInfluTraj.push_back(jtraj[i]);
        robotTraj noInfTraj(noInfluTraj);
        noInfDiliting = noInfTraj.CuttingTo(norm1Off);
        noInfDiliting.pop_back();
    }
    robotTraj endTraj(jtraj[jtraj.size()-2],jtraj[jtraj.size()-1]);
    std::vector<robotConfig> firstDiliting = endTraj.CuttingTo(norm1Off);
    // int sizeOfD = firstDiliting.size()/endPath;
    robotConfig slowP =firstDiliting [firstDiliting.size()*2/3];

    robotTraj firTraj(jtraj[jtraj.size()-2], slowP);
    robotTraj secondTraj(slowP ,jtraj[jtraj.size()-1]);


    std::vector<robotConfig> firDiliting=firTraj.CuttingTo(norm1Off);
    firDiliting.pop_back();
    std::vector<robotConfig> secDiliting=secondTraj.Slow_CuttingTo(norm1Off);

    std::vector<robotConfig> resDiliting;
    resDiliting.insert(resDiliting.end(), noInfDiliting.begin(), noInfDiliting.end());
    resDiliting.insert(resDiliting.end(), firDiliting.begin(), firDiliting.end());
    resDiliting.insert(resDiliting.end(), secDiliting.begin(), secDiliting.end());

    std::cout<<"resDiliting is "<<resDiliting.size()<<std::endl;
    return resDiliting;
}

std::vector<robotConfig> robotTraj::Cart_CuttingTo(double step, JAKArobot* curRob){
    // //先生成笛卡尔关键点
    // std::vector<Eigen::Vector3d> TrajEndPose;
    // for(auto cof:jtraj){
    //     Eigen::Matrix4d endpc=curRob->forwardKineEnd(cof);
    //     TrajEndPose.push_back(endpc.block<3,1>(0,3));
    // }
    // //位姿不变，统一为初始时的位姿
    // Eigen::Matrix3d rotationSave=curRob->forwardKineEnd(jtraj[0]).block<3,3>(0,0);

    // //确定生成点数
    // int numOfP=0;
    // double dis=0;
    // for(int i=1;i<jtraj.size();i++){
    //     dis+=sqrt(pow(TrajEndPose[i](0)-TrajEndPose[i-1](0),2)+pow(TrajEndPose[i](1)-TrajEndPose[i-1](1),2)+pow(TrajEndPose[i](2)-TrajEndPose[i-1](2),2));
    // }
    // numOfP=dis/step;
    // double rStep=1/numOfP;
    // std::vector<double>zeros;
    // std::vector<double>resx;
    // std::vector<double>resy;
    // std::vector<double>resz;
    // //每一维单独生成三次样条
    // std::vector<double> normx;
    // for(int i=0;i<jtraj.size();i++){
    //     normx.push_back(1.0/jtraj.size());
    // }
    // threeSpline myS;
    // std::vector<double> normy;
    // myS.setwx(normx);
    // for(int i=0;i<3;i++){
    //     normy.clear();
    //     for(int j=0;j<jtraj.size();i++){
    //         normy.push_back(TrajEndPose[j](i));
    //     }
    //     myS.setwy(normy);
    //     if(i==0)
    //         myS.interpolation(rStep, zeros, resx);
    //     else if(i==1)
    //         myS.interpolation(rStep, zeros, resy);
    //     else if(i==2)
    //         myS.interpolation(rStep, zeros, resz);
    // }

    //直接采用moveit自带函数
    std::vector<geometry_msgs::Pose> waypoints;

    for(auto conf:jtraj){
        Eigen::Matrix4d endP=curRob->forwardKineEnd(conf);
        Eigen::Matrix3d endR=endP.block<3,3>(0,0);
        Eigen::Vector3d endT=endP.block<3,1>(0,3);
        geometry_msgs::Pose pose;
        pose.position.x=endT(0);pose.position.y=endT(1);pose.position.z=endT(2);
        Eigen::Quaterniond qq(endR);
        pose.orientation.x=qq.x();pose.orientation.y=qq.y();pose.orientation.z=qq.z();pose.orientation.w=qq.w();
        waypoints.push_back(pose);
    }
    moveit_msgs::RobotTrajectory myTraj;
    double fraction=0;
    while(fraction<1){
        fraction= curRob->getarm()->computeCartesianPath(waypoints, step, 0.0, myTraj, true);
        //std::cout<<fraction<<std::endl;
    }
    std::vector<robotConfig>resConf;
    for(int k=0;k<myTraj.joint_trajectory.points.size();k++){
        std::vector<double>joint_goal;
        for(int i=0; i<6; i++)
        {
            joint_goal.push_back(myTraj.joint_trajectory.points[k].positions[i]);
        }
        robotConfig jT(joint_goal);
        resConf.push_back(jT);
    }
    return resConf;
}

std::vector<robotConfig> robotTraj::Cutting3To(double norm1Off){
    std::cout<<"here"<<std::endl;
    std::vector<robotConfig> safeTraj = CuttingTo(0.01);
    robotTraj safeT(safeTraj);
    CubicSplineInterpolation mySpline(safeT);
    std::cout<<"out here"<<std::endl;
    return mySpline.GetInterpolatedPoints(norm1Off);
}

std::vector<robotConfig> robotTraj::Diliting3(){
    return Cutting3To(0.01);
}
std::vector<robotConfig> robotTraj::Slow_Cutting3To(double norm1Off){
    static int fileCounter = 1; 
    std::vector<robotConfig> curDliT;
    std::string directoryPath = "";
    std::string fileName = directoryPath + "output" + std::to_string(fileCounter) + ".txt";

    // 打开文件
    std::ofstream outputFile(fileName);
    // std::streambuf* coutBuf = std::cout.rdbuf(outputFile.rdbuf());
    std::vector<robotConfig> curT = CuttingTo(1);
    robotTraj curTrajh(curT); 
    if (outputFile.is_open()) {
        // 重定向cout到文件
        // 定义一个缓冲区来存储当前工作目录
        char buffer[PATH_MAX];

        // 获取当前工作目录
        if (getcwd(buffer, sizeof(buffer)) != nullptr) {
            std::cout << "Current working directory: " << buffer << std::endl;
        } else {
            std::cerr << "Failed to get current working directory." << std::endl;
        }
        std::streambuf* coutBuf = std::cout.rdbuf(outputFile.rdbuf());

        // 输出你的内容
        std::cout << "Key frame is" << std::endl;
        for (auto rc : curT) {
            for (int i = 0; i < 6; i++) {
                std::cout << rc.getAngle(i) << ' ';
            }
            std::cout << std::endl;
        }

        CubicSplineInterpolation mySpline(curTrajh);
        curDliT = mySpline.GetTrapInterpolatedPoints(norm1Off);
        std::cout << "Whole Traj is" << std::endl;
        for (auto rc : curDliT) {
            for (int i = 0; i < 6; i++) {
                std::cout << rc.getAngle(i) << ' ';
            }
            std::cout << std::endl;
        }

        // 恢复cout的原始缓冲区
        std::cout.rdbuf(coutBuf);

        // 关闭文件
        outputFile.close();

        fileCounter++; // 递增文件计数器，以便下次生成不同的文件名
    } else {
        std::cerr << "Failed to open file for writing: " << fileName << std::endl;
    }
    return curDliT;
}

JAKArobot::JAKArobot(ros::NodeHandle &nh):nodeh(nh){radius={0.065,0.07,0.06,0.06,0.05,0.03};height={0.2,0.425,0.24,0.15,0.21,0.18};isSimulator=true;action_pub=nodeh.advertise<sensor_msgs::JointState>("robot_moveit_action", 10);std::cout<<"Simulation"<<std::endl;
traj_pub=nodeh.advertise<std_msgs::Float64MultiArray>("/activeTraj", 10);}
JAKArobot::JAKArobot(ros::NodeHandle &nh,moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP):nodeh(nh){
    radius={0.065,0.07,0.06,0.06,0.05,0.03};height={0.2,0.425,0.24,0.15,0.21,0.18};
    this->arm_=&arm;
    kinematic_state=arm_->getCurrentState();
    joint_model_group=arm_->getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    isSimulator=true;
    action_pub=nodeh.advertise<sensor_msgs::JointState>("robot_moveit_action", 10);
    std::cout<<"Simulation"<<std::endl;
    traj_pub=nodeh.advertise<std_msgs::Float64MultiArray>("/activeTraj", 10);
    updateCurConfig();
    pthread_t robot_pose;
    // 仿真取代/joint_state_publisher而新开线程发布机械臂状态
    pthread_create(&robot_pose, NULL, Fake_State_Thread, this);
}
JAKArobot::JAKArobot(ros::NodeHandle &nh,moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP,const char*robotip):nodeh(nh){
    radius={0.065,0.07,0.06,0.06,0.05,0.03};height={0.2,0.425,0.24,0.15,0.21,0.18};
    this->arm_=&arm;
    kinematic_state=arm_->getCurrentState();
    joint_model_group=arm_->getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    action_pub=nodeh.advertise<sensor_msgs::JointState>("robot_moveit_action", 10);
    isSimulator=false;
    JointValue joint_init;
    std::cout << "Connect robot IP: " << robotip << std::endl;//用于实际机械臂连接
    std::string robot_IP=robotip;
    if(!robot.login_in(robot_IP.c_str()))
    {
        if(!robot.power_on())
        {
            if(!robot.enable_robot())
            {
                pthread_t robot_pose;
                Robot_State_Thread_Flag = true;

                std::cout << "Robot connect!" << std::endl;
                Robot_Connect_Flag = true;

                robot.get_joint_position(&joint_init);
                usleep(100 * 1000);

                // 使Rviz机械臂状态为真实机械臂状态
                RVIZ_Joint_Init(joint_init);
                updateCurConfig();
                // 取代/joint_state_publisher而新开线程发布机械臂状态
                pthread_create(&robot_pose, NULL, Robot_State_Thread, NULL);
            }
        }
    }
    else
    {
        std::cout << "Cannot connect robot!" << std::endl;
        exit(-1);
    }
    traj_pub=nodeh.advertise<std_msgs::Float64MultiArray>("/activeTraj", 10);
}

void JAKArobot::enableRobot(){
    robot.servo_move_use_none_filter();
    robot.servo_move_enable(true);
}
void JAKArobot::forbidRobot(){
    robot.servo_move_enable(false);
}
void JAKArobot::getCurrentJointValue(JointValue &jv)
{
    robot.get_joint_position(&jv);
}
void JAKArobot::pubJointState(sensor_msgs::JointState js){
    action_pub.publish(js);
}
void JAKArobot::pubTraj(std_msgs::Float64MultiArray fm){
    traj_pub.publish(fm);
}
void JAKArobot::servojoint(JointValue & joint_goal){
    robot.servo_j(&joint_goal, ABS,2);
}
void JAKArobot::keepStatic(robotConfig staticConf){
    ros::Rate looprate(20);
    sensor_msgs::JointState joint_states;
    for(int i=0; i<6; i++)
    {
        int j = i+1;
        joint_states.name.push_back("joint_"+ std::to_string(j));
    }
    while(ros::ok()){
        if (!Robot_Move_Flag){
            joint_states.position.clear();
            joint_states.header.stamp = ros::Time::now();
                
            for(int i = 0; i < 6; i++)
            {
                joint_states.position.push_back(staticConf.getAngle(i));
            }
            pubJointState(joint_states);
        }
        else{
            JointValue joint_now;
            getCurrentJointValue(joint_now);
            joint_states.position.clear();
            joint_states.header.stamp = ros::Time::now();
                
            for(int i = 0; i < 6; i++)
            {
                joint_states.position.push_back(staticConf.getAngle(i));
            }
            pubJointState(joint_states);
        }
    }
}
void JAKArobot::pubCurJointStates(){
    sensor_msgs::JointState joint_states;
    for(int i=0; i<6; i++)
    {
        int j = i+1;
        joint_states.name.push_back("joint_"+ std::to_string(j));
    }
    joint_states.position.clear();
    joint_states.header.stamp = ros::Time::now();
        
    for(int i = 0; i < 6; i++)
    {
        joint_states.position.push_back(curConfig.getAngle(i));
    }
    pubJointState(joint_states);
    
    //std::cout<<"watch dog!!!"<<std::endl;
}
std::vector<geometry_msgs::PoseStamped> JAKArobot::axis_pose(robotConfig targetValue){
    kinematic_state->setJointGroupPositions(joint_model_group,  targetValue.getAngle());
    std::vector<Eigen::Affine3d> manipPos;
	const Eigen::Affine3d & Link1pos = kinematic_state->getGlobalLinkTransform("link_1");
    manipPos.push_back(Link1pos);
    const Eigen::Affine3d & Link2pos = kinematic_state->getGlobalLinkTransform("link_2");
    manipPos.push_back(Link2pos);
    const Eigen::Affine3d & Link3pos = kinematic_state->getGlobalLinkTransform("link_3");
    manipPos.push_back(Link3pos);
    const Eigen::Affine3d & Link4pos = kinematic_state->getGlobalLinkTransform("link_4");
    manipPos.push_back(Link4pos);
    const Eigen::Affine3d & Link5pos = kinematic_state->getGlobalLinkTransform("link_5");
    manipPos.push_back(Link5pos);
    const Eigen::Affine3d & Link6pos = kinematic_state->getGlobalLinkTransform("link_6");
    manipPos.push_back(Link6pos);

    //cout<<"setPosOfCylinder!!!!!"<<endl;
    std::vector<geometry_msgs::PoseStamped> resAxis;
    for(int id=0;id<6;id++){
        Eigen::Matrix3d Rota=manipPos[id].rotation();
        Eigen::Vector3d transl=manipPos[id].translation();
        //fcl固定在中心，需要移动到末端执行器
        Eigen::Matrix3d real_Rota;
        Eigen::Vector3d real_transl;
        //圆柱位姿调整
        if(id==0)
        {
            real_Rota=Rota;
            real_transl=transl;
        }
        else if(id==1)
        {
            Eigen::Matrix3d Rota_offset;
            Rota_offset<<0,0,1,
                        0,1,0,
                        -1,0,0;
            real_Rota=Rota*Rota_offset;
            Eigen::Vector3d offset((radius[0]+radius[1])/2.0+0.076,0,height[1]/2);
            real_transl=real_Rota*offset+transl;
        }    
        else if(id==2)
        {
            Eigen::Matrix3d Rota_offset;
            Rota_offset<<0,0,1,
                        0,1,0,
                        -1,0,0;
            real_Rota=Rota*Rota_offset;
            //std::cout<<"id "<<id<<real_Rota<<std::endl;
            Eigen::Vector3d offset(0,0,height[2]/2.0);
            real_transl=real_Rota*offset+transl;
        } 
        else if(id==3)
        {
            Eigen::Matrix3d Rota_offset;
            Rota_offset<<1,0,0,
                        0,1,0,
                        0,0,1;
            real_Rota=Rota*Rota_offset;
            Eigen::Vector3d offset(0,0,height[3]/2.0);
            //std::cout<<"id "<<id<<real_Rota<<std::endl;
            real_transl=real_Rota*offset+transl;
        } 
        else if(id==4)
        {
            real_Rota=Rota;
            Eigen::Vector3d offset(0,0,-height[4]/2.0);
            real_transl=real_Rota*offset+transl;
        }
        else if(id==5)
        {
            real_Rota=Rota;
            Eigen::Vector3d offset(0,0,-height[5]/2.0);
            real_transl=real_Rota*offset+transl;
        }

        geometry_msgs::PoseStamped axis_p;
        axis_p.header.frame_id = "world";
        axis_p.pose.position.x = real_transl(0);
        axis_p.pose.position.y = real_transl(1);
        axis_p.pose.position.z = real_transl(2);
        Eigen::Quaterniond quat(real_Rota);
        axis_p.pose.orientation.x = quat.x();
        axis_p.pose.orientation.y = quat.y();
        axis_p.pose.orientation.z = quat.z();
        axis_p.pose.orientation.w = quat.w();

        resAxis.push_back(axis_p);
    }
    return resAxis;
}

std::vector<fcl::CollisionObjectd> JAKArobot::forwardKine(robotConfig targetValue){
    kinematic_state->setJointGroupPositions(joint_model_group,  targetValue.getAngle());
    std::vector<Eigen::Affine3d> manipPos;
	const Eigen::Affine3d & Link1pos = kinematic_state->getGlobalLinkTransform("link_1");
    manipPos.push_back(Link1pos);
    const Eigen::Affine3d & Link2pos = kinematic_state->getGlobalLinkTransform("link_2");
    manipPos.push_back(Link2pos);
    const Eigen::Affine3d & Link3pos = kinematic_state->getGlobalLinkTransform("link_3");
    manipPos.push_back(Link3pos);
    const Eigen::Affine3d & Link4pos = kinematic_state->getGlobalLinkTransform("link_4");
    manipPos.push_back(Link4pos);
    const Eigen::Affine3d & Link5pos = kinematic_state->getGlobalLinkTransform("link_5");
    manipPos.push_back(Link5pos);
    const Eigen::Affine3d & Link6pos = kinematic_state->getGlobalLinkTransform("link_6");
    manipPos.push_back(Link6pos);

    std::shared_ptr<fcl::Cylinder<double>> Link1Cylinder = std::make_shared<fcl::Cylinder<double>>(radius[0],height[0]);
    std::shared_ptr<fcl::Cylinder<double>> Link2Cylinder = std::make_shared<fcl::Cylinder<double>>(radius[1],height[1]);
    std::shared_ptr<fcl::Cylinder<double>> Link3Cylinder = std::make_shared<fcl::Cylinder<double>>(radius[2],height[2]);
    std::shared_ptr<fcl::Cylinder<double>> Link4Cylinder = std::make_shared<fcl::Cylinder<double>>(radius[3],height[3]);
    std::shared_ptr<fcl::Cylinder<double>> Link5Cylinder = std::make_shared<fcl::Cylinder<double>>(radius[4],height[4]);
    std::shared_ptr<fcl::Cylinder<double>> Link6Cylinder = std::make_shared<fcl::Cylinder<double>>(radius[5],height[5]);
    //cout<<"setPosOfCylinder!!!!!"<<endl;
    std::vector<fcl::CollisionObjectd> resCylinders;
    for(int id=0;id<6;id++){
        Eigen::Matrix3d Rota=manipPos[id].rotation();
        Eigen::Vector3d transl=manipPos[id].translation();
        //fcl固定在中心，需要移动到末端执行器
        Eigen::Matrix3d real_Rota;
        Eigen::Vector3d real_transl;
        //圆柱位姿调整
        if(id==0)
        {
            real_Rota=Rota;
            real_transl=transl;
            fcl::CollisionObjectd objtmp(Link1Cylinder);
            objtmp.setTranslation(real_transl); 
            objtmp.setRotation(real_Rota);
            resCylinders.push_back(objtmp);
        }
        else if(id==1)
        {
            Eigen::Matrix3d Rota_offset;
            Rota_offset<<0,0,1,
                        0,1,0,
                        -1,0,0;
            real_Rota=Rota*Rota_offset;
            Eigen::Vector3d offset((radius[0]+radius[1])/2.0+0.076,0,height[1]/2.0);
            real_transl=real_Rota*offset+transl;
            fcl::CollisionObjectd objtmp(Link2Cylinder);
            objtmp.setTranslation(real_transl); 
            objtmp.setRotation(real_Rota);
            resCylinders.push_back(objtmp);
        }    
        else if(id==2)
        {
            Eigen::Matrix3d Rota_offset;
            Rota_offset<<0,0,1,
                        0,1,0,
                        -1,0,0;
            real_Rota=Rota*Rota_offset;
            //std::cout<<"id "<<id<<real_Rota<<std::endl;
            Eigen::Vector3d offset(0,0,height[2]/2.0);
            real_transl=real_Rota*offset+transl;
            fcl::CollisionObjectd objtmp(Link3Cylinder);
            objtmp.setTranslation(real_transl); 
            objtmp.setRotation(real_Rota);
            resCylinders.push_back(objtmp);
        } 
        else if(id==3)
        {
            Eigen::Matrix3d Rota_offset;
            Rota_offset<<1,0,0,
                        0,1,0,
                        0,0,1;
            real_Rota=Rota*Rota_offset;
            Eigen::Vector3d offset(0,0,height[3]/2.0);
            //std::cout<<"id "<<id<<real_Rota<<std::endl;
            real_transl=real_Rota*offset+transl;
            fcl::CollisionObjectd objtmp(Link4Cylinder);
            objtmp.setTranslation(real_transl); 
            objtmp.setRotation(real_Rota);
            resCylinders.push_back(objtmp);
        } 
        else if(id==4)
        {
            real_Rota=Rota;
            Eigen::Vector3d offset(0,0,-height[4]/2.0);
            real_transl=real_Rota*offset+transl;
            fcl::CollisionObjectd objtmp(Link5Cylinder);
            objtmp.setTranslation(real_transl); 
            objtmp.setRotation(real_Rota);
            resCylinders.push_back(objtmp);
        }
        else if(id==5)
        {
            real_Rota=Rota;
            Eigen::Vector3d offset(0,0,-height[5]/2.0);
            real_transl=real_Rota*offset+transl;
            fcl::CollisionObjectd objtmp(Link6Cylinder);
            objtmp.setTranslation(real_transl); 
            objtmp.setRotation(real_Rota);
            resCylinders.push_back(objtmp);
        }
    }
    return resCylinders;
}
//逆运动学接口要很好，鲁棒，尽量解析呀
robotConfig JAKArobot::inverseKine(Eigen::Matrix4d endPose, robotConfig relatedConf, bool & isFind){
    Eigen::Affine3d affine(endPose);
    Eigen::Quaterniond quaternion(affine.rotation());

    geometry_msgs::Pose pose;
    pose.position.x = affine.translation().x();
    pose.position.y = affine.translation().y();
    pose.position.z = affine.translation().z();
    pose.orientation.x = quaternion.x();
    pose.orientation.y = quaternion.y();
    pose.orientation.z = quaternion.z();
    pose.orientation.w = quaternion.w();
    
    // // Load the robot model
    // robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
    // robot_model::RobotModelPtr robot_model = robot_model_loader.getModel();

    // // Create a robot state
    // robot_state::RobotStatePtr robot_state(new robot_state::RobotState(robot_model));

    // // Get the joint model group for the arm you're interested in
    // const robot_model::JointModelGroup* arm_joint_model_group = robot_model->getJointModelGroup("arm");

    // Create the IK solver
    const kinematics::KinematicsBaseConstPtr kinematics_solver = joint_model_group->getSolverInstance();
    if (!kinematics_solver)
    {
        ROS_ERROR("Failed to initialize the IK solver.");
        return {};
    }

    // Set the desired pose in the robot state
    isFind = kinematic_state->setFromIK(joint_model_group, pose, 0.1);

    // Get the resulting joint values
    std::vector<double> joint_values;
    kinematic_state->copyJointGroupPositions(joint_model_group, joint_values);

    return robotConfig(joint_values);
}

std::vector<fcl::CollisionObjectd> JAKArobot::getCover(){
    robotConfig curjv=getConfig();
    return forwardKine(curjv);
}

void JAKArobot::smoothTraj(robotTraj curTraj,double speed){
	JointValue joint_goal, joint_now, joint_offset;
    //起点检测
    robotConfig startConf=curTraj.getStart();
    robotConfig curConf=getConfig();
    if(startConf.norm2(curConf)>0.01){
        curTraj.addFrontTraj(curConf);
    }
    std::cout<<"In smooth"<<std::endl;
    //真实机械臂
	if(Robot_Connect_Flag && !Robot_Move_Flag)
	{
		enableRobot();
		getCurrentJointValue(joint_now);
        robotConfig startc=curTraj.getStart();
        for(int i=0;i<6;i++){
            if(abs(startc.getAngle(i)-joint_now.jVal[i])>1){
                joint_offset.jVal[i]=-2*M_PI;
            }
            else{
                joint_offset.jVal[i]=0;
            }
        }
		std::cout << "Real robot trajectory set" << std::endl;

		sensor_msgs::JointState joint_states;
		joint_states.position.clear();
		joint_states.name.clear();
		
		for(int i=0; i<6; i++)
		{
			int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
		}
        // std::vector<robotConfig> dilitingTraj=curTraj.Slow_CuttingTo(0.005*speed);
        std::vector<robotConfig> dilitingTraj=curTraj.Cutting3To(0.1*speed);
        // std::cout<<"dilitingTraj size is "<<dilitingTraj.size()<<std::endl;
		for(int k=0;k<dilitingTraj.size();k++)
		{   
            //update offset
            for(int i=0; i<6; i++)
			{   if(k==0)break;
                if(dilitingTraj[k].getAngle(i)-dilitingTraj[k-1].getAngle(i)>1) joint_offset.jVal[i]-=2*M_PI;
                if(dilitingTraj[k].getAngle(i)-dilitingTraj[k-1].getAngle(i)<-1) joint_offset.jVal[i]+=2*M_PI;
			}


			for(int i=0; i<6; i++)
			{
				joint_goal.jVal[i] =dilitingTraj[k].getAngle(i)+joint_offset.jVal[i];
			}
            
			//发布轨迹消息
			std_msgs::Float64MultiArray Trajs;
			int wholesize=dilitingTraj.size();
			int jumpstep=wholesize/6;
			if(jumpstep==0){
				jumpstep=1;
			}
			int curProcess=k/jumpstep;
			if(curProcess>3){
				for(int sss=0;sss<6-curProcess;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}	
				}
                if(curProcess==6){
                    for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[dilitingTraj.size()-1].getAngle(jointv));
					}
                }
			}
			else{
				for(int sss=0;sss<3;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}
				}
			}
			//cout<<"Traj size is"<<Trajs.data.size()<<endl;
            if(Trajs.data.size()==0){
                for(int jointv=0;jointv<6;jointv++){
					Trajs.data.push_back(dilitingTraj[dilitingTraj.size()-1].getAngle(jointv));
				}
            }
			pubTraj(Trajs);
            pubJointState(joint_states);
			robot.servo_j(&joint_goal, ABS,5);
			//ros::spinOnce();
			usleep(sleepTime); // unit: us
		}
		
		robot.get_joint_position(&joint_now);
		
		joint_states.position.clear();
		joint_states.header.stamp = ros::Time::now();
		
		for(int i = 0; i < 6; i++)
        {
            joint_states.position.push_back(joint_now.jVal[i]);
        }
        
		
		robot.servo_move_enable(false);
	}
	else if (!Robot_Move_Flag)
	{
		std::cout<<"In Fake"<<std::endl;
        sensor_msgs::JointState joint_states;
		
		joint_states.position.clear();
		joint_states.name.clear();
		
		for(int i=0; i<6; i++)
		{
			int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
		}

		// std::vector<robotConfig> dilitingTraj=curTraj.Slow_CuttingTo(0.005*speed);
        std::vector<robotConfig> dilitingTraj=curTraj.Cutting3To(0.1*speed);
        std::cout<<"dilitingTraj size is "<<dilitingTraj.size()<<std::endl;
		for(int k=0;k<dilitingTraj.size();k++)
		{
			joint_states.position.clear();
			joint_states.header.stamp = ros::Time::now();
			//test
			//if(k>=200) {moveStep=2;std::cout<<"lowSpeed!";}
			
			for(int i=0; i<6; i++)
			{
				joint_goal.jVal[i] = dilitingTraj[k].getAngle(i);
				joint_states.position.push_back(joint_goal.jVal[i]);
			}
			//发布轨迹消息
			std_msgs::Float64MultiArray Trajs;
			int wholesize=dilitingTraj.size();
			int jumpstep=wholesize/6;
			if(jumpstep==0){
				jumpstep=1;
			}
			int curProcess=k/jumpstep;
			if(curProcess>3){
				for(int sss=0;sss<6-curProcess;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}	
				}
                if(curProcess==6){
                    for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[dilitingTraj.size()-1].getAngle(jointv));
					}
                }
			}
            
			else{
				for(int sss=0;sss<3;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}
				}
			}
			//cout<<"Traj size is"<<Trajs.data.size()<<endl;
            if(Trajs.data.size()==0){
                for(int jointv=0;jointv<6;jointv++){
					Trajs.data.push_back(dilitingTraj[dilitingTraj.size()-1].getAngle(jointv));
				}
            }
			pubTraj(Trajs);
            pubJointState(joint_states);
            usleep(sleepTime); // unit: us
		}
	}
	else
	{
		std::cout << "Waiting for moveit command!" << std::endl;
	}	
}

void JAKArobot::active_smoothTraj(robotTraj curTraj, robotTraj nextTraj, double speed ){
    JointValue joint_goal, joint_now, joint_offset;
    //起点检测
    robotConfig startConf=curTraj.getStart();
    robotConfig curConf=getConfig();
    if(startConf.norm2(curConf)>0.01){
        curTraj.addFrontTraj(curConf);
    }
    //真实机械臂
	if(Robot_Connect_Flag && !Robot_Move_Flag)
	{
		enableRobot();
		getCurrentJointValue(joint_now);
        robotConfig startc=curTraj.getStart();
        for(int i=0;i<6;i++){
            if(abs(startc.getAngle(i)-joint_now.jVal[i])>1){
                joint_offset.jVal[i]=-2*M_PI;
            }
            else{
                joint_offset.jVal[i]=0;
            }
        }
		std::cout << "Real robot trajectory set" << std::endl;

		sensor_msgs::JointState joint_states;
		joint_states.position.clear();
		joint_states.name.clear();
		
		for(int i=0; i<6; i++)
		{
			int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
		}
        std::vector<robotConfig> dilitingTraj=curTraj.Slow_CuttingTo(0.005*speed);
        // std::vector<robotConfig> dilitingTraj=curTraj.Slow_Cutting3To(0.1*speed);
        std::vector<robotConfig> nextDtraj = nextTraj.Slow_CuttingTo(0.005*speed);
        // std::vector<robotConfig> nextDtraj=nextTraj.Slow_Cutting3To(0.1*speed);
        // std::cout<<"dilitingTraj size is "<<dilitingTraj.size()<<std::endl;
		for(int k=0;k<dilitingTraj.size();k++)
		{   
            //update offset
            
            for(int i=0; i<6; i++)
			{   if(k==0)break;
                if(dilitingTraj[k].getAngle(i)-dilitingTraj[k-1].getAngle(i)>1) joint_offset.jVal[i]-=2*M_PI;
                if(dilitingTraj[k].getAngle(i)-dilitingTraj[k-1].getAngle(i)<-1) joint_offset.jVal[i]+=2*M_PI;
			}


			for(int i=0; i<6; i++)
			{
				joint_goal.jVal[i] =dilitingTraj[k].getAngle(i)+joint_offset.jVal[i];
			}
            
			//发布轨迹消息
			std_msgs::Float64MultiArray Trajs;
			int wholesize=dilitingTraj.size();
            int wholeProcess = wholesize/125;
			int jumpstep=125;
			if(jumpstep==0){
				jumpstep=1;
			}
			int curProcess=k/jumpstep;
			if(wholeProcess-curProcess<3){
				for(int sss=0;sss<6-curProcess;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}	
				}
                for(int sss=0;sss<curProcess-3;sss++){
                    for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(nextDtraj[(sss+1)*jumpstep].getAngle(jointv));
					}	
                }
			}
            
			else{
				for(int sss=0;sss<3;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}
				}
			}
			//cout<<"Traj size is"<<Trajs.data.size()<<endl;
            if(Trajs.data.size()==0){
                for(int jointv=0;jointv<6;jointv++){
					Trajs.data.push_back(dilitingTraj[dilitingTraj.size()-1].getAngle(jointv));
				}
            }
			pubTraj(Trajs);
            pubJointState(joint_states);
			robot.servo_j(&joint_goal, ABS,2);
			//ros::spinOnce();
			usleep(sleepTime); // unit: us

            
		}
		
		robot.get_joint_position(&joint_now);
		
		joint_states.position.clear();
		joint_states.header.stamp = ros::Time::now();
		
		for(int i = 0; i < 6; i++)
        {
            joint_states.position.push_back(joint_now.jVal[i]);
        }
        
		
		robot.servo_move_enable(false);
	}
	else if (!Robot_Move_Flag)
	{
		sensor_msgs::JointState joint_states;
		
		joint_states.position.clear();
		joint_states.name.clear();
		
		for(int i=0; i<6; i++)
		{
			int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
		}

		std::vector<robotConfig> dilitingTraj=curTraj.Slow_CuttingTo(0.005*speed);
        // std::vector<robotConfig> dilitingTraj=curTraj.Slow_Cutting3To(0.1*speed);
        std::vector<robotConfig> nextDtraj = nextTraj.Slow_CuttingTo(0.005*speed);
        // std::vector<robotConfig> nextDtraj=nextTraj.Slow_Cutting3To(0.1*speed);
        // std::cout<<"dilitingTraj size is "<<dilitingTraj.size()<<std::endl;
		for(int k=0;k<dilitingTraj.size();k++)
		{
			auto start_time = std::chrono::high_resolution_clock::now();
            joint_states.position.clear();
			joint_states.header.stamp = ros::Time::now();
			//test
			//if(k>=200) {moveStep=2;std::cout<<"lowSpeed!";}
			
			for(int i=0; i<6; i++)
			{
				joint_goal.jVal[i] = dilitingTraj[k].getAngle(i);
				joint_states.position.push_back(joint_goal.jVal[i]);
			}
			//发布轨迹消息
			std_msgs::Float64MultiArray Trajs;
			int wholesize=dilitingTraj.size();
            int wholeProcess = wholesize/125;
			int jumpstep=125;
			if(jumpstep==0){
				jumpstep=1;
			}
			int curProcess=k/jumpstep;
			if(wholeProcess-curProcess<3){
				for(int sss=0;sss<6-curProcess;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}	
				}
                for(int sss=0;sss<curProcess-3;sss++){
                    for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(nextDtraj[(sss+1)*jumpstep].getAngle(jointv));
					}	
                }
			}
            
			else{
				for(int sss=0;sss<3;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}
				}
			}
			//cout<<"Traj size is"<<Trajs.data.size()<<endl;
            if(Trajs.data.size()==0){
                for(int jointv=0;jointv<6;jointv++){
					Trajs.data.push_back(dilitingTraj[dilitingTraj.size()-1].getAngle(jointv));
				}
            }
			pubTraj(Trajs);
            pubJointState(joint_states);
            usleep(sleepTime); // unit: us

		}
	}
	else
	{
		std::cout << "Waiting for moveit command!" << std::endl;
	}	
}
void JAKArobot::singleExecute(robotConfig tarpose){
	JointValue joint_goal, joint_now;
	if(Robot_Connect_Flag && !Robot_Move_Flag)
	{
		sensor_msgs::JointState joint_states;
		joint_states.position.clear();
		joint_states.name.clear();	
		for(int i=0; i<6; i++)
		{
			int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
            joint_goal.jVal[i] =tarpose.getAngle(i);
		}
        //robot.servo_j(&joint_goal, ABS);
        robot.get_joint_position(&joint_now);
        for(int i=0;i<6;i++){
            if(abs(joint_now.jVal[i]-joint_goal.jVal[i])>1){
                joint_goal.jVal[i]-=2*M_PI;
            }
        }
        robot.servo_j(&joint_goal, ABS,2);
        // for(int i=0;i<6;i++){
        //     std::cout<<joint_goal.jVal[i]<<std::endl;
        // }
        std::cout<<std::endl;
        robot.get_joint_position(&joint_now);
		joint_states.position.clear();
		joint_states.header.stamp = ros::Time::now();
		for(int i = 0; i < 6; i++)
        {
            joint_states.position.push_back(joint_now.jVal[i]);
        }
		action_pub.publish(joint_states);
	}
	else if (!Robot_Move_Flag)
	{
		sensor_msgs::JointState joint_states;
		
		joint_states.position.clear();
		joint_states.name.clear();
		
		for(int i=0; i<6; i++)
		{
			int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
		}
        joint_states.position.clear();
		joint_states.header.stamp = ros::Time::now();
        for(int i=0; i<6; i++)
        {
            joint_states.position.push_back(tarpose.getAngle(i));
        }
        action_pub.publish(joint_states);
	}
	else
	{
		std::cout << "Waiting for moveit command!" << std::endl;
	}	
    //usleep(sleepTime);
}

void JAKArobot::singleExecute(Eigen::Matrix4d endPose, robotConfig ref_Config){
    bool isFind;
    robotConfig ikConf=inverseKine(endPose, ref_Config, isFind);
    if(isFind){
        singleExecute(ikConf);
    }
    
}
//please use smoothTraj
void JAKArobot::executeTraj(robotTraj curTraj,double speed){
    JointValue joint_goal, joint_now, joint_offset;
    //起点检测
    robotConfig startConf=curTraj.getStart();
    robotConfig curConf=getConfig();
    if(startConf.norm2(curConf)>0.01){
        curTraj.addFrontTraj(curConf);
    }
    //真实机械臂
	if(Robot_Connect_Flag && !Robot_Move_Flag)
	{
		enableRobot();
		getCurrentJointValue(joint_now);
        robotConfig startc=curTraj.getStart();
        for(int i=0;i<6;i++){
            if(abs(startc.getAngle(i)-joint_now.jVal[i])>1){
                joint_offset.jVal[i]=-2*M_PI;
            }
            else{
                joint_offset.jVal[i]=0;
            }
        }
		std::cout << "Real robot trajectory set" << std::endl;

		sensor_msgs::JointState joint_states;
		joint_states.position.clear();
		joint_states.name.clear();
		
		for(int i=0; i<6; i++)
		{
			int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
		}
        std::vector<robotConfig> dilitingTraj=curTraj.CuttingTo(0.005*speed);
        // std::cout<<"dilitingTraj size is "<<dilitingTraj.size()<<std::endl;
		for(int k=0;k<dilitingTraj.size();k++)
		{   
            //update offset
            for(int i=0; i<6; i++)
			{   if(k==0)break;
                if(dilitingTraj[k].getAngle(i)-dilitingTraj[k-1].getAngle(i)>1) joint_offset.jVal[i]-=2*M_PI;
                if(dilitingTraj[k].getAngle(i)-dilitingTraj[k-1].getAngle(i)<-1) joint_offset.jVal[i]+=2*M_PI;
			}


			for(int i=0; i<6; i++)
			{
				joint_goal.jVal[i] =dilitingTraj[k].getAngle(i)+joint_offset.jVal[i];
			}
            
			//发布轨迹消息
			std_msgs::Float64MultiArray Trajs;
			int wholesize=dilitingTraj.size();
			int jumpstep=wholesize/10;
			if(jumpstep==0){
				jumpstep=1;
			}
			int curProcess=k/jumpstep;
			if(curProcess>7){
				for(int sss=0;sss<10-curProcess;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}	
				}
			}
			else{
				for(int sss=0;sss<3;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}
				}
			}
			//cout<<"Traj size is"<<Trajs.data.size()<<endl;
			pubTraj(Trajs);
            pubJointState(joint_states);
			robot.servo_j(&joint_goal, ABS,2);
			//ros::spinOnce();
			usleep(sleepTime); // unit: us
		}
		
		robot.get_joint_position(&joint_now);
		
		joint_states.position.clear();
		joint_states.header.stamp = ros::Time::now();
		
		for(int i = 0; i < 6; i++)
        {
            joint_states.position.push_back(joint_now.jVal[i]);
        }
        
		
		robot.servo_move_enable(false);
	}
	else if (!Robot_Move_Flag)
	{
		sensor_msgs::JointState joint_states;
		
		joint_states.position.clear();
		joint_states.name.clear();
		
		for(int i=0; i<6; i++)
		{
			int j = i+1;
            joint_states.name.push_back("joint_"+ std::to_string(j));
		}

		std::vector<robotConfig> dilitingTraj=curTraj.CuttingTo(0.005*speed);
        // std::cout<<"dilitingTraj size is "<<dilitingTraj.size()<<std::endl;
		for(int k=0;k<dilitingTraj.size();k++)
		{
			joint_states.position.clear();
			joint_states.header.stamp = ros::Time::now();
			//test
			//if(k>=200) {moveStep=2;std::cout<<"lowSpeed!";}
			
			for(int i=0; i<6; i++)
			{
				joint_goal.jVal[i] = dilitingTraj[k].getAngle(i);
				joint_states.position.push_back(joint_goal.jVal[i]);
			}
			//发布轨迹消息
			std_msgs::Float64MultiArray Trajs;
			int wholesize=dilitingTraj.size();
			int jumpstep=wholesize/10;
			if(jumpstep==0){
				jumpstep=1;
			}
			int curProcess=k/jumpstep;
			if(curProcess>7){
				for(int sss=0;sss<10-curProcess;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}	
				}
			}
			else{
				for(int sss=0;sss<3;sss++){
					for(int jointv=0;jointv<6;jointv++){
						Trajs.data.push_back(dilitingTraj[k+sss*jumpstep].getAngle(jointv));
					}
				}
			}
			//cout<<"Traj size is"<<Trajs.data.size()<<endl;
			pubTraj(Trajs);
            pubJointState(joint_states);
            usleep(sleepTime); // unit: us
		}
	}
	else
	{
		std::cout << "Waiting for moveit command!" << std::endl;
	}	
}

void JAKArobot::goToConf(robotConfig targetConf,double speed){
    robotConfig curC=getConfig();
    robotTraj sjTraj(curC, targetConf);
    smoothTraj(sjTraj,speed);
}

void JAKArobot::gohome(double speed){
    std::vector<double>initNode={0.292701,1.513222,1.541136,1.659464,-1.568797,1.863675};
    robotConfig endc(initNode);
    goToConf(endc,speed);
}

void collideJudge::generateBoxesFromOctomap(fcl::OcTree<double>& tree)
    {
        boxes.clear();  
        std::vector<std::array<double, 6> > boxes_ = tree.toBoxes();
        // std::cout<<"boxes_ is"<<boxes_.size()<<std::endl;
        for(std::size_t i = 0; i < boxes_.size(); ++i)
        {
            double x = boxes_[i][0];
            double y = boxes_[i][1];
            double z = boxes_[i][2]-0.76;
            double size = boxes_[i][3];
            double cost = boxes_[i][4];
            double threshold = boxes_[i][5];
            //std::cout<<x<<' '<<y<<' '<<z<<' '<<size<<std::endl;
            fcl::Box<double>* box = new fcl::Box<double>(size, size, size);
            box->cost_density = cost;
            box->threshold_occupied = threshold;
            fcl::CollisionObject<double>* obj = new fcl::CollisionObject<double>(std::shared_ptr<fcl::CollisionGeometry<double>>(box), fcl::Transform3<double>(fcl::Translation3<double>(fcl::Vector3<double>(x, y, z))));
            boxes.push_back(obj);
        }
        
    }
bool collideJudge::collideCheck(const fcl::CollisionObjectd& obj){
    std::vector<fcl::CollisionObjectd> CollisionLink= jrob->forwardKine(jrob->getConfig());
    //球体check
    // Perform collision check
    for(int i=0; i<6; i++){
        fcl::CollisionRequestd request;
        fcl::CollisionResultd result;
        std::cout<<"Link "<<i<<": Translation"<<CollisionLink[i].getTranslation()<<std::endl;
        std::cout<<"Link "<<i<<": Rotation"<<std::endl;
        std::cout<<CollisionLink[i].getRotation()<<std::endl;
        fcl::collide(&obj, &CollisionLink[i], request, result);
        if (result.isCollision()) {
            return false;
        }
    }
    // Check if a collision occurred
    return true;
}
bool collideJudge::collideLogical(robotConfig tarpose, bool isDis){
    int collideTimes = 0 ;
    //自身碰撞包络
    std::vector<fcl::CollisionObjectd> CollisionLink= jrob->forwardKine(tarpose);
    //std::cout<<"2 transl = "<<CollisionLink[2].getRotation()<<std::endl;
    //std::cout<<"4 transl = "<<CollisionLink[4].getRotation()<<std::endl;
    //std::cout<<CollisionLink[1].getTranslation()<<std::endl;
    //地面碰撞包络
    std::shared_ptr<fcl::Halfspace<double>> groundPlane = std::make_shared<fcl::Halfspace<double>>(fcl::Vector3<double>(0,0,1),-0.74);
    fcl::CollisionObjectd groundObj(groundPlane);
    for(int i=0;i<6;i++){
        //自身碰撞检测
        for(int j=i+2;j<6;j++){
            fcl::CollisionRequestd myRe;
            fcl::CollisionResultd result;
            fcl::collide(&CollisionLink[i], &CollisionLink[j], myRe, result);
            if (result.isCollision()) {
                //std::cout<<i<<" and "<<j<<" collide!!!"<<std::endl;
                //std::cout<<"i transl = "<<CollisionLink[i].getTranslation()<<std::endl;
                //std::cout<<"j transl = "<<CollisionLink[j].getTranslation()<<std::endl;
                return false;
            } 
        }
        fcl::CollisionRequestd myRe;
        fcl::CollisionResultd result;
        //地面碰撞检测
        if(i!=0) {
            fcl::collide(&CollisionLink[i], &groundObj, myRe, result);
            if (result.isCollision()) {
                //std::cout<<i<<" and ground collide!!!"<<std::endl;
                return false;
            } 
        }
        //Octomap碰撞检测
        if(!isDis){
            // if(boxes.size()!=0){
            //     for(std::size_t k = 0; k < boxes.size(); ++k){
            //         for(int i=0;i<6;i++){
            //             fcl::DistanceRequestd requestd;
            //             fcl::DistanceResultd resultd;
            //             double dis=0;
            //             fcl::distance(&*boxes[k], &CollisionLink[i],requestd ,resultd);
            //             dis = resultd.min_distance;
            //             //fcl::collide(&*boxes[k], &CollisionLink[i], myRe, result);
            //             if (dis< 0.05) {
            //                 //std::cout<<"Link "<<i<<" Collide"<<std::endl;
            //                 return false;
            //             } 
            //         }  
            //     }
            // }
            if(boxes.size()!=0){
                for(std::size_t k = 0; k < boxes.size(); ++k){
                    // fcl::DistanceRequestd requestd;
                    // fcl::DistanceResultd resultd;
                    fcl::CollisionRequestd myRe;
                    fcl::CollisionResultd result;
                    // double dis=0;
                    // fcl::distance(&*boxes[k], &CollisionLink[i],requestd ,resultd);
                    // dis = resultd.min_distance;
                    fcl::collide(&*boxes[k], &CollisionLink[i], myRe, result);
                    if (result.isCollision()) {
                        //std::cout<<"Link "<<i<<" Collide"<<std::endl;
                        collideTimes++;
                        if(collideTimes>7)
                            return false;
                    }  
                }
            }
        }
        else{
            if(boxes.size()!=0){
                for(std::size_t k = 0; k < boxes.size(); ++k){
                    fcl::DistanceRequestd requestd;
                    fcl::DistanceResultd resultd;
                    // fcl::CollisionRequestd myRe;
                    // fcl::CollisionResultd result;
                    double dis=0;
                    fcl::distance(&*boxes[k], &CollisionLink[i],requestd ,resultd);
                    dis = resultd.min_distance;
                    // fcl::collide(&*boxes[k], &CollisionLink[i], myRe, result);
                    if (dis<0.04) {
                        //std::cout<<"Link "<<i<<" Collide"<<std::endl;
                        collideTimes++;
                        if(collideTimes>7)
                            return false;
                        
                    } 
                }
            }
        }
        
        
    }
    return true;
}

bool collideJudge::collideTraj(robotTraj mytraj, bool isDis){
    std::vector<robotConfig> dilitedPath= mytraj.CuttingTo(1);
    for(auto conf : dilitedPath){
        int id=0;
        if(!collideLogical(conf,isDis)){
            //std::cout<<id<<" is wrong"<<std::endl;
            return false;
        }
    }
    return true;
}
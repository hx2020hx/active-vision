#include "active_vision_control/simdetector.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "people_simulation_node");
    ros::NodeHandle nh;
    ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>("testVision", 1);
    ros::AsyncSpinner spinner(2);
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    spinner.start();    
    detector* maindetector = new detector(nh, arm, PLANNING_GROUP);
    ros::Rate looprate(0.2);
    while(ros::ok){
        maindetector->updateVision();
        Eigen::Matrix4d left_c = maindetector->getCamera(1);

        srand(time(0));

        // 生成在指定范围内的随机坐标
        // double random_x = -1.0 + static_cast<double>(rand()) / (static_cast<double>(RAND_MAX / 2.0));
        // double random_y = -1.0 + static_cast<double>(rand()) / (static_cast<double>(RAND_MAX / 2.0));
        // double random_z = -1.0 + static_cast<double>(rand()) / (static_cast<double>(RAND_MAX / 1.0));


   
        double random_x=-0.401022;
        double random_y=-0.000226235;
        double random_z=-0.310333;
        double target_x = left_c(0,3);
        double target_y = left_c(1,3);
        double target_z = left_c(2,3);

        random_x = random_x+(random_x - target_x)*0.3;
        random_y = random_y+(random_y - target_y)*0.3;
        random_z = random_z+(random_z - target_z)*0.3;
        // 创建一个可视化标记
        visualization_msgs::Marker marker;
        marker.header.frame_id = "world"; // 选择合适的坐标系
        marker.header.stamp = ros::Time::now();
        marker.ns = "points_and_lines";
        marker.id = 0;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;

        // 设置点的位置
        marker.pose.position.x = random_x;
        marker.pose.position.y = random_y;
        marker.pose.position.z = random_z;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;

        marker.scale.x = 0.1;
        marker.scale.y = 0.1;
        marker.scale.z = 0.1;

        marker.color.r = 1.0;
        marker.color.g = 0.0;
        marker.color.b = 0.0;
        marker.color.a = 1.0;

        // 发布可视化标记
        marker_pub.publish(marker);

        // 创建一个线段标记以连接两个点
        visualization_msgs::Marker line;
        line.header.frame_id = "world";
        line.header.stamp = ros::Time::now();
        line.ns = "points_and_lines";
        line.id = 1;
        line.type = visualization_msgs::Marker::LINE_STRIP;
        line.action = visualization_msgs::Marker::ADD;

        // 设置线段的两个点
        geometry_msgs::Point p1;
        p1.x = random_x;
        p1.y = random_y;
        p1.z = random_z;
        geometry_msgs::Point p2;
        p2.x = target_x;
        p2.y = target_y;
        p2.z = target_z;

        line.points.push_back(p1);
        line.points.push_back(p2);

        line.scale.x = 0.05;
        line.color.r = 0.0;
        line.color.g = 1.0;
        line.color.b = 0.0;
        line.color.a = 1.0;

        // 发布线段标记
        marker_pub.publish(line);
        std::cout<<"current point is "<<maindetector->isVisiblePoint(Eigen::Vector3d(random_x, random_y, random_z), left_c)<<std::endl;

        looprate.sleep();
    }
    
    return 0;
}

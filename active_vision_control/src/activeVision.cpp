#include "active_vision_control/activeVision.h"

ros::Publisher servo1publisher;
ros::Publisher servo2publisher;
ros::Publisher servo3publisher;
ros::Publisher servo4publisher;
ros::Publisher pose_stamped_pub1;
ros::Publisher pose_stamped_pub2;

const int servosleepTime=8*1000;
int sign(double x){
    return x>0?1:-1;
}
double servoState::disCost(servoState otherState){
    double dis1=abs(servo1angle-otherState.getAngle1())>(2*3.1415926-abs(servo1angle-otherState.getAngle1()))?(2*3.1415926-abs(servo1angle-otherState.getAngle1())):abs(servo1angle-otherState.getAngle1());
    double dis2=abs(servo2angle-otherState.getAngle2())>(2*3.1415926-abs(servo2angle-otherState.getAngle2()))?(2*3.1415926-abs(servo2angle-otherState.getAngle2())):abs(servo2angle-otherState.getAngle2());
    double norm2dis=sqrt(dis1*dis1+dis2*dis2);
    return norm2dis;
}

std::vector<Eigen::Vector3d> costBlock::blockCoff(){
    if(ocTree->size()==0){
        std::vector<Eigen::Vector3d>zeroReturn;
        zeroReturn.resize(0);
        return zeroReturn;
    }
    octomap::OcTreeNode* obCenter=ocTree->getRoot();
    int Depth=ocTree->getTreeDepth();
    //std::cout<<"Cur Depth "<<Depth<<std::endl;
    std::vector<Eigen::Vector3d>resPoints;
    if(Depth<3){
        int osize=0;
        
        for(octomap::OcTree::leaf_iterator it = ocTree->begin_leafs(),end = ocTree->end_leafs(); it != end; ++it){
            if(osize>=3)break;
            octomap::point3d center= it.getCoordinate(); 
            Eigen::Vector3d leafPoints(center(0),center(1),center(2)-0.8);
            resPoints.push_back(leafPoints);
            osize++;
        }
    }
    else{
        int osize=0;
        for(octomap::OcTree::tree_iterator it = ocTree->begin_tree(),end = ocTree->end_tree(); it != end; ++it){
            if(osize>=3)break;
            if(it.getDepth()<Depth-2)continue;
            octomap::point3d center= it.getCoordinate(); 
            Eigen::Vector3d leafPoints(center(0),center(1),center(2)-0.8);
            resPoints.push_back(leafPoints);
            osize++;
        }
    }
    return resPoints;
}

double costBlock::costOfState(servoState nextView,std::vector<robotCylinder> nextCover, std::vector<Eigen::Vector3d> &Traj, ActiveVision* vision){
    //获得相机z轴
    Eigen::Matrix4d endvision=vision->visionForwardKine(nextView);
    Eigen::Vector3d zInWorld=endvision.block<3,1>(0,2);
    Eigen::Vector3d endInWorld=endvision.block<3,1>(0,3);
    int iterTimes=vision->getVisionDepth()/resolution;
    //std::cout<<iterTimes<<std::endl;
    double wholeProb=0;
    for(int i=0;i<iterTimes;i++){
        Eigen::Vector3d curCenter=endInWorld+resolution*double(i)*zInWorld;
        for(int j=0;j<10;j++)//把圆盘分为10份
        {
            int itrTime=double(i)*abs(tan(vision->getVisionAngle()/2.0));
            //std::cout<<"iterTime is "<<itrTime<<std::endl;
            for(int k=0;k<=itrTime;k++){
                Eigen::Vector4d curp(k*resolution*cos(3.14159*double(j)/5.0),k*resolution*sin(3.14159*double(j)/5.0),0,1);
                Eigen::Matrix4d tran22;
                tran22<<1,0,0,0,0,1,0,0,0,0,1,i*resolution,0,0,0,1;
                Eigen::Vector4d wcurP=endvision*tran22*curp;
                //std::cout<<wcurP<<std::endl;
                double curProb=costOfBlock(wcurP(0)/resolution,wcurP(1)/resolution,wcurP(2)/resolution,nextView,nextCover,Traj,vision);
                //std::cout<<curProb<<std::endl;
                wholeProb+=curProb;
            }
        }
    }
    return wholeProb;
}

bool costBlock::isBlocked(int i, int j, int k,servoState nextView,std::vector<robotCylinder>&nextCover, ActiveVision *vision){
    octomap::point3d blockCartesian(double(i)*resolution,double(j)*resolution,double(k)*resolution);
    Eigen::Vector4d blockpp(double(i)*resolution,double(j)*resolution,double(k)*resolution,1);
    
    //判断是否遮挡
    Eigen::Matrix4d endvision=vision->visionForwardKine(nextView);
    Eigen::Vector3d endInWorld=endvision.block<3,1>(0,3);
    // std::cout<<"endInWorld is"<<std::endl;
    // std::cout<<endvision<<std::endl;
    Eigen::Vector4d blockInEnd=endvision.inverse()*blockpp;
    
    //std::cout<<blockInEnd<<std::endl;
    int idd=0;
    for(auto cylin:nextCover){
        //获得遮挡面积以及表示
        idd++;
        Eigen::Matrix3d wcylinPose=cylin.getRotation();
        Eigen::Matrix3d endTransRotation=endvision.block<3,3>(0,0).transpose();
        Eigen::Matrix3d endcylinPose=endTransRotation*wcylinPose;
        Eigen::Vector3d worldInEnd=-endTransRotation*endInWorld;
        Eigen::Vector3d CylinWorldinEnd=endTransRotation*cylin.getCenter_trans();
        Eigen::Vector3d CylininEnd=worldInEnd+CylinWorldinEnd;
        Eigen::Vector3d zInEnd=endcylinPose.block<3,1>(0,2);
        
        double zangle=0;
        if(abs(zInEnd(2))<1e-8)zangle=3.1415926/2.0;
        else   zangle=abs(atan(sqrt(zInEnd(0)*zInEnd(0)+zInEnd(1)*zInEnd(1))/zInEnd(2)));
        
        double cylinWidth=cylin.getHeight()*sin(zangle)+cylin.getRadius()*cos(zangle)*2;
        
        double cylinlength=2.0*cylin.getRadius();
        
        // if(idd==4){
        //     std::cout<<"Im here"<<std::endl;
        //     std::cout<<"End pose is"<<std::endl;
        //     std::cout<<endcylinPose<<std::endl;
        //     std::cout<<"End center is"<<std::endl;
        //     std::cout<<CylininEnd<<std::endl;
        //     std::cout<<"zangle is "<<zangle<<std::endl;
        //     std::cout<<"cylinWidth is "<<cylinWidth<<std::endl;
        //     std::cout<<"cylinlength is "<<cylinlength<<std::endl;
        // }
        if(blockInEnd(2)>CylininEnd(2)){
            double plantAngle=atan2(zInEnd(1),zInEnd(0));
           
            Eigen::Vector2d plantPoint(blockInEnd(0)*CylininEnd(2)/blockInEnd(2)-CylininEnd(0),blockInEnd(1)*CylininEnd(2)/blockInEnd(2)-CylininEnd(1));
            
            Eigen::Matrix2d TT;
            TT<<cos(plantAngle),sin(plantAngle),-sin(plantAngle),cos(plantAngle);
            Eigen::Vector2d plantPointInPlant=TT*plantPoint;
            
            // if(idd==4){
            //     std::cout<<"plantAngle is "<<plantAngle<<std::endl;
            //     std::cout<<"plantPoint is "<<plantPoint<<std::endl;
            //     std::cout<<"plantPointInPlant is "<<plantPointInPlant<<std::endl;
            // }
            
            if(abs(plantPointInPlant(0))<cylinWidth/2.0 && abs(plantPointInPlant(1))<cylinlength/2.0){
                //std::cout<<"Blocked by "<<idd<<std::endl;
                return true;
            }
            
        }     
    }
    return false;
}

double costBlock::costOfBlock(int i, int j, int k,servoState nextView, std::vector<robotCylinder> nextCover,std::vector<Eigen::Vector3d> &Traj, ActiveVision* vision){
    octomap::point3d blockCartesian(double(i)*resolution,double(j)*resolution,double(k)*resolution);
    Eigen::Vector4d blockpp(double(i)*resolution,double(j)*resolution,double(k)*resolution,1);
    Eigen::Matrix4d endvision=vision->visionForwardKine(nextView);
    Eigen::Vector3d endInWorld=endvision.block<3,1>(0,3);
    Eigen::Vector4d blockInEnd=endvision.inverse()*blockpp;
    
    //判断是否遮挡
    if(isBlocked(i,j,k,nextView,nextCover, vision)){
        return 0;
    }
    
    //轨迹项
    double trajDis=100;
    for(int i=0;i<Traj.size();i++){
        if(i>=5)break;
        double curDis=sqrt(pow(Traj[i](0)-blockCartesian(0),2)+pow(Traj[i](1)-blockCartesian(1),2)+pow(Traj[i](2)-blockCartesian(2),2));
        if(curDis<trajDis)trajDis=curDis;
    }
    //std::cout<<"CurDis "<<trajDis<<std::endl;
    double TrajProb=exp(-trajDis*4);

    //外围项（工作空间项）
    double centerDis=sqrt(pow(blockCartesian(0),2)+pow(blockCartesian(1),2));
    double CenterProb=0;
    if(centerDis<=1.2) CenterProb=exp(-abs(centerDis-0.85));

    //方向项 
    double DirectionProb=0;
    if(vision->getBase()(0)<blockCartesian(0))DirectionProb+=directionProb[0]*0.25;
    if(vision->getBase()(0)>blockCartesian(0))DirectionProb+=directionProb[1]*0.25;
    if(vision->getBase()(1)<blockCartesian(1))DirectionProb+=directionProb[2]*0.25;
    if(vision->getBase()(1)>blockCartesian(1))DirectionProb+=directionProb[3]*0.25;
    
    //障碍物项
    //octomap::OcTreeKey tmp;
    double ObstacleProb=0;
    octomap::point3d blockCartesianUp(double(i)*resolution,double(j)*resolution,double(k)*resolution+0.8);
    octomap::OcTreeNode* obNode=ocTree->search(blockCartesian);
    if(obNode){ObstacleProb=obNode->getOccupancy();}
    else{
                octomap::point3d_collection everyPoints;
                if(ocTree->computeRay(octomap::point3d(endInWorld(0),endInWorld(1),endInWorld(2)+0.8),blockCartesian,everyPoints)){
                    for(auto pp :everyPoints){
                        octomap::OcTreeNode* obNode=ocTree->search(blockCartesian);
                        if(obNode){ObstacleProb=obNode->getOccupancy()*0.8;}
                    }
                }
            }
    //这里需要测试，是不是这样的意思，光线追踪是不是都返回true
    double wholeProb=costCoff[0]*TrajProb+costCoff[1]*CenterProb+costCoff[2]*DirectionProb+costCoff[3]*ObstacleProb;
    //if(vision->getId()==2)
        //std::cout<<TrajProb<<", "<<CenterProb<<", "<<DirectionProb<<", "<<ObstacleProb<<", "<<std::endl;
    return wholeProb;


} 
std::vector<servoState> visionTraj::CuttingTo(double norm1Off){
    std::vector<servoState> afterDilitingPath;
    //先生成step
    double step=norm1Off/12.0;
    afterDilitingPath.push_back(jtraj[0]);
    for(int i=1;i<jtraj.size();i++){
        servoState frontNode=jtraj[i-1];
        servoState backNode=jtraj[i];
        //float step=stepSize/10.0;
        //double delta1,delta2,delta3,delta4,delta5,delta6;
        std::vector<double> dangle=backNode.substract(frontNode);
        double divideTime[]={abs(dangle[0])/step, abs(dangle[1])/step};
        int ddTime[]={int(divideTime[0]),int(divideTime[1])};
        int maxIt=ddTime[0];
        std::vector<double> curJoint={frontNode.getAngle1(),frontNode.getAngle2()};
        for(int i=1;i<2;i++){
            if(ddTime[i]>maxIt)maxIt=ddTime[i];
        }
        for(int i=0;i<maxIt;i++){
            for(int j=0;j<2;j++){
                if(ddTime[j]>0){
                    curJoint[j]=curJoint[j]+step*sign(dangle[j]);
                    if(curJoint[j]>3.1415926)curJoint[j]-=2*3.1415926;
                    else if(curJoint[j]<-3.1415926)curJoint[j]+=2*3.1415926;
                    ddTime[j]=ddTime[j]-1;
                }
            }
            servoState curNewNode(curJoint[0], curJoint[1]);
            afterDilitingPath.push_back(curNewNode);
            //std::cout<<curNewNode.getAngle(0)<<" "<<curNewNode.getAngle(1)<<' '<<curNewNode.getAngle(2)<<std::endl;
        }
        afterDilitingPath.push_back(backNode);
    }
    return afterDilitingPath;
}

std::vector<servoState> visionTraj::Diliting(){
    return CuttingTo(0.005);//以1范数的0.005为标准进行标准剪裁
}
ActiveVision::ActiveVision(servoState ss, ros::NodeHandle& _nh):nh(_nh),visionState(ss),base(Eigen::Vector3d(0,-0.5,-0.8)),id(1){blockMap=new costBlock;l1=0.069;l2=0.02;l3=0.061;visionAngle=2.0/3.0*3.14159;visionDepth=1.2;myDecision=new VisionDecision;
    servo1publisher = nh.advertise<std_msgs::Float64> ("/servo1", 10);  
    servo2publisher = nh.advertise<std_msgs::Float64> ("/servo2", 10); 
    servo3publisher = nh.advertise<std_msgs::Float64> ("/servo3", 10);  
    servo4publisher = nh.advertise<std_msgs::Float64> ("/servo4", 10); 
    pose_stamped_pub1 = nh.advertise<geometry_msgs::PoseStamped>("/cam1_pose", 10);
    pose_stamped_pub2 = nh.advertise<geometry_msgs::PoseStamped>("/cam2_pose", 10);
    // initialState();
    servo = nullptr;
    delta_s = Eigen::Vector2d(0,0);}
ActiveVision::ActiveVision(servoState ss, Eigen::Vector3d bb,int ID, ros::NodeHandle& _nh):nh(_nh),visionState(ss),base(bb),id(ID){blockMap=new costBlock;l1=0.069;l2=0.02;l3=0.061;visionAngle=2.0/3.0*3.14159;visionDepth=1.2;myDecision=new VisionDecision;
    servo1publisher = nh.advertise<std_msgs::Float64> ("/servo1", 10);  
    servo2publisher = nh.advertise<std_msgs::Float64> ("/servo2", 10); 
    servo3publisher = nh.advertise<std_msgs::Float64> ("/servo3", 10);  
    servo4publisher = nh.advertise<std_msgs::Float64> ("/servo4", 10); 
    pose_stamped_pub1 = nh.advertise<geometry_msgs::PoseStamped>("/cam1_pose", 10);
    pose_stamped_pub2 = nh.advertise<geometry_msgs::PoseStamped>("/cam2_pose", 10);
    // initialState();
    servo = nullptr;
    delta_s = Eigen::Vector2d(0,0);}

ActiveVision::ActiveVision(servoState ss, Eigen::Vector3d bb,int ID, ros::NodeHandle& _nh, int argc, char** argv):nh(_nh),visionState(ss),base(bb),id(ID){blockMap=new costBlock;l1=0.069;l2=0.02;l3=0.061;visionAngle=2.0/3.0*3.14159;visionDepth=1.2;myDecision=new VisionDecision;
    servo1publisher = nh.advertise<std_msgs::Float64> ("/servo1", 10);  
    servo2publisher = nh.advertise<std_msgs::Float64> ("/servo2", 10); 
    servo3publisher = nh.advertise<std_msgs::Float64> ("/servo3", 10);  
    servo4publisher = nh.advertise<std_msgs::Float64> ("/servo4", 10); 
    pose_stamped_pub1 = nh.advertise<geometry_msgs::PoseStamped>("/cam1_pose", 10);
    pose_stamped_pub2 = nh.advertise<geometry_msgs::PoseStamped>("/cam2_pose", 10);
    // initialState();
    delta_s = Eigen::Vector2d(0,0);
    servo = new ftservo(argc, argv);
    // updateThread = std::thread(&ActiveVision::broadCastThreadFun, this);
    }
void ActiveVision::broadCastThreadFun(){
    
    while(ros::ok()){
        if(id == 1){
            servoState curState(servo->read_angle(1), servo->read_angle(2));
            visionState = curState;
            std::cout<<visionState.getAngle1()<<' '<<visionState.getAngle2()<<std::endl;
        }
        else if(id == 2){
            servoState curState(servo->read_angle(3), servo->read_angle(4));
            visionState = curState;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}
// void ActiveVision::broadcastTF(){
//     //相机根部TF维护
//     static tf::TransformBroadcaster br;
//     tf::Transform transform;
//     transform.setOrigin(tf::Vector3(base(0),base(1),0));
//     tf::Quaternion q;
//     q.setRPY(0,0,0);
//     transform.setRotation(q);
//     br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "/fix_link"+std::to_string(id)));
//     //相机末端TF维护
//     static tf::TransformBroadcaster br2;
//     tf::Transform transform2;
//     Eigen::Matrix4d world_end_pose=visionForwardKine();
//     // std::cout<<"id "<<id<<" world_end_pose is "<<std::endl;
//     // std::cout<<world_end_pose<<std::endl;
//     Eigen::Vector3d camera_end_pos = world_end_pose.block<3,1>(0,3);
//     if(id == 1){
//         transform2.setOrigin(tf::Vector3(camera_end_pos(0),camera_end_pos(1),(camera_end_pos(2)-base(2))));
//     }
//     else{
//         transform2.setOrigin(tf::Vector3(camera_end_pos(0),camera_end_pos(1),(camera_end_pos(2)-base(2))));
//     }
    
//     Eigen::Quaterniond end_quad=Eigen::Quaterniond(world_end_pose.block<3,3>(0,0));
//     tf::Quaternion q2(end_quad.x(),end_quad.y(),end_quad.z(),end_quad.w());
//     transform2.setRotation(q2);
//     br2.sendTransform(tf::StampedTransform(transform2, ros::Time::now(), "base_link", "/cam_"+std::to_string(id)+"_link"));

// }

void ActiveVision::broadcastTF(){
    //相机根部TF维护
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin(tf::Vector3(base(0),base(1),0));
    tf::Quaternion q;
    q.setRPY(0,0,0);
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "/fix_link"+std::to_string(id)));
    //相机末端TF维护
    static tf::TransformBroadcaster br2;
    tf::Transform transform2;
    Eigen::Matrix4d world_end_pose=visionForwardKine();
    // std::cout<<"id "<<id<<" world_end_pose is "<<std::endl;
    // std::cout<<world_end_pose<<std::endl;
    Eigen::Vector3d camera_end_pos = world_end_pose.block<3,1>(0,3);
    if(id == 1){
        transform2.setOrigin(tf::Vector3(camera_end_pos(0),camera_end_pos(1),(camera_end_pos(2)-base(2))));
    }
    else{
        transform2.setOrigin(tf::Vector3(camera_end_pos(0),camera_end_pos(1),(camera_end_pos(2)-base(2))));
    }
    
    Eigen::Quaterniond end_quad=Eigen::Quaterniond(world_end_pose.block<3,3>(0,0));
    tf::Quaternion q2(end_quad.x(),end_quad.y(),end_quad.z(),end_quad.w());
    transform2.setRotation(q2);
    br2.sendTransform(tf::StampedTransform(transform2, ros::Time::now(), "base_link", "/cam_"+std::to_string(id)+"_link"));

    if(id == 1){
        geometry_msgs::PoseStamped camera_pose_msg;
        camera_pose_msg.header.stamp = ros::Time::now();
        camera_pose_msg.header.frame_id = "base_link";
        camera_pose_msg.pose.position.x = transform2.getOrigin().getX();
        camera_pose_msg.pose.position.y = transform2.getOrigin().getY();
        camera_pose_msg.pose.position.z = transform2.getOrigin().getZ();
        camera_pose_msg.pose.orientation.x = q2.getX();
        camera_pose_msg.pose.orientation.y = q2.getY();
        camera_pose_msg.pose.orientation.z = q2.getZ();
        camera_pose_msg.pose.orientation.w = q2.getW();

        // 发布消息
        pose_stamped_pub1.publish(camera_pose_msg);
    }
    else if(id == 2){
        geometry_msgs::PoseStamped camera_pose_msg;
        camera_pose_msg.header.stamp = ros::Time::now();
        camera_pose_msg.header.frame_id = "base_link";
        camera_pose_msg.pose.position.x = transform2.getOrigin().getX();
        camera_pose_msg.pose.position.y = transform2.getOrigin().getY();
        camera_pose_msg.pose.position.z = transform2.getOrigin().getZ();
        camera_pose_msg.pose.orientation.x = q2.getX();
        camera_pose_msg.pose.orientation.y = q2.getY();
        camera_pose_msg.pose.orientation.z = q2.getZ();
        camera_pose_msg.pose.orientation.w = q2.getW();

        // 发布消息
        pose_stamped_pub2.publish(camera_pose_msg);
    }

}
//主动视觉机构正运动学
Eigen::Matrix4d ActiveVision::visionForwardKine(servoState ss){
    //一定要注意两个舵机，需要有普适性
    //注意主动视觉机构基坐标系要与机械臂坐标系平起
    Eigen::Matrix4d g0;
    g0<< 1,0,0,0,
                0,1,0,l2,
                0,0,1,l1+l3,
                0,0,0,1;
    Eigen::Matrix4d e1;
    e1<<cos(ss.getAngle1()),-sin(ss.getAngle1()), 0,0,
            sin(ss.getAngle1()),cos(ss.getAngle1()),0,0,
            0,0,1,0,
            0,0,0,1;
    Eigen::Matrix4d e2;
    e2<<1,0,0,0,
            0,cos(ss.getAngle2()),sin(ss.getAngle2()),l2*(1-cos(ss.getAngle2()))-l1*sin(ss.getAngle2()),
            0,-sin(ss.getAngle2()),cos(ss.getAngle2()),l2*sin(ss.getAngle2())+l2*(1-cos(ss.getAngle2())),
            0,0,0,1;
    Eigen::Matrix4d current_end_pose=e1*e2*g0;
    Eigen::Matrix4d trans ;
    trans << 1,0,0,base(0),
                    0,1,0,base(1),
                    0,0,1,base(2),
                    0,0,0,1;
    Eigen::Matrix4d world_end_pose=trans*current_end_pose;
    return world_end_pose;
}
//主动视觉机构逆运动学
servoState ActiveVision::visionInverseKine(Eigen::Vector3d endPoint){
    // if(endPoint(2)<-0.05)endPoint(2)+=0.8;
    double theta1=atan2(base(0)-endPoint(0),endPoint(1)-base(1));//rad
    double dis=sqrt(pow(base(0)-l1*sin(theta1)-endPoint(0),2)+pow(base(1)-l2*cos(theta1)-endPoint(1),2));
    double theta2=3.1415926535/2.0-atan2(endPoint(2)-l1-base(2),dis);
    servoState resState(theta1,theta2);
    return resState;
}

double ActiveVision::getCost(servoState nextView,  std::vector<robotCylinder>& robotCover,std::vector<Eigen::Vector3d> &Traj){
    double coffBlock,coffConti;
    coffBlock=0.4;
    coffConti=0.6;

    double wholeProf=blockMap->costOfState(nextView, robotCover, Traj, this);
    // double disProf=1.0/(1.0+visionState.disCost(nextView));
    double disProf=-visionState.disCost(nextView);
    double Jmax=coffBlock*wholeProf+coffConti*disProf;
    return Jmax;
}

std::vector<double> ActiveVision::profitOfEachDecision(std::vector<Eigen::Vector3d> &Traj, std::vector<robotCylinder>& nextCover, octomap::OcTree* preMap,std::vector<servoState> &eachState){
    //决策层
    if(preMap==nullptr)preMap=new octomap::OcTree(0.1);
    blockMap->setOcT(preMap);
    myDecision->genTrajAction(this, Traj);
    myDecision->genMovingAction(this);
    //myDecision->genSerchingAction(this);
    //std::cout<<"IM HERE"<<std::endl;
    std::vector<servoState>TrajAlter=myDecision->getTrajState();
    std::vector<servoState>ObAlter=myDecision->getMovingState();
    //std::vector<servoState>WayAlter=myDecision->getSearchingState();
    // if(id==2){
    //     for(auto ss:ObAlter)
    //         std::cout<<ss.getAngle1()<<' '<<ss.getAngle2()<<std::endl;
    // }
    //优化层
    blockMap->setOcT(preMap);
    double TrajbestProfit=0;
    servoState TrajbestState;
    bool HasTraj=false;
    if(TrajAlter.size()==0){
        TrajbestState=visionState;
    }
    for(auto ss:TrajAlter){
        HasTraj=true;
        double curProfit=getCost(ss,nextCover,Traj);
        if(curProfit>=TrajbestProfit){
            TrajbestProfit=curProfit;
            TrajbestState=ss;
        }
    }
    // if(id==2){
    //     std::cout<<TrajbestState.getAngle1()<<' '<<TrajbestState.getAngle2()<<std::endl;
    // }
    double ObbestProfit=0;
    servoState ObbestState;
    bool HasOb=false;
    for(auto ss:ObAlter){
        HasOb=true;
        double curProfit=getCost(ss,nextCover,Traj);
        if(curProfit>=ObbestProfit){
            ObbestProfit=curProfit;
            ObbestState=ss;
        }
    }
    // if(id==2){
    //     std::cout<<ObbestState.getAngle1()<<' '<<ObbestState.getAngle2()<<std::endl;
    // }
    // double WaybestProfit=0;
    // servoState WaybestState;
    // bool HasWay=false;
    // for(auto ss:WayAlter){
    //     HasWay=true;
    //     double curProfit=getCost(ss,nextCover,Traj);
    //     if(curProfit>=WaybestProfit){
    //         WaybestProfit=curProfit;
    //         WaybestState=ss;
    //     }
    // }

    std::vector<double>resProfit;
    eachState.clear();
    eachState.push_back(TrajbestState);
    resProfit.push_back(TrajbestProfit);
    eachState.push_back(ObbestState);
    resProfit.push_back(ObbestProfit);
    //eachState.push_back(WaybestState);
    //resProfit.push_back(WaybestProfit);
    // if(id==2){
    //     for(auto ss:eachState)
    //         std::cout<<ss.getAngle1()<<' '<<ss.getAngle2()<<std::endl;
    // }
    return resProfit;

}

void ActiveVision::moveCameraTo(servoState targetState){
    if(servo == nullptr){
        // std::cout<<"BARK4"<<std::endl;
        std_msgs::Float64 data1,data2;
        visionState = targetState;
        data1.data=(targetState.getAngle1())*180.0/3.1415926535-delta_s(0);
        data2.data=(targetState.getAngle2())*180.0/3.1415926535-delta_s(1);
        //std::cout<<data2.data<<std::endl;
        if(id == 1){
            servo1publisher.publish(data1);
            servo2publisher.publish(data2);
        }
        else if(id == 2){
            servo3publisher.publish(data1);
            servo4publisher.publish(data2);
        }
        broadcastTF();
    }
    else{
        // std::cout<<"have servo"<<std::endl;
        if(id == 1){
            servo->servoControl((targetState.getAngle1())*180.0/3.1415926535-delta_s(0), 1);
            servo->servoControl((targetState.getAngle2())*180.0/3.1415926535-delta_s(1), 2);
        }
        else if(id == 2){
            servo->servoControl((targetState.getAngle1())*180.0/3.1415926535-delta_s(0), 3);
            servo->servoControl((targetState.getAngle2())*180.0/3.1415926535-delta_s(1), 4);
        }
        visionState = targetState;
        broadcastTF();
    }
    
}

void ActiveVision::initialState(){
    moveCameraTo(visionState);
}
void ActiveVision::executeVisionTraj(visionTraj myVisionTraj, double speed ){
    // std::cout<<"BARK1"<<std::endl;
    if(myVisionTraj.getStart().disCost(visionState)>0.2) myVisionTraj.addFrontTraj(visionState);

    std::vector<servoState> afterDiliting = myVisionTraj.CuttingTo(speed*0.02);
    // std::cout<<"afterDiliting is "<<afterDiliting.size()<<std::endl;
    int wholesize = afterDiliting.size();
    // std::cout<<"BARK2"<<std::endl;
    auto start_time = std::chrono::high_resolution_clock::now();
    for(auto _svst:afterDiliting){
        // std::cout<<"angle is "<<_svst.getAngle1()<<' '<<_svst.getAngle2()<<std::endl;
        // std::cout<<"BARK3"<<std::endl;
        moveCameraTo(_svst);
        usleep(2e6/wholesize/2.0);
                    // 记录结束时间
        
    }
    auto end_time = std::chrono::high_resolution_clock::now();

    // 计算时间差
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);

    // std::cout << "代码运行时间: " << duration.count() << " 微秒" << std::endl;
}
void ActiveVision::goToState(servoState targetState, double speed){
    visionTraj mytraj_(visionState, targetState);
    executeVisionTraj(mytraj_, speed);
}
//直接选择轨迹前4个点作为目标点
void VisionDecision::genTrajAction(ActiveVision * visionDevice,std::vector<Eigen::Vector3d> &Traj){
    //std::cout<<Traj.size()<<std::endl;
    actionOfWatchTraj.clear();

    for(int i=0;i<Traj.size();i++){
        if(i>=3)return;
        servoState nextAlterView=visionDevice->visionInverseKine(Traj[i]);
        actionOfWatchTraj.push_back(nextAlterView);
    }
}

void VisionDecision::genMovingAction(ActiveVision * visionDevice){
    // octomap::AbstractOcTreeNode* obCenters=visionDevice->blockMap->oc
    actionOfWatchMoving.clear();
    std::vector<Eigen::Vector3d>obCenter=visionDevice->getBlockMap()->blockCoff();
    
    for(auto pp:obCenter){
        if(pp(2)>-0.7&&pp(0)<0){
            servoState nextAlterView=visionDevice->visionInverseKine(pp);
            actionOfWatchMoving.push_back(nextAlterView);
        }
    }
    // std::cout<<actionOfWatchMoving.size()<<std::endl;
}

void VisionDecision::genSerchingAction(ActiveVision *visionDevice){
    ;
}
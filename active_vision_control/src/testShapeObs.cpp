#include "active_vision_control/envelopeShape.h"
#include "active_vision_control/movingObs.h"

void testShape(){
    //test for constructor
    envelopeShape::Cylinder testCylinder(0.5,0.7);
    std::vector<double> par ;
    testCylinder.getShapeParameters(par);
    for(auto itm:par){
        std::cout<<itm<<std::endl;
    }

    //test for clone
    envelopeShape::Shape* testClone=testCylinder.clone();
    std::vector<double> par1 ={0.3,0.4};
    testCylinder.setShapeParameters(par1);
    testClone->getShapeParameters(par);
    for(auto itm:par){
        std::cout<<itm<<std::endl;
    }

    //test for pose
    Eigen::Vector3d testC(0,0,0);
    Eigen::Matrix3d testM;
    testM << 1,0,0,0,1,0,0,0,1;

    testCylinder.setPose(testC,testM);
    fcl::CollisionObjectd* testObs=testCylinder.toCollisionObject();
    std::cout<<testObs->getRotation()<<std::endl;
    std::cout<<testObs->getTranslation()<<std::endl;
}

void testGaussion(){
    Gaussian3D testg(Eigen::Vector3d(1,2,1), 0.1*Eigen::Matrix3d::Identity());
    //test for prob in one ball
    std::cout<<testg.probabilityInBall(Eigen::Vector3d(0,0,0), 1)<<std::endl;
    std::cout<<testg.probabilityInBall(Eigen::Vector3d(1,2,1), 1)<<std::endl;
    std::cout<<testg.probabilityInBall(Eigen::Vector3d(1,2,1), 2)<<std::endl;
    //test for prob in two ball
    std::cout<<testg.probabilityInTwoBall(Eigen::Vector3d(1,2,1), Eigen::Vector3d(1,2,1), 1)<<std::endl;
    std::cout<<testg.probabilityInTwoBall(Eigen::Vector3d(1,2,1), Eigen::Vector3d(1,2,0.5), 1)<<std::endl;
    std::cout<<testg.probabilityInTwoBall(Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0.5), 1)<<std::endl;
    //test for cov
    Eigen::Matrix3d testcov = testg.updateVelocityCovariance(Eigen::Vector3d(0,0,0), Eigen::Vector3d(1,1,1), Eigen::Matrix3d::Identity());
    std::cout<<testcov<<std::endl;
}

void testMovingObs(){
    envelopeShape::Cylinder testCylinder(0.5,0.7);
    Eigen::Vector3d testC(0,0,0);
    Eigen::Matrix3d testM;
    testM << 1,0,0,0,1,0,0,0,1;
    testCylinder.setPose(testC,testM);

    Eigen::Matrix3d Cov;
    Cov << 0.1,0,0,0,0.1,0,0,0,0.1;
    Eigen::Vector3d testVelo(1,0,0);
    Eigen::Vector3d testOmega(1,0,0);

    envelopeShape::movingObs testObs(testCylinder.clone(), testVelo, testOmega, 1.0, Cov,Cov,Cov,Cov,0);
    //test for get center
    std::cout<<testObs.getCenter()<<std::endl;
    //test for clone
    envelopeShape::movingObs testClone = testObs.clone();
    //test for state evolution
    double time=0.5;
    testObs.stateEvolution(time);
    std::cout<<testObs.getCenter()<<std::endl;
    std::cout<<testObs.getOrientation()<<std::endl;

    //test for clone
    std::cout<<testClone.getCenter()<<std::endl;
    std::cout<<testClone.getOrientation()<<std::endl;

    testObs.updateUncertainty(0.5, 1);
    testClone.updateUncertainty(0.5, 0);

    std::cout<<testObs.getUncertainty()<<std::endl;
    std::cout<<testClone.getUncertainty()<<std::endl;

}
int main(int argc, char** argv){

    //testShape();
    //testGaussion();
    testMovingObs();
    return 0;
}
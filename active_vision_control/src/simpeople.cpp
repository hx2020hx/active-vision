#include "active_vision_control/simpeople.h"

SimulationPeople::SimulationPeople(){
    handRotationOmega=0.5;
    handp = 0.25;
    rhand = 0.1;
    Eigen::Matrix3d rotationMatrix;
    rotationMatrix<<1,0,0,0,1,0,0,0,1;
    Eigen::Vector3d bodyCenter(-0.6, 0, -0.5);
    envelopeShape::Cylinder* bodyCylinder=new envelopeShape::Cylinder(0.35, 0.6);
    bodyCylinder->setPose(bodyCenter, rotationMatrix);
    body = new envelopeShape::movingObs(bodyCylinder, Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0), 1, false);

    Eigen::Vector3d rightArmCenter(-0.6, -handp, -0.35);
    envelopeShape::Cylinder* rightArmCylinder=new envelopeShape::Cylinder(0.1, 0.3);
    rightArmCylinder->setPose(rightArmCenter, rotationMatrix);
    right_arm = new envelopeShape::movingObs(rightArmCylinder, handRotationOmega*0.15*Eigen::Vector3d(1,0,0), handRotationOmega*Eigen::Vector3d(0,-1,0), 1, false);

    Eigen::Vector3d rightHandCenter(-0.6, -handp, -0.65);
    envelopeShape::Cylinder* rightHandCylinder=new envelopeShape::Cylinder(0.1, 0.3);
    rightHandCylinder->setPose(rightHandCenter, rotationMatrix);
    right_hand = new envelopeShape::movingObs(rightHandCylinder, handRotationOmega*0.15*Eigen::Vector3d(1,0,0), handRotationOmega*Eigen::Vector3d(0,-1,0), 1, false);

    Eigen::Vector3d leftArmCenter(-0.6, handp, -0.35);
    envelopeShape::Cylinder* leftArmCylinder=new envelopeShape::Cylinder(0.1, 0.3);
    leftArmCylinder->setPose(leftArmCenter, rotationMatrix);
    left_arm = new envelopeShape::movingObs(leftArmCylinder, handRotationOmega*0.15*Eigen::Vector3d(1,0,0), handRotationOmega*Eigen::Vector3d(0,-1,0), 1, false);

    Eigen::Vector3d leftHandCenter(-0.6, handp, -0.65);
    envelopeShape::Cylinder* leftHandCylinder=new envelopeShape::Cylinder(0.1, 0.3);
    leftHandCylinder->setPose(leftHandCenter, rotationMatrix);
    left_hand = new envelopeShape::movingObs(leftHandCylinder, handRotationOmega*0.15*Eigen::Vector3d(1,0,0), handRotationOmega*Eigen::Vector3d(0,-1,0), 1, false);

    
}

SimulationPeople::SimulationPeople(ros::NodeHandle &nh):nhe(nh){
    handp = 0.25;
    handRotationOmega=0.25;
    Eigen::Matrix3d rotationMatrix;
    rotationMatrix<<1,0,0,0,1,0,0,0,1;
    Eigen::Vector3d bodyCenter(-0.6, 0, -0.5);
    envelopeShape::Cylinder* bodyCylinder=new envelopeShape::Cylinder(0.15, 0.6);
    bodyCylinder->setPose(bodyCenter, rotationMatrix);
    body = new envelopeShape::movingObs(bodyCylinder, Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0), 1, false);

    Eigen::Vector3d rightArmCenter(-0.6, -handp, -0.35);
    envelopeShape::Cylinder* rightArmCylinder=new envelopeShape::Cylinder(0.1, 0.3);
    rightArmCylinder->setPose(rightArmCenter, rotationMatrix);
    right_arm = new envelopeShape::movingObs(rightArmCylinder, handRotationOmega*0.15*Eigen::Vector3d(1,0,0), handRotationOmega*Eigen::Vector3d(0,-1,0), 1, false);

    Eigen::Vector3d rightHandCenter(-0.6, -handp, -0.65);
    envelopeShape::Cylinder* rightHandCylinder=new envelopeShape::Cylinder(0.1, 0.3);
    rightHandCylinder->setPose(rightHandCenter, rotationMatrix);
    right_hand = new envelopeShape::movingObs(rightHandCylinder, handRotationOmega*0.45*Eigen::Vector3d(1,0,0), handRotationOmega*Eigen::Vector3d(0,-1,0), 1, false);

    Eigen::Vector3d leftArmCenter(-0.6, handp, -0.35);
    envelopeShape::Cylinder* leftArmCylinder=new envelopeShape::Cylinder(0.1, 0.3);
    leftArmCylinder->setPose(leftArmCenter, rotationMatrix);
    left_arm = new envelopeShape::movingObs(leftArmCylinder, handRotationOmega*0.15*Eigen::Vector3d(1,0,0), handRotationOmega*Eigen::Vector3d(0,-1,0), 1, false);

    Eigen::Vector3d leftHandCenter(-0.6, handp, -0.65);
    envelopeShape::Cylinder* leftHandCylinder=new envelopeShape::Cylinder(0.1, 0.3);
    leftHandCylinder->setPose(leftHandCenter, rotationMatrix);
    left_hand = new envelopeShape::movingObs(leftHandCylinder, handRotationOmega*0.45*Eigen::Vector3d(1,0,0), handRotationOmega*Eigen::Vector3d(0,-1,0), 1, false);
    
    peopleStatePub = nhe.advertise<std_msgs::Float64MultiArray>("/odomState",10);
    markerPub = nhe.advertise<visualization_msgs::MarkerArray>("/marker_array", 10);
    
}

void SimulationPeople::initialize(){
    start_time_ = std::chrono::high_resolution_clock::now();
}
Eigen::Matrix3d SimulationPeople::rotationAxis(Eigen::Vector3d rotation_vector, Eigen::Matrix3d original_rotation_matrix){
    Eigen::Matrix3d rotated_matrix;
    double angle = rotation_vector.norm();
    if (angle > 1e-6) {
        Eigen::Vector3d axis = rotation_vector.normalized();
        Eigen::AngleAxisd rotation(angle, axis);
        rotated_matrix = rotation.toRotationMatrix() * original_rotation_matrix;
    } else {
        rotated_matrix = original_rotation_matrix;
    }
    return rotated_matrix;
}

void SimulationPeople::armRotationmotion(){
    auto end_time_ = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed_time = end_time_ - start_time_;
    double passedTime = elapsed_time.count();
    Eigen::Matrix3d originrotationMatrix;
    originrotationMatrix<<1,0,0,0,1,0,0,0,1;
    int rotationTimes = passedTime*handRotationOmega/(0.5*M_PI);
    double leftAngle = passedTime*handRotationOmega-double(rotationTimes)*(0.5*M_PI);
    
    if(rotationTimes==0 || rotationTimes%2==0){
        
        right_arm->updateVelocity(handRotationOmega*0.15*Eigen::Vector3d(cos(leftAngle),0,sin(leftAngle)), handRotationOmega*Eigen::Vector3d(0,-1,0));
        Eigen::Matrix3d right_arm_rotationMatrix = rotationAxis(leftAngle*Eigen::Vector3d(0,-1,0), originrotationMatrix);
        right_arm->updateShape(Eigen::Vector3d(-0.6+0.15*sin(leftAngle), -handp, -0.35-0.15*cos(leftAngle)), right_arm_rotationMatrix);

        left_arm->updateVelocity(handRotationOmega*0.15*Eigen::Vector3d(cos(leftAngle),0,sin(leftAngle)), handRotationOmega*Eigen::Vector3d(0,-1,0));
        Eigen::Matrix3d left_arm_rotationMatrix = rotationAxis(leftAngle*Eigen::Vector3d(0,-1,0), originrotationMatrix);
        left_arm->updateShape(Eigen::Vector3d(-0.6+0.15*sin(leftAngle), handp, -0.35-0.15*cos(leftAngle)), left_arm_rotationMatrix);

        right_hand->updateVelocity(handRotationOmega*0.45*Eigen::Vector3d(cos(leftAngle),0,sin(leftAngle)), handRotationOmega*Eigen::Vector3d(0,-1,0));
        Eigen::Matrix3d right_hand_rotationMatrix = rotationAxis(leftAngle*Eigen::Vector3d(0,-1,0), originrotationMatrix);
        right_hand->updateShape(Eigen::Vector3d(-0.6+0.45*sin(leftAngle), -handp, -0.35-0.45*cos(leftAngle)), right_hand_rotationMatrix);

        left_hand->updateVelocity(handRotationOmega*0.45*Eigen::Vector3d(cos(leftAngle),0,sin(leftAngle)), handRotationOmega*Eigen::Vector3d(0,-1,0));
        Eigen::Matrix3d left_hand_rotationMatrix = rotationAxis(leftAngle*Eigen::Vector3d(0,-1,0), originrotationMatrix);
        left_hand->updateShape(Eigen::Vector3d(-0.6+0.45*sin(leftAngle), handp, -0.35-0.45*cos(leftAngle)), left_hand_rotationMatrix);
        
    }
    
    else{
        leftAngle = (0.5*M_PI) - leftAngle;
        right_arm->updateVelocity(handRotationOmega*0.15*Eigen::Vector3d(cos(leftAngle),0,sin(leftAngle)), handRotationOmega*Eigen::Vector3d(0,-1,0));
        Eigen::Matrix3d right_arm_rotationMatrix = rotationAxis(leftAngle*Eigen::Vector3d(0,-1,0), originrotationMatrix);
        right_arm->updateShape(Eigen::Vector3d(-0.6+0.15*sin(leftAngle), -handp, -0.35-0.15*cos(leftAngle)), right_arm_rotationMatrix);

        left_arm->updateVelocity(handRotationOmega*0.15*Eigen::Vector3d(cos(leftAngle),0,sin(leftAngle)), handRotationOmega*Eigen::Vector3d(0,-1,0));
        Eigen::Matrix3d left_arm_rotationMatrix = rotationAxis(leftAngle*Eigen::Vector3d(0,-1,0), originrotationMatrix);
        left_arm->updateShape(Eigen::Vector3d(-0.6+0.15*sin(leftAngle), handp, -0.35-0.15*cos(leftAngle)), left_arm_rotationMatrix);

        right_hand->updateVelocity(handRotationOmega*0.45*Eigen::Vector3d(cos(leftAngle),0,sin(leftAngle)), handRotationOmega*Eigen::Vector3d(0,-1,0));
        Eigen::Matrix3d right_hand_rotationMatrix = rotationAxis(leftAngle*Eigen::Vector3d(0,-1,0), originrotationMatrix);
        right_hand->updateShape(Eigen::Vector3d(-0.6+0.45*sin(leftAngle), -handp, -0.35-0.45*cos(leftAngle)), right_hand_rotationMatrix);

        left_hand->updateVelocity(handRotationOmega*0.45*Eigen::Vector3d(cos(leftAngle),0,sin(leftAngle)), handRotationOmega*Eigen::Vector3d(0,-1,0));
        Eigen::Matrix3d left_hand_rotationMatrix = rotationAxis(leftAngle*Eigen::Vector3d(0,-1,0), originrotationMatrix);
        left_hand->updateShape(Eigen::Vector3d(-0.6+0.45*sin(leftAngle), handp, -0.35-0.45*cos(leftAngle)), left_hand_rotationMatrix);
    }

    
}

void SimulationPeople::baseMoving(){
    auto end_time_ = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed_time = end_time_ - start_time_;
    double passedTime = elapsed_time.count();
    double y_offset=0;
    double velo = 0;
    if(passedTime<10) {
        y_offset = -0.5+0.1*passedTime;
        velo = 0.1;
    }
    else {
        y_offset = 0.5-0.1*(passedTime-10);
        velo = -0.1;
    }

    // body->updateVelocity(velo*Eigen::Vector3d(0,1,0), body->getAngularVelocity());
    body->updateShape(Eigen::Vector3d(-0.6, 0, -0.5)+y_offset*Eigen::Vector3d(0,1,0), body->getOrientation());
    //std::cout<<"Body "<<body->getCenter()+y_offset*Eigen::Vector3d(0,1,0)<<std::endl;
    // right_arm->updateVelocity(velo*Eigen::Vector3d(0,1,0)+right_arm->getVelocity(), right_arm->getAngularVelocity());
    right_arm->updateShape(right_arm->getCenter()+y_offset*Eigen::Vector3d(0,1,0), right_arm->getOrientation());
    //std::cout<<"right_arm "<<right_arm->getCenter()<<std::endl;
    // left_arm->updateVelocity(velo*Eigen::Vector3d(0,1,0)+left_arm->getVelocity(), left_arm->getAngularVelocity());
    left_arm->updateShape(left_arm->getCenter()+y_offset*Eigen::Vector3d(0,1,0), left_arm->getOrientation());

    // right_hand->updateVelocity(velo*Eigen::Vector3d(0,1,0)+right_hand->getVelocity(), right_hand->getAngularVelocity());
    right_hand->updateShape(right_hand->getCenter()+y_offset*Eigen::Vector3d(0,1,0), right_hand->getOrientation());

    // left_hand->updateVelocity(velo*Eigen::Vector3d(0,1,0)+left_hand->getVelocity(), left_hand->getAngularVelocity());
    left_hand->updateShape(left_hand->getCenter()+y_offset*Eigen::Vector3d(0,1,0), left_hand->getOrientation());
    
}
void SimulationPeople::vel_clear(){
    body->updateVelocity(Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0));
    right_arm->updateVelocity(Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0));
    left_arm->updateVelocity(Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0));
    right_hand->updateVelocity(Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0));
    left_hand->updateVelocity(Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0));
}
//统一发送编码
//中心位置（3），旋转向量（3），半径，高度，速度（3），角速度（3）
std_msgs::Float64MultiArray SimulationPeople::genMsgFromCylinder(envelopeShape::movingObs* cy){
    Eigen::Vector3d center = cy->getCenter();
    Eigen::Vector3d zAxis = cy->getz();
    std::vector<double> paramm;
    cy->getShape()->getShapeParameters(paramm);
    Eigen::Vector3d velo = cy->getVelocity();
    Eigen::Vector3d Anglevelo = cy->getAngularVelocity();

    std_msgs::Float64MultiArray pubmsg;
    pubmsg.data.push_back(center(0));
    pubmsg.data.push_back(center(1));
    pubmsg.data.push_back(center(2));
    pubmsg.data.push_back(zAxis(0));
    pubmsg.data.push_back(zAxis(1));
    pubmsg.data.push_back(zAxis(2));
    pubmsg.data.push_back(paramm[0]);
    pubmsg.data.push_back(paramm[1]);
    pubmsg.data.push_back(velo(0));
    pubmsg.data.push_back(velo(1));
    pubmsg.data.push_back(velo(2));
    pubmsg.data.push_back(Anglevelo(0));
    pubmsg.data.push_back(Anglevelo(1));
    pubmsg.data.push_back(Anglevelo(2));
    return pubmsg;
}
void SimulationPeople::publishMsg(){
    std_msgs::Float64MultiArray bodymsg = genMsgFromCylinder(body);
    std_msgs::Float64MultiArray right_armmsg = genMsgFromCylinder(right_arm);
    std_msgs::Float64MultiArray left_armmsg = genMsgFromCylinder(left_arm);
    std_msgs::Float64MultiArray right_handmsg = genMsgFromCylinder(right_hand);
    std_msgs::Float64MultiArray left_handmsg = genMsgFromCylinder(left_hand);

    // 创建一个新的 std_msgs::Float64MultiArray 对象，用于存储拼接后的数据
    std_msgs::Float64MultiArray concatenatedArray;
    
    // 将每个消息对象的数据依次添加到新对象中
    concatenatedArray.data.insert(concatenatedArray.data.end(), bodymsg.data.begin(), bodymsg.data.end());
    concatenatedArray.data.insert(concatenatedArray.data.end(), right_armmsg.data.begin(), right_armmsg.data.end());
    concatenatedArray.data.insert(concatenatedArray.data.end(), left_armmsg.data.begin(), left_armmsg.data.end());
    concatenatedArray.data.insert(concatenatedArray.data.end(), right_handmsg.data.begin(), right_handmsg.data.end());
    concatenatedArray.data.insert(concatenatedArray.data.end(), left_handmsg.data.begin(), left_handmsg.data.end());
    
    peopleStatePub.publish(concatenatedArray);
    //std::cout<<"BARK"<<std::endl;
    visualization_msgs::Marker body_marker = body->getShape()->genCylinderRviz(1);
    visualization_msgs::Marker right_arm_marker = right_arm->getShape()->genCylinderRviz(2);
    visualization_msgs::Marker left_arm_marker = left_arm->getShape()->genCylinderRviz(3);
    visualization_msgs::Marker right_hand_marker = right_hand->getShape()->genCylinderRviz(4);
    visualization_msgs::Marker left_hand_marker = left_hand->getShape()->genCylinderRviz(5);
    
    visualization_msgs::MarkerArray markerArray;
    // 将各个Marker添加到MarkerArray中
    markerArray.markers.push_back(body_marker);
    markerArray.markers.push_back(right_arm_marker);
    markerArray.markers.push_back(left_arm_marker);
    markerArray.markers.push_back(right_hand_marker);
    markerArray.markers.push_back(left_hand_marker);
    
    // 发布MarkerArray
    markerPub.publish(markerArray);
    
    
}

void* SimulationPeople::peopleSimulationStart(void* threadid){
    initialize();
    ros::Rate looprate(20);
    while(true){
        armRotationmotion();
        baseMoving();
        // vel_clear();
        publishMsg();
        looprate.sleep();
    }
    return 0;
}
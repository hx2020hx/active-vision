#include "active_vision_control/simpeople.h"
#include "active_vision_control/simdetector.h"
#include <pthread.h>
void* runPeopleSimulation(void* simulationPeopleObj) {
    SimulationPeople* simPeople = static_cast<SimulationPeople*>(simulationPeopleObj);
    simPeople->peopleSimulationStart(nullptr);
    return nullptr;
}
void* runDetectorSimulation(void* simulationdetectorObj) {
    detector* simdetector = static_cast<detector*>(simulationdetectorObj);
    simdetector->detectorThread(nullptr);
    return nullptr;
}
int main(int argc, char** argv) {
    ros::init(argc, argv, "people_simulation_node");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(2);
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    spinner.start();
    SimulationPeople* mainpeople=new SimulationPeople(nh);
    robot_state::RobotStatePtr ll=arm.getCurrentState();
    
    detector* maindetector = new detector(nh, arm, PLANNING_GROUP);
    
    // 创建一个线程
    pthread_t thread_people;
    int thread_id = 0;  // 线程ID，可以根据需要修改
    int result = pthread_create(&thread_people, nullptr, runPeopleSimulation, mainpeople);
    if (result != 0) {
        ROS_ERROR("无法创建线程，错误代码: %d", result);
        return 1;
    }
    pthread_t thread_detector;
    result = pthread_create(&thread_detector, nullptr, runDetectorSimulation, maindetector);
    if (result != 0) {
        ROS_ERROR("无法创建线程，错误代码: %d", result);
        return 1;
    }
    ros::Rate sleepRate(10);
    while(ros::ok()){
        sleepRate.sleep();
    }
    return 0;
}

#include "active_vision_control/visualization.h"

// 构造函数
Visualization::Visualization(ros::NodeHandle &nh_, moveit::planning_interface::MoveGroupInterface &arm_, const std::string &PLANNING_GROUP_, int mode_)
    : nh(nh_),mode(mode_) {
    // 初始化ROS subscribers
    obsSub = nh.subscribe("/obsState", 10, &Visualization::obsCallback, this);
    cam1Sub = nh.subscribe("/cam1_pose", 10, &Visualization::cam1Callback, this);
    cam2Sub = nh.subscribe("/cam2_pose", 10, &Visualization::cam2Callback, this);
    SCEPub = nh.advertise<visualization_msgs::MarkerArray>("/SCE", 10);
    PoEPub = nh.advertise<geometry_msgs::PointStamped>("/PoE", 10);
    // PoEPub = nh.advertise<std_msgs::Float64>("/PoE", 10);
    curtime = 0.4;
    avg_PoE = 0;
    times = 0;
    testRobot = new JAKArobot(nh_, arm_, PLANNING_GROUP_);
    mRobot = new MoveitRobot(arm_, PLANNING_GROUP_);
    curVision1=new ActiveVision(servoState(0,0),Eigen::Vector3d(-0.24,-0.34,-0.82), 1,nh);
    curVision2=new ActiveVision(servoState(0,0),Eigen::Vector3d(-0.20 ,0.34 ,-0.84), 2,nh);
    cam1ok = false;
    cam2ok = false;
}

// 主函数
void Visualization::spin() {
    ros::Rate looprate(20);
    while((!cam1ok || !cam2ok) && ros::ok() ){
        ros::spinOnce();
        looprate.sleep();
    }
    
    while(ros::ok()){
        data_collide();
        visualize_SCE();
        ros::spinOnce();
        looprate.sleep();
        // ROS_INFO("excute time");
    }
    ROS_INFO("excute time");
}

// 其他成员函数的实现
void Visualization::obsCallback(const std_msgs::Float64MultiArray msg) {
    SCEs.clear();
    int obsNum=msg.data.size()/14;
    for(int i=0; i<obsNum; i++){
        envelopeShape::Cylinder* cyl;
        Eigen::Vector3d center;
        center << msg.data[0+14*i], msg.data[1+14*i], msg.data[2+14*i];
        Eigen::Vector3d zAxis;
        zAxis << msg.data[3+14*i], msg.data[4+14*i], msg.data[5+14*i];
        // 将旋转向量转化为AngleAxisd对象
        Eigen::AngleAxisd rotationAngleAxis(zAxis.norm(), zAxis.normalized());
        // 将AngleAxisd对象转化为旋转矩阵
        Eigen::Matrix3d rotationMatrix = rotationAngleAxis.toRotationMatrix();
        std::vector<double> paramm;
        paramm.push_back(msg.data[6+14*i]);
        paramm.push_back(msg.data[7+14*i]);
        cyl=new envelopeShape::Cylinder (paramm[0], paramm[1]);
        cyl->setPose(center, rotationMatrix);
        Eigen::Vector3d velo;
        velo << msg.data[8+14*i], msg.data[9+14*i], msg.data[10+14*i];
        Eigen::Vector3d Anglevelo;
        Anglevelo << msg.data[11+14*i], msg.data[12+14*i], msg.data[13+14*i];
        envelopeShape::movingObs cy(cyl,velo,Anglevelo,1,false);
        SCEs.push_back(cy);
    }
}

void Visualization::cam1Callback(const geometry_msgs::PoseStamped& msg) {
    cam1pose = poseStampedToMatrix4d(msg);
    // curVision1->setVisionState(curVision1->visionInverseKine(cam1pose.block<3,1>(0,3)));
    // curVision1->broadcastTF();
    cam1ok = true;
}

void Visualization::cam2Callback(const geometry_msgs::PoseStamped& msg) {
    cam2pose = poseStampedToMatrix4d(msg);
    // curVision2->setVisionState(curVision2->visionInverseKine(cam2pose.block<3,1>(0,3)));
    // curVision2->broadcastTF();
    cam2ok = true;
}
std::vector<Eigen::Vector3d> Visualization::genPointsInCylinder(envelopeShape::movingObs &myCylinder, int num_points){
    Eigen::Vector3d center;
    Eigen::Matrix3d orientation;
    std::vector<double> param;
    double radius;
    double height;
    myCylinder.getPose(center, orientation);
    myCylinder.getShape()->getShapeParameters(param);
    radius=param[0];
    height=param[1];
    std::vector<Eigen::Vector3d> points;

    int numOfz = num_points/10;
    for(int j=0; j<numOfz; j++){
        for (int i = 0; i < 10; ++i) {
            double angle = 2.0 * M_PI * static_cast<double>(i) / static_cast<double>(10);
            double x = radius * std::cos(angle);
            double y = radius * std::sin(angle);
            double z = static_cast<double>(j) / static_cast<double>(numOfz - 1) * height - (height / 2.0);
            Eigen::Vector3d point(x, y, z);
            points.push_back(orientation * point + center);
        }
    }
    // 生成圆柱顶部和底部的点
    Eigen::Vector3d top_center = center + (orientation * Eigen::Vector3d(0.0, 0.0, height / 2.0));
    Eigen::Vector3d bottom_center = center + (orientation * Eigen::Vector3d(0.0, 0.0, -height / 2.0));
    
    points.push_back(top_center); // 顶部中心点
    points.push_back(bottom_center); // 底部中心点

    return points;
}
bool Visualization::isVaildPoint(Eigen::Vector3d cyp, Eigen::Matrix4d endvision, std::vector<robotCylinder>&nextCover){
    double visionAngle=M_PI/3.0*2.0;
    Eigen::Vector4d blockpp(cyp(0),cyp(1),cyp(2),1);
    Eigen::Vector3d endInWorld=endvision.block<3,1>(0,3);
    Eigen::Vector4d blockInEnd=endvision.inverse()*blockpp;

    if(blockInEnd(2)<0)return false;
    if(sqrt(blockInEnd(1)*blockInEnd(1)+blockInEnd(0)*blockInEnd(0))/blockInEnd(2)>tan(visionAngle/2.0))return false;

    return true;
    int idd=0;

    for(auto cylin:nextCover){
        //获得遮挡面积以及表示
        idd++;
        Eigen::Matrix3d wcylinPose=cylin.getRotation();
        Eigen::Matrix3d endTransRotation=endvision.block<3,3>(0,0).transpose();
        Eigen::Matrix3d endcylinPose=endTransRotation*wcylinPose;
        Eigen::Vector3d worldInEnd=-endTransRotation*endInWorld;
        Eigen::Vector3d CylinWorldinEnd=endTransRotation*cylin.getCenter_trans();
        Eigen::Vector3d CylininEnd=worldInEnd+CylinWorldinEnd;
        Eigen::Vector3d zInEnd=endcylinPose.block<3,1>(0,2);
        
        double zangle=0;
        if(abs(zInEnd(2))<1e-8)zangle=3.1415926/2.0;
        else   zangle=abs(atan(sqrt(zInEnd(0)*zInEnd(0)+zInEnd(1)*zInEnd(1))/zInEnd(2)));
        
        double cylinWidth=cylin.getHeight()*sin(zangle)+cylin.getRadius()*cos(zangle)*2;
        
        double cylinlength=2.0*cylin.getRadius();
        

        if(blockInEnd(2)>CylininEnd(2)){
            double plantAngle=atan2(zInEnd(1),zInEnd(0));
           
            Eigen::Vector2d plantPoint(blockInEnd(0)*CylininEnd(2)/blockInEnd(2)-CylininEnd(0),blockInEnd(1)*CylininEnd(2)/blockInEnd(2)-CylininEnd(1));
            
            Eigen::Matrix2d TT;
            TT<<cos(plantAngle),sin(plantAngle),-sin(plantAngle),cos(plantAngle);
            Eigen::Vector2d plantPointInPlant=TT*plantPoint;

            if(abs(plantPointInPlant(0))<cylinWidth/2.0 && abs(plantPointInPlant(1))<cylinlength/2.0){
                return false;
            }
            
        }     
    }
    return true;
}
bool Visualization::isValid(envelopeShape::movingObs obs, std::vector<robotCylinder>&nextCover) {
    
    int num_sampling=50;
    std::vector<Eigen::Vector3d> cypoints = genPointsInCylinder(obs, num_sampling);
    int seeNum = 0;
    //tcp_mode
    if(mode == 2){
        Eigen::Vector3d endp = testRobot->getEnd().block<3,1>(0,3);
        cam1pose = curVision1->visionForwardKine(curVision1->visionInverseKine(endp));
        cam2pose = curVision2->visionForwardKine(curVision2->visionInverseKine(endp));
    }
    else if(mode == 3){
        cam1pose = curVision1->visionForwardKine(servoState(M_PI/3.0, M_PI/3.0));
        cam2pose = curVision2->visionForwardKine(servoState(M_PI*2.0/3.0, M_PI/3.0));
    }

    for(auto cyp: cypoints){
        if( isVaildPoint(cyp, cam1pose, nextCover) || isVaildPoint(cyp, cam2pose, nextCover)) seeNum++;
    }
    double visionProb = static_cast<double>(seeNum) / static_cast<double>(num_sampling+2);
    // if(visionProb>0.5)ROS_INFO("Is valid");
    // else ROS_INFO("Invalid with %f", visionProb);
    
    return (visionProb>0.3);
}

bool Visualization::collideJ(envelopeShape::movingObs obs, std::vector<robotCylinder>&nextCover){
    for(auto rc:nextCover){
        Eigen::Vector3d endp = rc.getCenter_trans();
        Eigen::Matrix4d obsCoor = Eigen::Matrix4d::Identity();
        Eigen::Vector3d centerPose;
        Eigen::Matrix3d rotationMatrix;
        // ROS_INFO("BARK2");
        obs.getShape()->getPose(centerPose, rotationMatrix);
        
        obsCoor.block<3, 1>(0, 3) = centerPose;
        obsCoor.block<3, 3>(0, 0) = rotationMatrix;
        
        Eigen::Vector4d endpp(endp(0),endp(1),endp(2),1);
        Eigen::Vector4d endInobs=obsCoor.inverse()*endpp;

        // std::cout<<endp<<std::endl;
        // std::cout<<endInobs<<std::endl;

        double r_uncertainty = obs.getRp() + obs.getRadius()+ obs.getRo() + rc.getRadius();
        double h_uncertainty = obs.getRp() + obs.getHeight()/2.0 + rc.getHeight()/2.0;

        if(sqrt(endpp(0)*endpp(0)+endpp(1)*endpp(1))<r_uncertainty && fabs(endp(2))< h_uncertainty) return true;
    }
    return false;
    
}

double Visualization::collideProb(envelopeShape::movingObs obs) {
    double pnotc=1;
    
    Eigen::Vector3d endp = testRobot->getEnd().block<3,1>(0,3);
    std::vector<robotCylinder> nextCover = mRobot->getCover();
    if(collideJ(obs, nextCover)){
        ROS_WARN("Collide!!");
        if(isValid(obs, nextCover)){
            obs.updateUncertainty(curtime, true);
            pnotc=obs.getUncertainty();
        }
        else {
            pnotc=0;
        }
    }
    else{
        double pNorm=1.0/2.0*obs.maxAcceleration*curtime*curtime;
        envelopeShape::movingObs fakeobss=obs.clone();
        fakeobss.addRadius(pNorm);
        fakeobss.addHeight(pNorm);
        // fcl::CollisionObjectd* curObsCollide=fakeobss->getCollision();
        if(isValid(obs, nextCover)) {
            pnotc=1;
        }
        else{
            obs.updateUncertainty(curtime, true);
            pnotc=obs.getUncertainty();
        }
    }
    // ROS_WARN("Collide prob is %f", pnotc);
    return pnotc;
}

void Visualization::visualize_SCE() {
    visualization_msgs::MarkerArray markerArray;
    for(int i=0;i<SCEs.size();i++){
        visualization_msgs::Marker SCE_marker = SCEs[i].getShape()->genCylinderRviz(i, cp[i]);
        markerArray.markers.push_back(SCE_marker);
    }
    
    // 发布MarkerArray
    SCEPub.publish(markerArray);
}

void Visualization::data_collide() {
    double PoE = 1;
    cp.clear();
    // visualization_msgs::MarkerArray markerArray;
    // ROS_INFO("Size of SCEs vector: %zu", SCEs.size());
    for(int i=0;i<SCEs.size();i++){
        double PoEi = collideProb(SCEs[i]);
        // ROS_INFO("Collide i is %f", PoEi);
        cp.push_back(PoEi);
        // visualization_msgs::Marker SCE_marker = SCEs[i].getShape()->genCylinderRviz(i);
        // markerArray.markers.push_back(SCE_marker);
        PoE *= PoEi;
    }
    PoE = 1-PoE;
    // 创建std_msgs::Float64消息
    // std_msgs::Float64 PoEMsg;
    // // 填充double数据
    // PoEMsg.data = PoE;  // 你的double数据
    // // 发布消息
    // PoEPub.publish(PoEMsg);

     // 创建一个PointStamped类型的消息
    geometry_msgs::PointStamped PoEMsg;

    // 设置时间戳
    PoEMsg.header.stamp = ros::Time::now();  // 使用当前时间戳
    PoEMsg.header.frame_id = "base_link";    // 设置坐标系

    // 填充double数据到Point.x
    PoEMsg.point.x = PoE;  // 替换PoE为你的double数据

    // 发布消息
    PoEPub.publish(PoEMsg);
    // SCEPub.publish(markerArray);
    
    // avg_PoE = avg_PoE*double(times)+PoE;
    // times++;
    // avg_PoE /= double(times);

    // ROS_INFO("Avg PoE is: %f", avg_PoE);
}

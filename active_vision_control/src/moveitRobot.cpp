#include "active_vision_control/moveitRobot.h"

MoveitRobot::MoveitRobot(robot_state::RobotStatePtr my_robot_state,const robot_state::JointModelGroup * myJoint,moveit::planning_interface::MoveGroupInterface &arm):
kinematic_state(my_robot_state),joint_model_group(myJoint)
{
    radius={0.065,0.07,0.06,0.06,0.05,0.03};height={0.2,0.425,0.24,0.15,0.21,0.18};
    this->arm_=&arm;
}

MoveitRobot::MoveitRobot(moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP)
{   
    radius={0.065,0.07,0.06,0.06,0.05,0.03};height={0.2,0.425,0.24,0.15,0.21,0.18};
    this->arm_=&arm;
    kinematic_state=arm_->getCurrentState();
    joint_model_group=arm_->getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    //std::cout<<"BARK"<<std::endl;
}

std::vector<robotCylinder>MoveitRobot::forwardKine(std::vector<double>& jointValue){
    kinematic_state->setJointGroupPositions(joint_model_group,  jointValue);
    std::vector<Eigen::Affine3d> manipPos;
	const Eigen::Affine3d & Link1pos = kinematic_state->getGlobalLinkTransform("link_1");
    manipPos.push_back(Link1pos);
    const Eigen::Affine3d & Link2pos = kinematic_state->getGlobalLinkTransform("link_2");
    manipPos.push_back(Link2pos);
    const Eigen::Affine3d & Link3pos = kinematic_state->getGlobalLinkTransform("link_3");
    manipPos.push_back(Link3pos);
    const Eigen::Affine3d & Link4pos = kinematic_state->getGlobalLinkTransform("link_4");
    manipPos.push_back(Link4pos);
    const Eigen::Affine3d & Link5pos = kinematic_state->getGlobalLinkTransform("link_5");
    manipPos.push_back(Link5pos);
    const Eigen::Affine3d & Link6pos = kinematic_state->getGlobalLinkTransform("link_6");
    manipPos.push_back(Link6pos);
    //cout<<"setPosOfCylinder!!!!!"<<endl;
    std::vector<robotCylinder> resCylinders;
    for(int id=0;id<6;id++){
        robotCylinder curLink(radius[id],height[id]);
        Eigen::Matrix3d Rota=manipPos[id].rotation();
        Eigen::Vector3d transl=manipPos[id].translation();
        //fcl固定在中心，需要移动到末端执行器
        Eigen::Matrix3d real_Rota;
        Eigen::Vector3d real_transl;
        //圆柱位姿调整
        if(id==0)
        {
            // fcl::Vector3d offse(0,0,0);
            // fcl::Vector3d real_tan;
            // real_tan=Rota*offse+transl;
            real_Rota=Rota;
            real_transl=transl;
        }
        else if(id==1)
        {
            Eigen::Matrix3d Rota_offset;
            Rota_offset<<0,0,1,
                        0,1,0,
                        -1,0,0;
            real_Rota=Rota*Rota_offset;
            Eigen::Vector3d offset((radius[0]+radius[1])/2.0+0.076,0,height[1]/2.0);
            real_transl=real_Rota*offset+transl;
        }    
        else if(id==2)
        {
            Eigen::Matrix3d Rota_offset;
            Rota_offset<<0,0,1,
                        0,1,0,
                        -1,0,0;
            real_Rota=Rota*Rota_offset;
            //std::cout<<"id "<<id<<real_Rota<<std::endl;
            Eigen::Vector3d offset(0,0,height[2]/2.0);
            real_transl=real_Rota*offset+transl;
        } 
        else if(id==3)
        {
           Eigen::Matrix3d Rota_offset;
            Rota_offset<<1,0,0,
                        0,1,0,
                        0,0,1;
            real_Rota=Rota*Rota_offset;
            Eigen::Vector3d offset(0,0,height[3]/2.0);
            //std::cout<<"id "<<id<<real_Rota<<std::endl;
            real_transl=real_Rota*offset+transl;
            real_transl=real_Rota*offset+transl;
        } 
        else if(id==4)
        {
            real_Rota=Rota;
            Eigen::Vector3d offset(0,0,-height[4]/2.0);
            real_transl=real_Rota*offset+transl;
        }
        else if(id==5)
        {
            real_Rota=Rota;
            Eigen::Vector3d offset(0,0,-height[5]/2.0);
            real_transl=real_Rota*offset+transl;
        }

        curLink.setTranslation(real_transl); 
        curLink.setRotation(real_Rota);
        resCylinders.push_back(curLink);
    }
    return resCylinders;
}

Eigen::Matrix4d MoveitRobot::forwordKineEnd(std::vector<double>& jointValue){
    kinematic_state->setJointGroupPositions(joint_model_group,  jointValue);
    const Eigen::Affine3d & Link6pos = kinematic_state->getGlobalLinkTransform("link_6");
    Eigen::Matrix4d endpose= Link6pos.matrix();
    return endpose;
}

void MoveitRobot::genCurrentCover(){
    std::vector<double> current_joint_values = arm_->getCurrentJointValues();
    robotCover=forwardKine(current_joint_values);
}
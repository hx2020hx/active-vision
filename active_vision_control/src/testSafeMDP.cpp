#include "active_vision_control/mdp.h"
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PolygonStamped.h>
ActiveVision* curVision1;
safeCircle* curSafeCircle;
JAKArobot* myrob;
std::vector<envelopeShape::movingObs*> curObsList;
void testSafeCircle(){
    safeCircle* cloneCircle=curSafeCircle->clone();
    curSafeCircle->updateSafeZone(0.05, servoState(M_PI/2.0,M_PI/2.0));
    curSafeCircle->updateSafeZone(0.4, servoState(M_PI*3.0/2.0,M_PI/2.0));
    curSafeCircle->updateSafeZone(0.4, servoState(M_PI/2.0,M_PI/2.0));
    curSafeCircle->updateSafeZone(0.4, servoState(M_PI*3.0/2.0,M_PI/2.0));
    // std::cout<<curSafeCircle->getAngleDepth(servoState(M_PI/2.0,M_PI/2.0))<<std::endl;
    // std::cout<<curSafeCircle->getAngleDepth(servoState(0,0))<<std::endl;
    // std::cout<<curSafeCircle->getAngleDepth(servoState(M_PI*5.0/6.0-0.1,M_PI/2.0))<<std::endl;

    // std::cout<<cloneCircle->getAngleDepth(servoState(M_PI/2.0,M_PI/2.0))<<std::endl;
}

// void testMDP(){
//     MDP mdpSolver;
//     std::vector<double>initNode={0.292701,1.513222,1.541136,1.659464,-1.568797,1.863675};
//     std::vector<double>pickNode={-0.531258,2.326090,1.441877,0.942127,-1.579983,1.044988};
//     robotConfig initConf(initNode);
//     robotConfig pickConf(pickNode);
//     robotTraj testTraj1(initConf, pickConf);

//     Eigen::Matrix3d rotationMatrix;
//     rotationMatrix<<1,0,0,0,1,0,0,0,1;
//     Eigen::Vector3d bodyCenter(-0.6, 0, -0.5);
//     envelopeShape::Cylinder* bodyCylinder=new envelopeShape::Cylinder(0.35, 0.6);
//     bodyCylinder->setPose(bodyCenter, rotationMatrix);
//     envelopeShape::movingObs* body = new envelopeShape::movingObs(bodyCylinder, Eigen::Vector3d(0,0,0), Eigen::Vector3d(0,0,0), 1, false);
//     mdpSolver.clearObs();
//     mdpSolver.addObs(body);
//     // mdpSolver.setRobotTraj(testTraj1);
//     // mdpSolver.setMoveitRobot(myrob);
//     mdpSolver.setWholeTime(1.00);
//     //注意是clone
//     mdpSolver.setSafeCircle(curSafeCircle->clone());
//     // for(auto obs:curObsList){
//     //     mdpSolver.addObs(obs);
//     // }
    
//     std::vector<servoState> curServoTraj=mdpSolver.solveMDP();
//     std::cout<<curServoTraj.size()<<std::endl;
//     for(auto tit:curServoTraj){
//         std::cout<<tit.getAngle1()<<' '<<tit.getAngle2()<<std::endl;
//     }
// }


int main(int argc, char** argv){
    ros::init(argc, argv, "activeVision");
    ros::AsyncSpinner spinner(1);//一定要用这个
    ros::NodeHandle nh;
    spinner.start();
    //机器人初始化
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    myrob=new JAKArobot(nh,arm, PLANNING_GROUP);
    //一些初始化
    curVision1=new ActiveVision(servoState(0,0));
    curSafeCircle=new safeCircle(curVision1, 10, nh);
    testSafeCircle();
    ros::Rate looprate(40);
    while(ros::ok()){
        curVision1->broadcastTF();
        curSafeCircle->visualize_safeCircle();
        looprate.sleep();
    }
    return 0;

    // ros::init(argc, argv, "polygon_publisher");
    // ros::NodeHandle nh;
    // ros::Publisher polygon_pub = nh.advertise<geometry_msgs::PolygonStamped>("polygon", 1);

    // // 创建一个矩形
    // geometry_msgs::PolygonStamped polygonStamped;
    // polygonStamped.header.frame_id = "map";  // 你的基坐标系
    // polygonStamped.header.stamp = ros::Time::now();

    // geometry_msgs::Point32 point;
    // point.x = 1.0;
    // point.y = 1.0;
    // polygonStamped.polygon.points.push_back(point);

    // point.x = 1.0;
    // point.y = -1.0;
    // polygonStamped.polygon.points.push_back(point);

    // point.x = -1.0;
    // point.y = -1.0;
    // polygonStamped.polygon.points.push_back(point);

    // point.x = -1.0;
    // point.y = 1.0;
    // polygonStamped.polygon.points.push_back(point);

    // // 发布PolygonStamped消息
    // while (ros::ok())
    // {
    //     polygon_pub.publish(polygonStamped);
    //     ros::spinOnce();
    //     ros::Rate(10).sleep();
    // }

    // return 0;
}
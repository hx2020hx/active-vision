#include <iostream>
#include <ros/ros.h>
#include <Eigen/Eigen>
#include <std_msgs/Float64.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include "active_vision_control/activeVision.h"
#include <math.h>
using namespace std;
using namespace Eigen;
const Vector3d base1(0,-0.35,-0.78);//主动视觉基坐标系在世界坐标系下的位置

ActiveVision device1(servoState(0,0),base1,1);//base为默认值


ros::Publisher servo1_pub;
ros::Publisher servo2_pub;
ros::Publisher end_pose_pub;


void pubServoMsg(){
    std_msgs::Float64 data1,data2;
    data1.data=device1.getVisionState().getAngle1()*180.0/3.1415926535;
    data2.data=device1.getVisionState().getAngle2()*180.0/3.1415926535;
    //std::cout<<data2.data<<std::endl;
    servo1_pub.publish(data1);
    servo2_pub.publish(data2);
}
void broadcast_tf(){
    //相机根部TF维护
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin(tf::Vector3(device1.getBase()(0),device1.getBase()(1),0));
    tf::Quaternion q;
    q.setRPY(0,0,0);
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "/fix_link"));
    //相机末端TF维护
    static tf::TransformBroadcaster br2;
    tf::Transform transform2;
    Matrix4d world_end_pose=device1.visionForwardKine();
    Vector3d camera_end_pos = world_end_pose.block<3,1>(0,3);
    transform2.setOrigin(tf::Vector3(camera_end_pos(0),camera_end_pos(1),(camera_end_pos(2)-device1.getBase()(2))));
    Quaterniond end_quad=Quaterniond(world_end_pose.block<3,3>(0,0));
    tf::Quaternion q2(end_quad.x(),end_quad.y(),end_quad.z(),end_quad.w());
    transform2.setRotation(q2);
    br2.sendTransform(tf::StampedTransform(transform2, ros::Time::now(), "base_link", "/camera_link"));
}

void positionCallback(tf::StampedTransform &msg)
{
    Vector3d current_robot_end_pose ;
    current_robot_end_pose<< msg.getOrigin().x(), msg.getOrigin().y(), msg.getOrigin().z();
    servoState curState=device1.visionInverseKine(current_robot_end_pose);
    device1.setVisionState(curState);
    pubServoMsg();
    //发布servo消息
    //cout<<"servo 1 move to"<<data1.data<<endl;
    //cout<<"servo 2 move to"<<data2.data<<endl;
    //发布末端消息
    broadcast_tf();
}
int main(int argc, char **argv){
    ros::init(argc, argv, "servo_controller");
    ros::NodeHandle nh;
    servo1_pub = nh.advertise<std_msgs::Float64> ("servo1", 10);  
    servo2_pub = nh.advertise<std_msgs::Float64> ("servo2", 10); 
    end_pose_pub=nh.advertise <geometry_msgs::PoseStamped> ("/camera_pose",10) ;

    tf::TransformListener listener;  // tf监听器
    ros::Rate rate(10.0);
    while (nh.ok())
    {
        tf::StampedTransform transform;
        try
        {
            // 查找turtle2与turtle1的坐标变换
            listener.waitForTransform("/world", "/link_6", ros::Time(0), ros::Duration(3.0));
            listener.lookupTransform("/world", "/link_6", ros::Time(0), transform);
            positionCallback(transform);
        }
        catch (tf::TransformException &ex) 
        {
            ROS_ERROR("%s",ex.what());
            ros::Duration(1.0).sleep();
            continue;
        }
 
        rate.sleep();
    }
    return 0;
}
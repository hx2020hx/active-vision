#include "active_vision_control/activeVision.h"
#include "active_vision_control/moveitRobot.h"
octomap::OcTree * sub_octree;
bool hasMap=false;
void octomapCallback(const octomap_msgs::Octomap & octo_map){
    octomap::AbstractOcTree* sTree=octomap_msgs::fullMsgToMap(octo_map);
    sub_octree = new octomap::OcTree(octo_map.resolution);
    sub_octree = dynamic_cast<octomap::OcTree*>(sTree);

    hasMap=true;
}

//state 类测试函数
void testForState(){
    servoState testState1(3.14*2-0.3, -3.14*6+0.4);
    //test for angle
    std::cout<<"test angle1 is "<<testState1.getAngle1()<<", angle2 is "<<testState1.getAngle2()<<std::endl;
    //test for state norm
    double testnorm=testState1.disCost(servoState(0.1, -0.2));
    std::cout<<"test norm is "<<testnorm<<std::endl;
}

//activeVision类测试函数
void testForActiveVision(){
    //test for kinemetics
    // 见servo_control_active
    std::vector<Eigen::Vector3d> testTraj;
    testTraj.push_back(0.001*Eigen::Vector3d(-400, 0, -300));
    testTraj.push_back(0.001*Eigen::Vector3d(400, -100, -700));
    testTraj.push_back(0.001*Eigen::Vector3d(-400, 200, -100));
    testTraj.push_back(0.001*Eigen::Vector3d(-400, -300, -300));
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    MoveitRobot testRobot(arm,PLANNING_GROUP);
    testRobot.genCurrentCover();
    std::vector<robotCylinder> testCylinderCover=testRobot.getCover(); 
    if(hasMap){
        
        ActiveVision testVision1(servoState(0,0));
        std::vector<double>testProfit;
        std::vector<servoState>testStateDecision;
        testProfit=testVision1.profitOfEachDecision(testTraj,testCylinderCover,sub_octree,testStateDecision);
        for(int i=0;i<testStateDecision.size();i++){
            std::cout<<"Profit1 is "<<testProfit[i]<<std::endl;
            std::cout<<"State is ("<<testStateDecision[i].getAngle1()<<", "<<testStateDecision[i].getAngle2()<<")"<<std::endl;
        }
        
    }
    else{
        std::cout<<"Has no map"<<std::endl;
    }

}

void testForZeroInput(){
    //test for zeroInput
    std::vector<Eigen::Vector3d> testTraj;
    testTraj.push_back(0.001*Eigen::Vector3d(-400, 0, -300));
    testTraj.push_back(0.001*Eigen::Vector3d(400, -100, -700));
    testTraj.push_back(0.001*Eigen::Vector3d(-400, 200, -100));
    testTraj.push_back(0.001*Eigen::Vector3d(-400, -300, -300));
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    MoveitRobot testRobot(arm,PLANNING_GROUP);
    testRobot.genCurrentCover();
    std::vector<robotCylinder> testCylinderCover=testRobot.getCover();   
    ActiveVision testVision1(servoState(0,0));
    std::vector<double>testProfit;
    std::vector<servoState>testStateDecision;
    testProfit=testVision1.profitOfEachDecision(testTraj,testCylinderCover,sub_octree,testStateDecision);
    for(int i=0;i<testStateDecision.size();i++){
        std::cout<<"Profit1 is "<<testProfit[i]<<std::endl;
        std::cout<<"State is ("<<testStateDecision[i].getAngle1()<<", "<<testStateDecision[i].getAngle2()<<")"<<std::endl;
    }
}
//visionDecision类测试函数
void testForVisionDecision(){
    ActiveVision testVision1(servoState(0,0));
    VisionDecision testDecision1;
    //轨迹测试
    std::vector<Eigen::Vector3d> testTraj;
    testTraj.push_back(0.001*Eigen::Vector3d(-400, 0, -300));
    testTraj.push_back(0.001*Eigen::Vector3d(400, -100, -700));
    testTraj.push_back(0.001*Eigen::Vector3d(-400, 200, -100));
    testTraj.push_back(0.001*Eigen::Vector3d(-400, -300, -300));//mm为单位
    //testVision1.getVisionDecision()->genTrajAction()
    testDecision1.genTrajAction(&testVision1, testTraj);
    for(auto decisionState:testDecision1.getTrajState()){
        testVision1.setVisionState(decisionState);
        std::cout<<"Cur decisionState is ("<<decisionState.getAngle1()<<", "<<decisionState.getAngle2()<<")"<<std::endl;
        testVision1.broadcastTF();
        usleep(5000*1000);
    }
}
//moveitRobot类测试函数
void testForMoveitRobot(){
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    MoveitRobot testRobot(arm,PLANNING_GROUP);
    testRobot.genCurrentCover();
    std::vector<robotCylinder> testCylinderCover=testRobot.getCover();
    int i=1;
    for(auto cc:testCylinderCover){
        std::cout<<"Link "<<i<<" :"<<std::endl;
        std::cout<<"Rotation: "<<std::endl;
        std::cout<<cc.getRotation()<<std::endl;
        std::cout<<"Translation: "<<std::endl;
        std::cout<<cc.getCenter_trans()<<std::endl;
        std::cout<<std::endl;
        i++;
    }

    MoveitRobot testRobot2(arm,PLANNING_GROUP);
    testRobot2.genCurrentCover();
    testCylinderCover=testRobot2.getCover();
    i=1;
    for(auto cc:testCylinderCover){
        std::cout<<"Link "<<i<<" :"<<std::endl;
        std::cout<<"Rotation: "<<std::endl;
        std::cout<<cc.getRotation()<<std::endl;
        std::cout<<"Translation: "<<std::endl;
        std::cout<<cc.getCenter_trans()<<std::endl;
        std::cout<<std::endl;
        i++;
    }
}

//costBlocks类测试函数
void testForCostBlocks(){
    //某点遮挡计算
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    MoveitRobot testRobot(arm,PLANNING_GROUP);
    testRobot.genCurrentCover();
    std::vector<robotCylinder> testCylinderCover=testRobot.getCover();
    costBlock testCostBlock;
    if(hasMap)testCostBlock.setOcT(sub_octree);
    else{
        std::cout<<"no octree"<<std::endl;
        return;
    }
    ActiveVision testVision1(servoState(0,3.1415926/2));
    bool testJudge=testCostBlock.isBlocked(0,0,-5,servoState(0,3.1415926/2),testCylinderCover, &testVision1);
    std::cout<<testJudge<<std::endl;

    //某点cost计算
    std::vector<Eigen::Vector3d> testTraj;
    testTraj.push_back(0.001*Eigen::Vector3d(-400, 0, -300));
    testTraj.push_back(0.001*Eigen::Vector3d(400, -100, -700));
    testTraj.push_back(0.001*Eigen::Vector3d(-400, 200, -100));
    testTraj.push_back(0.001*Eigen::Vector3d(-400, -300, -300));
    double testCostTraj;
    testCostTraj=testCostBlock.costOfBlock(-4,0,-3,testVision1.visionInverseKine(0.001*Eigen::Vector3d(-400, 0, -300)),testCylinderCover,testTraj,&testVision1);
    std::cout<<"Center Cost"<<testCostTraj<<std::endl;
    testCostTraj=testCostBlock.costOfBlock(-3,0,-3,testVision1.visionInverseKine(0.001*Eigen::Vector3d(-400, 0, -300)),testCylinderCover,testTraj,&testVision1);
    std::cout<<"no Center Cost"<<testCostTraj<<std::endl;
    testCostTraj=testCostBlock.costOfBlock(4,0,-3,testVision1.visionInverseKine(0.001*Eigen::Vector3d(-400, 0, -300)),testCylinderCover,testTraj,&testVision1);
    std::cout<<"Far Cost"<<testCostTraj<<std::endl;
    testCostTraj=testCostBlock.costOfBlock(4,3,-3,testVision1.visionInverseKine(0.001*Eigen::Vector3d(-400, 0, -300)),testCylinderCover,testTraj,&testVision1);
    std::cout<<"no block Cost"<<testCostTraj<<std::endl;
    testCostTraj=testCostBlock.costOfBlock(-4,-2,-4,testVision1.visionInverseKine(0.001*Eigen::Vector3d(-400, 0, -300)),testCylinderCover,testTraj,&testVision1);
    std::cout<<"block Cost"<<testCostTraj<<std::endl;

    //某状态计算
    testCostTraj=testCostBlock.costOfState(testVision1.visionInverseKine(0.001*Eigen::Vector3d(-400, 0, -300)),testCylinderCover,testTraj,&testVision1);
    std::cout<<"Traj Cost"<<testCostTraj<<std::endl;
    testCostTraj=testCostBlock.costOfState(testVision1.visionInverseKine(Eigen::Vector3d(-0.4, -0.2, -0.4)),testCylinderCover,testTraj,&testVision1);
    std::cout<<"obs Cost"<<testCostTraj<<std::endl;
    testCostTraj=testCostBlock.costOfState(testVision1.visionInverseKine(0.001*Eigen::Vector3d(0.4, 0.3, -0.3)),testCylinderCover,testTraj,&testVision1);
    std::cout<<"nothing Cost"<<testCostTraj<<std::endl;

    //障碍中心位置
    std::vector<Eigen::Vector3d>testCenter=testCostBlock.blockCoff();
    std::cout<<"Cur obs Center"<<std::endl;
    for(auto pp:testCenter){
        std::cout<<pp<<std::endl;
        std::cout<<std::endl;
    }
    
} 

int main(int argc, char** argv){
    ros::init(argc, argv, "testnode");
    ros::AsyncSpinner spinner(1);//一定要用这个
    ros::NodeHandle nh;
    spinner.start();
    ros::Subscriber octomapSub=nh.subscribe("/octomap_full", 10, octomapCallback);
    //ros::Rate looprate(20);
    //testForState();

    //testForVisionDecision();

    //testForMoveitRobot();
    ros::spinOnce();
    //testForCostBlocks();
    testForZeroInput();
    return 0;

}
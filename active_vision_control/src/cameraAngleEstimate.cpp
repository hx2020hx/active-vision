#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include "active_vision_control/activeVision.h"

void cameraPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    // Received estimated camera pose
    // ROS_INFO("Received Camera Pose:");
    // ROS_INFO("Position (x, y, z): (%f, %f, %f)",
    //          msg->pose.position.x,
    //          msg->pose.position.y,
    //          msg->pose.position.z);
    // ROS_INFO("Orientation (x, y, z, w): (%f, %f, %f, %f)",
    //          msg->pose.orientation.x,
    //          msg->pose.orientation.y,
    //          msg->pose.orientation.z,
    //          msg->pose.orientation.w);

    ActiveVision cameraLink(servoState(0,0));
    servoState estimateState = cameraLink.visionInverseKine(Eigen::Vector3d(msg->pose.position.x,msg->pose.position.y,msg->pose.position.z));
    std::cout<<"Estimate state is ("<<estimateState.getAngle1()<<" ,"<<estimateState.getAngle2()<<")"<<std::endl;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "camera_pose_subscriber");
    ros::NodeHandle nh;

    ros::Subscriber camera_pose_subscriber = nh.subscribe("/camera_pose_world", 1, cameraPoseCallback);
    ros::spin();

    return 0;
}
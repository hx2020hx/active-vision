#include <ros/ros.h>
#include <Eigen/Dense>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/PoseStamped.h>
#include "active_vision_control/activeVision.h"
#include "active_vision_control/trajWithSpeed.h"

std::vector<Eigen::Matrix4d> zn_list;
std::vector<servoState> servolist;
std::vector<Eigen::Matrix4d> endplist;
bool isReturn =true;
bool locked = false;
void printServoState(servoState theta_i) {
    std::cout << theta_i.getAngle1() << ',' << theta_i.getAngle2() << std::endl;
}

void printMatrix4d(const Eigen::Matrix4d& matrix) {
    // Assuming you want to print the entire matrix

    std::cout << matrix(0,0)<< ','<<matrix(0,1)<< ','<<matrix(0,2)<< ','<<matrix(0,3)<< ','<< std::endl;
    std::cout << matrix(1,0)<< ','<<matrix(1,1)<< ','<<matrix(1,2)<< ','<<matrix(1,3)<< ','<< std::endl;
    std::cout << matrix(2,0)<< ','<<matrix(2,1)<< ','<<matrix(2,2)<< ','<<matrix(2,3)<< ','<< std::endl;
    std::cout << matrix(3,0)<< ','<<matrix(3,1)<< ','<<matrix(3,2)<< ','<<matrix(3,3)<< std::endl;
}
Eigen::MatrixXd logT(const Eigen::MatrixXd& T_SE3) {
    Eigen::Matrix3d R = T_SE3.block<3, 3>(0, 0);
    Eigen::Vector3d p = T_SE3.block<3, 1>(0, 3);
    double theta = acos(0.5 * (R.trace() - 1));

    Eigen::Matrix3d Omega;
    Eigen::Matrix3d V_inv;
    Eigen::Vector3d w(0, 0, 0);

    if (theta == 0) {
        Omega.setZero();
        V_inv.setIdentity();
    } else {
        double A = sin(theta) / theta;
        double B = (1 - cos(theta)) / (theta * theta);
        Omega = (theta / 2 / sin(theta)) * (R - R.transpose());
        V_inv = Eigen::Matrix3d::Identity() - 0.5 * Omega + 1 / (theta * theta) * (1 - A / 2 / B) * Omega * Omega;
        w << Omega(2, 1), Omega(0, 2), Omega(1, 0);
    }

    Eigen::MatrixXd T_se3(6, 1);
    T_se3.block<3, 1>(0, 0) = V_inv * p;
    T_se3.block<3, 1>(3, 0) = w;
    
    return T_se3;
}

double cal_en(const Eigen::VectorXd& promVar) {
    double ennorm = 0.0;
    // Eigen::MatrixXd data_array; // Load this from your file or source
    
    Eigen::Vector3d base_pose = promVar.segment<3>(0);
    Eigen::Vector2d deltaTheta = promVar.segment<2>(3);
    double dcur = promVar(5);

    // Initialize curVision (assumed class or function)
    // activeVisionBase curVision(base_pose, 0.069, 0.02, 0.061);
    ActiveVision curVision(servoState(0,0), base_pose, -1);
    // std::cout<<"T matrix is :";
    for (int i = 0; i < zn_list.size(); ++i) {
        Eigen::Matrix4d transform_matrix = Eigen::Matrix4d::Identity();
        transform_matrix.block<3, 1>(0, 3) = Eigen::Vector3d(0, 0, dcur);
        Eigen::Matrix4d Tz;
        // Tz<<-1,0,0,0,
        //     0,-1,0,0,
        //     0,0,1,0,
        //     0,0,0,1;

        Tz<<0,1,0,0,
            -1,0,0,0,
            0,0,1,0,
            0,0,0,1;

        Eigen::MatrixXd Tn =  transform_matrix * endplist[i]*zn_list[i].inverse()*Tz;

        Eigen::MatrixXd T_theta = curVision.visionForwardKine(servoState(servolist[i].getAngle1()+deltaTheta(0), servolist[i].getAngle2()+deltaTheta(1)));
        // printMatrix4d(T_theta);
        // std::cout<<std::endl;
        Eigen::MatrixXd T_error = T_theta.inverse() * Tn;

        Eigen::MatrixXd en = logT(T_error);
        ennorm += en.norm();
    }
    return ennorm;
}

void znCallback(geometry_msgs::PoseStamped msg){
    // Extract position and orientation from PoseStamped message
    Eigen::Vector3d position(msg.pose.position.x, msg.pose.position.y, msg.pose.position.z);
    Eigen::Quaterniond orientation(msg.pose.orientation.w, msg.pose.orientation.x,
                                   msg.pose.orientation.y, msg.pose.orientation.z);

    // Create a 4x4 transformation matrix
    Eigen::Matrix4d transform_matrix = Eigen::Matrix4d::Identity();

    // Set the top-left 3x3 submatrix to the rotation matrix
    transform_matrix.block<3, 3>(0, 0) = orientation.toRotationMatrix();

    // Set the top-right 3x1 submatrix to the translation vector
    transform_matrix.block<3, 1>(0, 3) = position;

    // Add the matrix to the vector
    if(!locked) {
        zn_list.push_back(transform_matrix);
        locked = true;
        std::cout<<"receive "<<zn_list.size()<<std::endl;
    }
    
    isReturn = false;
}
int main(int argc, char** argv) {
    //step1：采集数据
    ros::init(argc, argv, "calibration");
	ros::AsyncSpinner spinner(1);
	ros::NodeHandle nh;
    spinner.start();
    static const std::string PLANNING_GROUP = "arm";
	moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    JAKArobot* testRobot;
    if(argv[1]!=NULL){
        testRobot=new JAKArobot(nh,arm, PLANNING_GROUP, argv[1]);
    }
    else{
        testRobot=new JAKArobot(nh,arm, PLANNING_GROUP);
    }
    ros::Publisher commandPub = nh.advertise<std_msgs::Bool>("estimate_flag", 10);
    ros::Subscriber znSub = nh.subscribe("/cam_1/trans", 1, znCallback);
    // ActiveVision cali_device(servoState(0,0), Eigen::Vector3d(-0.20,-0.4,-0.8), 1, nh);//--------------------init-----------------
    ActiveVision cali_device(servoState(0,0), Eigen::Vector3d(-0.20 ,0.34 ,-0.8), 2, nh);//--------------------init-----------------
    
//     cali_device.setDeltas(Eigen::Vector2d(-1.51997, -0.800289));
    std::vector<robotConfig> calibrationList;
    

    //10个位姿
    // std::vector<double>jv_1={0.506836,1.7887,1.19807,1.74217,5.04856,1.86366};
    // robotConfig cali_1(jv_1);
    // calibrationList.push_back(cali_1);

    // std::vector<double>jv_2={0.323837,1.90178,1.19821,1.9192,5.31769,1.86364};
    // robotConfig cali_2(jv_2);
    // calibrationList.push_back(cali_2);

    // std::vector<double>jv_3={0.431336,2.14167,1.19832,1.91913,5.38759,1.86366};
    // robotConfig cali_3(jv_3);
    // calibrationList.push_back(cali_3);

    // std::vector<double>jv_4={0.301846,2.34402,1.33824,2.1172,5.74096,1.86368};
    // robotConfig cali_4(jv_4);
    // calibrationList.push_back(cali_4);

    // std::vector<double>jv_5={0.418656,1.63176,1.05139,2.20107,5.08409,1.86366};
    // robotConfig cali_5(jv_5);
    // calibrationList.push_back(cali_5);

    // std::vector<double>jv_6={0.422288,1.72022,1.21453,1.76347,5.53236,1.86367};
    // robotConfig cali_6(jv_6);
    // calibrationList.push_back(cali_6);

    // std::vector<double>jv_7={0.115983,1.73613,1.32506,2.5999,5.82515,1.86368};
    // robotConfig cali_7(jv_7);
    // calibrationList.push_back(cali_7);

    // std::vector<double>jv_8={6.26049,1.8929,1.09096,2.88575,5.5075,1.86368};
    // robotConfig cali_8(jv_8);
    // calibrationList.push_back(cali_8);

    // std::vector<double>jv_9={6.26061,2.13206,1.14814,2.62383,5.50751,1.86368};
    // robotConfig cali_9(jv_9);
    // calibrationList.push_back(cali_9);

    // std::vector<double>jv_10={0.0279588,2.18425,1.13898,2.43448,5.86494,1.86372};
    // robotConfig cali_10(jv_10);
    // calibrationList.push_back(cali_10);
    std::vector<double>jv_1={0.212408,2.31021,1.17782,2.33133,4.09969,1.86376};
    robotConfig cali_1(jv_1);
    calibrationList.push_back(cali_1);

    std::vector<double>jv_2={6.24222,2.31004,1.17779,2.40638,4.14382,1.86374};
    robotConfig cali_2(jv_2);
    calibrationList.push_back(cali_2);

    std::vector<double>jv_3={6.24221,2.11597,1.1388,2.4064,4.21255,1.8637};
    robotConfig cali_3(jv_3);
    calibrationList.push_back(cali_3);

    std::vector<double>jv_4={0.234459,2.11608,1.03796,2.40626,4.49569,1.86375};
    robotConfig cali_4(jv_4);
    calibrationList.push_back(cali_4);

    std::vector<double>jv_5={0.234423,1.92581,1.00937,2.46135,4.24425,1.86375};
    robotConfig cali_5(jv_5);
    calibrationList.push_back(cali_5);

    std::vector<double>jv_6={0.00295966,1.92572,1.00937,2.54196,4.33854,1.86369};
    robotConfig cali_6(jv_6);
    calibrationList.push_back(cali_6);

    std::vector<double>jv_7={0.00298363,1.65949,1.00963,2.4295,4.23409,1.86373};
    robotConfig cali_7(jv_7);
    calibrationList.push_back(cali_7);

    std::vector<double>jv_8={0.321956,1.53015,1.14608,2.50318,4.39912,1.86369};
    robotConfig cali_8(jv_8);
    calibrationList.push_back(cali_8);

    std::vector<double>jv_9={0.0591777,1.39385,2.22983,1.35993,3.89279,1.86372};
    robotConfig cali_9(jv_9);
    calibrationList.push_back(cali_9);

    std::vector<double>jv_10={0.0275753,2.15995,1.16612,2.46564,4.2423,1.86378};
    robotConfig cali_10(jv_10);
    calibrationList.push_back(cali_10);

    for(int k=0; k<10; k++){
        testRobot->goToConf(calibrationList[k], 2);
        locked = false;
        Eigen::Vector3d endp = testRobot->forwardKineEnd(calibrationList[k]).block<3,1>(0,3);
        servoState theta_i = cali_device.visionInverseKine(endp);
        cali_device.goToState(theta_i);
        usleep(1e6);
        std_msgs::Bool start_estimate;
        start_estimate.data = true;
        commandPub.publish(start_estimate);
        ros::Rate looprate(20);
        isReturn=true;
        while(ros::ok() && isReturn){
            std_msgs::Bool start_estimate;
            start_estimate.data = true;
            commandPub.publish(start_estimate);
            ros::spinOnce();
            looprate.sleep();
        }
        servolist.push_back(theta_i);
        endplist.push_back(testRobot->forwardKineEnd(calibrationList[k]));
    }
    // // Print zn_list
    std::cout << "zn_list:" << std::endl;
    for (const auto& matrix : zn_list) {
        printMatrix4d(matrix);
    }

    // Print endplist
    std::cout << "endplist:" << std::endl;
    for (const auto& matrix : endplist) {
        printMatrix4d(matrix);
    }

    // Print servolist
    std::cout << "servolist:" << std::endl;
    for (const auto& theta_i : servolist) {
        printServoState(theta_i);
    }

    std::cout<<"Start cal"<<std::endl;


    
//     Eigen::Matrix4d zn_1;
//     zn_1<<0.555675,0.828632,-0.0677755,0.0183508,
// 0.757486,-0.470987,0.452092,-0.00210763,
// 0.342696,-0.302555,-0.889393,0.457505,
// 0,0,0,1;
//     zn_list.push_back(zn_1);
//     Eigen::Matrix4d endp_1;
//     endp_1<<0.967858,0.181794,0.173785,-0.457964,
// 0.225643,-0.932859,-0.280821,-0.157249,
// 0.111066,0.311008,-0.943895,-0.384102,
// 0,0,0,1;
//     endplist.push_back(endp_1);
//     servoState sv_1(0.815771,0.760475);
//     servolist.push_back(sv_1);
//     Eigen::Matrix4d zn_2;
//     zn_2 << 0.741913,0.670311,0.0157736,0.0278503,
// 0.640397,-0.715381,0.279501,-0.00983215,
// 0.198637,-0.197264,-0.960016,0.466913,
// 0,0,0,1;
//     zn_list.push_back(zn_2);

//     Eigen::Matrix4d zn_3;
//     zn_3 << 0.85787,0.511083,0.0534196,0.0272392,
// 0.472369,-0.825246,0.309575,0.00123268,
// 0.202303,-0.240341,-0.949373,0.369205,
// 0,0,0,1;
//     zn_list.push_back(zn_3);

//     Eigen::Matrix4d zn_4;
//     zn_4 << 0.993271,0.11498,-0.01384,0.035474,
// 0.115581,-0.976682,0.180924,-0.0117271,
// 0.00728539,-0.181307,-0.9834,0.352626,
// 0,0,0,1;
//     zn_list.push_back(zn_4);

//     Eigen::Matrix4d zn_5;
//     zn_5 << 0.441215,0.888432,0.126561,0.0254649,
// 0.808968,-0.454809,0.372452,-0.039841,
// 0.388459,-0.0619475,-0.919381,0.526549,
// 0,0,0,1;
//     zn_list.push_back(zn_5);

//     Eigen::Matrix4d zn_6;
//     zn_6 << 0.45892,0.842253,-0.282848,0.0133012,
// 0.869209,-0.49155,-0.053429,-0.0301342,
// -0.184035,-0.221334,-0.957676,0.487636,
// 0,0,0,1;
//     zn_list.push_back(zn_6);

//     Eigen::Matrix4d zn_7;
//     zn_7 << 0.964433,0.263178,-0.0246281,0.0310956,
// 0.248686,-0.93499,-0.25288,-0.0398691,
// -0.0895794,0.237761,-0.967184,0.488541,
// 0,0,0,1;
//     zn_list.push_back(zn_7);

//     Eigen::Matrix4d zn_8;
//     zn_8 << 0.965133,0.0784128,0.249738,0.0378047,
// 0.132698,-0.968959,-0.208588,-0.0437549,
// 0.22563,0.234455,-0.94558,0.540355,
// 0,0,0,1;
//     zn_list.push_back(zn_8);

//     Eigen::Matrix4d zn_9;
//     zn_9 << 0.978081,0.00920497,0.208022,0.031116,
// 0.0151443,-0.999521,-0.0269769,-0.0273352,
// 0.207674,0.029536,-0.977752,0.510775,
// 0,0,0,1;
//     zn_list.push_back(zn_9);

//     Eigen::Matrix4d zn_10;
//     zn_10 << 0.947854,0.115974,-0.296856,0.0203743,
// 0.112984,-0.993222,-0.0272719,-0.0238627,
// -0.298007,-0.00769009,-0.954533,0.503941,
// 0,0,0,1;
//     zn_list.push_back(zn_10);

//     // Repeat the above for other endp_values
//     Eigen::Matrix4d endp_2;
//     endp_2 << 0.893786,-0.16634,0.416506,-0.486407,
// 0.0495995,-0.886322,-0.460406,-0.0972508,
// 0.445742,0.432163,-0.783932,-0.44342,
// 0,0,0,1;
//     endplist.push_back(endp_2);

//     Eigen::Matrix4d endp_3;
//     endp_3 << 0.75194,-0.194201,0.629978,-0.512366,
// 0.0980007,-0.912076,-0.398136,-0.172657,
// 0.651907,0.361113,-0.666795,-0.563998,
// 0,0,0,1;
//     endplist.push_back(endp_3);

//     Eigen::Matrix4d endp_4;
//     endp_4 << 0.260405,-0.6745,0.690825,-0.499046,
// -0.0749642,-0.727475,-0.682027,-0.117846,
// 0.962585,0.125816,-0.240002,-0.702388,
// 0,0,0,1;
//     endplist.push_back(endp_4);

//     Eigen::Matrix4d endp_5;
//     endp_5 << 0.954822,0.0484423,0.293205,-0.379851,
// 0.130452,-0.954807,-0.267069,-0.0795719,
// 0.267016,0.293252,-0.91799,-0.302462,
// 0,0,0,1;
//     endplist.push_back(endp_5);

//     Eigen::Matrix4d endp_6;
//     endp_6 << 0.956679,0.0134395,0.290834,-0.432202,
// 0.213919,-0.710059,-0.670861,-0.142116,
// 0.197493,0.704014,-0.682174,-0.336696,
// 0,0,0,1;
//     endplist.push_back(endp_6);

//     Eigen::Matrix4d endp_7;
//     endp_7 << 0.359921,-0.811287,0.460728,-0.387547,
// -0.0865963,-0.52074,-0.849312,-0.0128483,
// 0.928955,0.265788,-0.25768,-0.42074,
// 0,0,0,1;
//     endplist.push_back(endp_7);

//     Eigen::Matrix4d endp_8;
//     endp_8 << 0.191387,-0.756962,0.624804,-0.391334,
// -0.206556,-0.653378,-0.728308,0.0579991,
// 0.959534,0.0103316,-0.281403,-0.423588,
// 0,0,0,1;
//     endplist.push_back(endp_8);

//     Eigen::Matrix4d endp_9;
//     endp_9 << 0.158281,-0.756805,0.634187,-0.464199,
// -0.205783,-0.653463,-0.72845,0.0595965,
// 0.965712,-0.0152045,-0.259169,-0.548343,
// 0,0,0,1;
//     endplist.push_back(endp_9);

//     Eigen::Matrix4d endp_10;
//     endp_10 << 0.255173,-0.890467,0.376769,-0.520625,
// -0.110189,-0.413912,-0.903624,0.0159684,
// 0.960596,0.189065,-0.203739,-0.559717,
// 0,0,0,1;
//     endplist.push_back(endp_10);

//     // Repeat the above for other servolist_values
//     servoState sv_2(0.757667, 0.944151);
//     servolist.push_back(sv_2);

//     servoState sv_3(0.941649, 1.1264);
//     servolist.push_back(sv_3);

//     servoState sv_4(0.814455, 1.49693);
//     servolist.push_back(sv_4);

//     servoState sv_5(0.511466, 0.70967);
//     servolist.push_back(sv_5);

//     servoState sv_6(0.733043, 0.697072);
//     servolist.push_back(sv_6);

//     servoState sv_7(0.451112, 0.950885);
//     servolist.push_back(sv_7);

//     servoState sv_8(0.395723, 1.02321);
//     servolist.push_back(sv_8);

//     servoState sv_9(0.521721, 1.23857);
//     servolist.push_back(sv_9);

//     servoState sv_10(0.656676, 1.24904);
//     servolist.push_back(sv_10);

    //step2:优化计算


    double learning_rate = 0.005;
    int num_iterations = 10000;

    Eigen::VectorXd var(6); // Initialize var vector with appropriate size and values
    // var << -0.20,-0.4,-0.8,0,0, 0.00836;
    var << -0.2,0.34 ,-0.8,0,0, 0.00836;//--------------------------init------------------------
    // Print the optimized var values
    std::cout << "Optimized var values: " << var.transpose() << std::endl;

    // Calculate cost after optimization
    double optimized_cost = cal_en(var);
    std::cout << "Cost before optimization: " << optimized_cost << std::endl;
    //注意是正负值
    double scale_x = 0.2;
    double scale_y = 0.2;
    double scale_z = 0.2;
    double scale_theta1 = M_PI/9.0;
    double scale_theta2 = M_PI/9.0;

    double curminen = optimized_cost;
    Eigen::VectorXd curminvar = var;
    for(int dx=0;dx<10;dx++){
        for(int dy=0;dy<10;dy++){
            for(int dz=0;dz<10;dz++){
                for(int dtheta1=0;dtheta1<10;dtheta1++){
                    for(int dtheta2=0;dtheta2<10;dtheta2++){
                        Eigen::VectorXd varnow(6);
                    varnow(0)=var(0)+scale_x * double(dx) / 10.0 - scale_x / 2;
                    varnow(1)=var(1)+scale_y * double(dy) / 10.0 - scale_y / 2;
                    varnow(2)=var(2)+scale_z * double(dz) / 10.0 - scale_z / 2;
                    varnow(3)=var(3)+scale_theta1 * double(dtheta1) / 10.0 - scale_theta1/2 ;
                    varnow(4)=var(4)+scale_theta2 * double(dtheta2) / 10.0 - scale_theta2/2 ;
                    // varnow(3)=var(3);
                    // varnow(4)=var(4);
                    varnow(5)=var(5);

                    double curen = cal_en(varnow);
                    if(curen<curminen){
                        curminen = curen;
                        curminvar = varnow;
                    }
                    }
                }
                
                    
            }
        }
    }


    // double last_state = 0;
    // double epsilon = 1e-5;
    // for (int i = 0; i < num_iterations; ++i) {
    //     double cost = cal_en(var);
        
    //     if(fabs(last_state - cost)<epsilon){
    //         epsilon+=1e-5;
    //     }
        
    //     std::cout<<"cost is "<<cost<<std::endl;
    //     std::cout<<"epsilon is "<<epsilon<<std::endl;
    //     if(cost<1)break;
        
    //     Eigen::VectorXd gradient = Eigen::VectorXd::Zero(var.size());

    //     for (int j = 0; j < var.size() - 1; ++j) {
    //         Eigen::VectorXd var_plus = var;
    //         var_plus(j) += epsilon;
    //         Eigen::VectorXd var_minus = var;
    //         var_minus(j) -= epsilon;
    //         gradient(j) = (cal_en(var_plus) - cal_en(var_minus)) / (2 * epsilon);
    //     }

    //     var -= learning_rate * gradient;
    //     last_state = cost;
    // }
    // std::cout << "Optimized var values: " << var.transpose() << std::endl;

    // // Calculate cost after optimization
    // optimized_cost = cal_en(var);
    // std::cout << "Cost before optimization: " << optimized_cost << std::endl;
    
    // Print the optimized var values
    std::cout << "Optimized var values: " << curminvar.transpose() << std::endl;

    // Calculate cost after optimization
    optimized_cost = cal_en(var);
    std::cout << "Cost after optimization: " << curminen << std::endl;

    // You may use the optimized 'var' and its associated cost for further processing or analysis
    return 0;
}
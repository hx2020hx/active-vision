#include "active_vision_control/activeVision.h"

int main(int argc, char** argv){
    ros::init(argc, argv, "ActiveSwarmNode");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(1);
    ros::Rate looprate(10);
    spinner.start();
    ActiveVision testvision(servoState(0,0), Eigen::Vector3d(-0.20,-0.4,-0.8), 1, nh, argc, argv);
     ActiveVision testvision2(servoState(0,0), Eigen::Vector3d(-0.20,-0.4,-0.8), 2, nh, argc, argv);
    testvision.goToState(servoState(M_PI/2, M_PI/2));
    testvision2.goToState(servoState(M_PI/2, M_PI/2));
    std::cout<<"finish"<<std::endl;
    testvision.goToState(servoState(0, 0));
    std::cout<<"finish"<<std::endl;
}
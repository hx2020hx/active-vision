/*
    main程序，用于主动视觉的规划，节点接收外界发来的robotTraj，以及运行时间，动态障碍物的状态估计等等元素进行规划
    节点规划完成后会发出轨迹信息指导舵机进行转动。
*/
#include "active_vision_control/activeVision.h"
#include "active_vision_control/envelopeShape.h"
#include "active_vision_control/Gaussian3D.h"
#include "active_vision_control/movingObs.h"
#include "active_vision_control/safeCircle.h"
#include "active_vision_control/mdp.h"
#include <ros/ros.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Float64.h>
#include <pthread.h>
#include <filesystem>
//线程参数传递结构体
typedef struct {
    servoState servoAngle;
    double delta_angle;
} ThreadParameters;
//注意这些都是全局变量，在模拟规划中请使用这些的clone。
std::vector<Eigen::Vector3d> curTraj;
double curTrajTime = 2.0;
double speed_coff = 0.8;
// JAKArobot* myrob;
std::vector<envelopeShape::movingObs> curObsList;
ActiveVision* curVision1;
ActiveVision* curVision2;
safeCircle* curSafeCircle1;
safeCircle* curSafeCircle2;
safeTimer* curTimer;
// robotConfig curConfig;

//ros pub接口
// ros::Publisher servo1pub,servo2pub;

//一些bool量
bool hasPlanned=false;
bool needReplan=false;
bool hasTraj=false;

//callback函数
void robotTrajCallback(const std_msgs::Float64MultiArray msg);
void obsStateCallback(const std_msgs::Float64MultiArray msg);
void rbStateCallback(const std_msgs::Float64MultiArray msg);

//随时间和角度变化需要更改安全圈
void updateSafeCircle(double time, servoState curServoState1, servoState curServoState2);
void planAndPub();

//线程函数，执行异步控制指令
void *thread_function1(void *arg);
void *thread_function2(void *arg);
int main(int argc, char** argv){
    ros::init(argc, argv, "activeVision");
    ros::AsyncSpinner spinner(1);//一定要用这个
    ros::NodeHandle nh;
    spinner.start();
    std::remove("/home/xian/jaka_ws/src/active_vision_control/result/output.txt");
    //机器人初始化
    // static const std::string PLANNING_GROUP = "arm";
	// moveit::planning_interface::MoveGroupInterface arm(PLANNING_GROUP);
    // myrob=new JAKArobot(nh,arm, PLANNING_GROUP);
    //publisher初始化，subscriber初始化
    ros::Subscriber robotTrajSub=nh.subscribe("/activeTraj", 10, robotTrajCallback);
    ros::Subscriber obsStateSub=nh.subscribe("/obsState", 10, obsStateCallback);
    // ros::Subscriber RobotStateSub=nh.subscribe("/rbstate", 10, rbStateCallback);
    //一些初始化
    curVision1=new ActiveVision(servoState(0,0),Eigen::Vector3d(-0.2,-0.36,-0.8), 1,nh);
    curVision2=new ActiveVision(servoState(0,0),Eigen::Vector3d(-0.22 ,0.348 ,-0.8), 2,nh);
    curVision1->setDeltas(Eigen::Vector2d( 0.0698,-0.174533));
    curVision2->setDeltas(Eigen::Vector2d( 0.0698 ,-0.0349066));
    curSafeCircle1=new safeCircle(curVision1, 10, nh);
    curSafeCircle2=new safeCircle(curVision2, 10, nh);
    
    ros::Rate looprate(20);

    while(ros::ok()){
        if(needReplan){
            planAndPub();
        }
        if(!hasPlanned){
            planAndPub();
        }
        looprate.sleep();
        ros::spinOnce();
    }
    return 0;
}

void updateSafeCircle(double time, servoState curServoState1, servoState curServoState2){
    curSafeCircle1->updateSafeZone(time, curServoState1);
    curSafeCircle2->updateSafeZone(time, curServoState2);
    curSafeCircle1->visualize_safeCircle();
    curSafeCircle2->visualize_safeCircle();
}

void planAndPub(){
    pthread_t thread1, thread2;
    if(!hasTraj) {return;}
    //mdp初始化
    needReplan=false;
    MDP mdpSolver1;
    
    mdpSolver1.setRobotTraj(curTraj);
    
    // mdpSolver1.setMoveitRobot(myrob);
    mdpSolver1.setWholeTime(curTrajTime);
    //注意是clone
    mdpSolver1.setSafeCircle(curSafeCircle1->clone());
    mdpSolver1.clearObs();
    for(auto obs:curObsList){
        mdpSolver1.addObs(obs);
        // std::cout<<obs.getCenter()<<std::endl;
    }
    
    std::vector<servoState> curServoTraj1=mdpSolver1.solveMDP();

    MDP mdpSolver2;
    
    mdpSolver2.setRobotTraj(curTraj);
    
    // mdpSolver2.setMoveitRobot(myrob);
    mdpSolver2.setWholeTime(curTrajTime);
    //注意是clone
    mdpSolver2.setSafeCircle(curSafeCircle2->clone());
    mdpSolver2.clearObs();
    for(auto obs:curObsList){
        mdpSolver2.addObs(obs);
    }
    
    std::vector<servoState> curServoTraj2=mdpSolver2.solveMDP();

    ros::Rate checkRate(40);

    for(int i=0;i<5;i++){

        servoState servoAngle1 = curServoTraj1[i];
        servoState servoAngle2 = curServoTraj2[i];
        ThreadParameters params1;
        params1.servoAngle = servoAngle1;
        params1.delta_angle = curVision1->getVisionState().disCost(servoAngle1);

        ThreadParameters params2;
        params2.servoAngle = servoAngle2;
        params2.delta_angle = curVision2->getVisionState().disCost(servoAngle2);
        // 创建线程1
        if (pthread_create(&thread1, NULL, thread_function1, &params1) != 0) {
            perror("Thread 1 creation failed");
            return ;
        }

        // 创建线程2
        if (pthread_create(&thread2, NULL, thread_function2, &params2) != 0) {
            perror("Thread 2 creation failed");
            return ;
        }
        pthread_join(thread1, NULL);
        pthread_join(thread2, NULL);

        //更新safeCircle
        updateSafeCircle(curTrajTime/5.0,servoAngle1, servoAngle2);
        
        checkRate.sleep();
        if(needReplan)break;

        // curTraj.clear();

    }
    hasPlanned=false;

}

void robotTrajCallback(const std_msgs::Float64MultiArray msg){
    hasTraj=true;
    curTraj.clear();
    int trajNum=msg.data.size()/3;
    // std::cout<<"trajNum is "<<trajNum<<std::endl;
    if(trajNum!=5){
        hasTraj=false;
        return;
    }
    // if(trajNum==0){
    //     Eigen::Matrix4d myc = myrob->getEnd(); 
    //     curTraj.push_back(myc.block<3,1>(0,3));
    // }
    else{
        // std::cout<<"trajNum is "<<trajNum<<std::endl;
        for(int i=0;i<trajNum;i++){
            Eigen::Vector3d pValue;
            for(int j=0;j<3;j++){
                pValue(j)=msg.data.at(i*3+j);
            }
            curTraj.push_back(pValue);
        }
    }
}
// void rbStateCallback(const std_msgs::Float64MultiArray msg){
//     for(int i=0;i<6;i++){
//         curConfig.setAngle(i, msg.data.at(i));
//     }
// }
void obsStateCallback(const std_msgs::Float64MultiArray msg){
    curObsList.clear();
    int trajNum=msg.data.size()/14;
    // std::cout<<"have obs"<<std::endl;
    for(int i=0; i<trajNum; i++){
        
        envelopeShape::Cylinder* cyl;
        Eigen::Vector3d center;
        center << msg.data[0+14*i], msg.data[1+14*i], msg.data[2+14*i];
        Eigen::Vector3d zAxis;
        zAxis << msg.data[3+14*i], msg.data[4+14*i], msg.data[5+14*i];
        // 将旋转向量转化为AngleAxisd对象
        Eigen::AngleAxisd rotationAngleAxis(zAxis.norm(), zAxis.normalized());
        // 将AngleAxisd对象转化为旋转矩阵
        Eigen::Matrix3d rotationMatrix = rotationAngleAxis.toRotationMatrix();

        std::vector<double> paramm;
        paramm.push_back(msg.data[6+14*i]);
        paramm.push_back(msg.data[7+14*i]);
        cyl=new envelopeShape::Cylinder (paramm[0], paramm[1]);
        cyl->setPose(center, rotationMatrix);
        Eigen::Vector3d velo;
        velo << msg.data[8+14*i], msg.data[9+14*i], msg.data[10+14*i];
        Eigen::Vector3d Anglevelo;
        Anglevelo << msg.data[11+14*i], msg.data[12+14*i], msg.data[13+14*i];
        envelopeShape::movingObs cy(cyl,velo,Anglevelo,1,false);

        curObsList.push_back(cy);
    }
}
void *thread_function1(void *arg){
    ThreadParameters *params = (ThreadParameters *)arg;
    servoState servoAngle1 = params->servoAngle;
    double delta_angle1 = params->delta_angle;
    curVision1->goToState(servoAngle1, delta_angle1*speed_coff);
    return NULL;
}
void *thread_function2(void *arg){
    ThreadParameters *params = (ThreadParameters *)arg;
    servoState servoAngle2 = params->servoAngle;
    double delta_angle2 = params->delta_angle;
    curVision2->goToState(servoAngle2, delta_angle2*speed_coff);
    return NULL;
}
#include "active_vision_control/mdp.h"

// 计算两个 Eigen::Vector3d 之间的欧氏距离
double euclideanDistance(const Eigen::Vector3d& p1, const Eigen::Vector3d& p2) {
    return (p1 - p2).norm();
}

// 实现 Mean-Shift 聚类
std::vector<Eigen::Vector3d> meanShiftClustering(const std::vector<Eigen::Vector3d>& points, double bandwidth) {
    std::vector<Eigen::Vector3d> clusterCenters;
    
    for (const Eigen::Vector3d& point : points) {
        Eigen::Vector3d center = point;
        double totalWeight = 0.0;

        while (true) {
            Eigen::Vector3d shift = Eigen::Vector3d::Zero();
            double totalWeightNew = 0.0;

            for (const Eigen::Vector3d& neighbor : points) {
                double distance = euclideanDistance(center, neighbor);
                if (distance < bandwidth) {
                    double weight = 1.0 - (distance / bandwidth);
                    shift += weight * neighbor;
                    totalWeightNew += weight;
                }
            }

            if (totalWeightNew > 0) {
                shift /= totalWeightNew;
                center = shift;
            } else {
                break;
            }

            totalWeight = totalWeightNew;
        }

        bool isNewCluster = true;
        for (const Eigen::Vector3d& existingCenter : clusterCenters) {
            if (euclideanDistance(center, existingCenter) < bandwidth) {
                isNewCluster = false;
                break;
            }
        }

        if (isNewCluster) {
            clusterCenters.push_back(center);
        }
    }

    return clusterCenters;
}


std::vector<servoState> MDP::solveMDP(){
    std::vector<envelopeShape::movingObs> fakeObs;
    std::vector<std::vector<servoState>> wholeDecision=genMarkovState();
    // return wholeDecision[0];
    // std::vector<std::vector<envelopeShape::movingObs*>> certainObs;
    // for(int i=1;i<=wholeTime/double(timeDivide);i++){
    //     double curtime=double(i)*double(wholeTime)/double(timeDivide);
    //     std::vector<envelopeShape::movingObs*>curObslist;
    //     for(auto obs:obsGroup){
    //         envelopeShape::movingObs* curt  = obs->clone();
    //         curt->stateEvolution(curtime);
    //         curObslist.push_back(curt);
    //     }
    //     certainObs.push_back(curObslist);
    // }

    
    // for(auto ll:wholeDecision){
    //     for(auto itt:ll){
    //         std::cout<<itt.getAngle1()<<' '<<itt.getAngle2()<<std::endl;
    //     }
    //     std::cout<<std::endl;
    // }

    int ITtime=1;
    std::vector<int> eachNum;
    std::vector<int> maxNum;
    std::vector<int> bestNum;
    std::vector<servoState> bestDecisionPath;
    int bestItem = 0;
    for(auto itstate:wholeDecision){
        ITtime*=itstate.size()+1;
        eachNum.push_back(0);
        bestNum.push_back(0);
        //without fake Obs
        maxNum.push_back(itstate.size()+1);
    }
    // std::cout<<"ITtime is "<<ITtime<<std::endl;
    double bestProb=-10000;
    for(int i=0;i<ITtime;i++){
        
        bool shouldJump=false;
        int lastNum=1;
        int llastNum=1;
        for(int n=0;n<eachNum.size(); n++){
            bestNum[n]=bestItem/lastNum%maxNum[n];
            eachNum[n]=i/lastNum%maxNum[n];
            lastNum *= maxNum[n];
        }
        // std::cout<<"the path is "<<i<<"th path"<<std::endl;
        std::vector<envelopeShape::movingObs> curObsSim;
        servoState lastState=safeSpace->getVision()->getVisionState();
        for(auto obs:obsGroup){
            curObsSim.push_back(obs.clone());
        }
        //std::cout<<"curcollisionProb"<<std::endl;
        
        std::vector<servoState> curDecisionPath;
        double curProb=0;
        servoState lstate=safeSpace->getVision()->getVisionState();
        std::vector<int> isFakeObs;
        // bool jumpflag=false;
        int j=0;
        // //生成现在的路径
        //___________________________________________________________________________________
        //std::cout<<"wholeDecision is "<<wholeDecision.size()<<std::endl;

        for(auto itState:wholeDecision){
            bool isfool=false;
            // if(fakeObs.size()!=0){
            //     std::cout<<"here"<<std::endl;
            //     std::cout<<"fakeobs has "<<fakeObs[0].getCenter()<<std::endl;
            // }
            //生成fakeObs
            if(eachNum[j]==maxNum[j]-1){
                // safespaceTimer;
                safeCircle* curSimCircle= safeSpace->clone();
                for(int k=0;k<=j;k++){
                    if(k!=0)lstate=itState[eachNum[k]-1];
                    curSimCircle->updateSafeZone(wholeTime/double(timeDivide),lstate);
                }
                curSimCircle->updateSafeZone(wholeTime*(1.0-double(j+1)/double(timeDivide)),lstate);
                //s//td::cout<<"update "<<safespaceTimer.getDeltaTime()<<std::endl;

                //safespaceTimer.clear();
                
                for(auto pp:Trajp){
                    bool isDang=curSimCircle->isInDanger(pp);
                    if(isDang){
                        // std::cout<<"dangerous"<<std::endl;
                        envelopeShape::movingObs curfakeObs=curSimCircle->simObsGene(pp, wholeTime);
                        // std::cout<<"curfakeObs.getCenter"<<curfakeObs.getCenter()<<std::endl;
                        if (curfakeObs.getCenter()(0)<0){
                            // std::cout<<"fakeObs is "<<curfakeObs.getCenter()<<std::endl;
                            // std::cout<<"CurDecision is "<<safeSpace->getVision()->visionInverseKine(curfakeObs.getCenter()).getAngle1()<<", "<<"CurDecision is "<<safeSpace->getVision()->visionInverseKine(curfakeObs.getCenter()).getAngle2()<<std::endl;
                            // envelopeShape::movingObs* curfakeObsPtr = &curfakeObs;
                            fakeObs.push_back(curfakeObs);
                            // std::cout<<"fakeobs1 has "<<fakeObs[0].getCenter()<<std::endl;
                            // itState.push_back(safeSpace->getVision()->visionInverseKine(curfakeObs.getCenter()));
                            curDecisionPath.push_back(safeSpace->getVision()->visionInverseKine(curfakeObs.getCenter()));
                            isFakeObs.push_back(1);
                            isfool=true;
                            break;
                        }  
                        // else{
                        //     isFakeObs.push_back(0);
                        //     int itemadd=0;
                        //     while(eachNum[itemadd]>=maxNum[itemadd]-1){
                        //         eachNum[itemadd]=0;
                        //         itemadd++;
                        //     }
                        //     eachNum[itemadd]++;
                        //     jumpflag=true;
                        //     break;
                        // }
                    }
                    // else{
                    //     isFakeObs.push_back(0);
                    //     int itemadd=0;
                    //     while(eachNum[itemadd]>=maxNum[itemadd]-1){
                    //         eachNum[itemadd]=0;
                    //         itemadd++;
                    //     }
                    //     eachNum[itemadd]++;
                    //     jumpflag=true;
                    //     break;
                    // }   
                }
                if(!isfool){
                    isFakeObs.push_back(0);
                    shouldJump=true;
                    break;
                }
                    
                //std::cout<<"danger "<<safespaceTimer.getDeltaTime()<<std::endl;
                // if(jumpflag)break;
                delete curSimCircle;
            }
            else{
                //std::cout<<"itState="<<itState.size()<<std::endl;
                //std::cout<<"eachNum="<<eachNum[j]<<std::endl;
                isFakeObs.push_back(0);
                curDecisionPath.push_back(itState[eachNum[j]]);
                lstate=itState[eachNum[j]];
            }
            
            j++;
        }
        if(shouldJump){
            continue;
        }
        // std::cout<<"Finish gen Path"<<std::endl;
        // ___________________________________________________________________________________
        // int itemadd=0;
        // while(eachNum[itemadd]>=maxNum[itemadd]-1){
        //     if(itemadd>=eachNum.size()){itemadd--;break;}
        //     eachNum[itemadd]=0;
        //     itemadd++;
        // }
        
        // eachNum[itemadd]++;
        //update each num
        
        // for(auto tit:curDecisionPath){
        //     std::cout<<tit.getAngle1()<<' '<<tit.getAngle2()<<std::endl;
        // }
        //___________________________________________________________________________________
        //if(jumpflag)continue;
        //计算当前路径下的碰撞概率
        // for(auto tit:isFakeObs){
        //     std::cout<<tit<<' ';
        // }
        // std::cout<<std::endl;
        int FakeObsflag=0;
        int step=1;
        j=0;
        // std::cout<<"fakeobs has "<<fakeObs[0].getCenter()<<std::endl;
        lstate=safeSpace->getVision()->getVisionState();
        for(auto itState:curDecisionPath){
            
            double curtime=double(step)*double(wholeTime)/double(timeDivide);
            
            for(auto obs:curObsSim){
                obs.stateEvolution(curtime);
            }
            if(isFakeObs.size()!=0){
                if(isFakeObs[j]==1){
                    curObsSim.push_back(fakeObs[FakeObsflag]);
                    
                    FakeObsflag++;
                }
            }
        
            // std::cout<<"curDecisionPath has "<<curDecisionPath.size()<<std::endl;
            // std::cout<<"curObsSim has "<<curObsSim.size()<<std::endl;
            // if(curObsSim.size()!=0){
            //     std::cout<<"fakeobs has "<<fakeObs[0].getCenter()<<std::endl;
            //     std::cout<<"curObsSim has "<<curObsSim[0]->getCenter()<<std::endl;
            // }
            //safeTimer evolution;
            // std::cout<<"beforecurcollisionProb "<<std::endl;
            double curcollisionProb=getCollisionProb(itState, step, Trajp[step-1], curObsSim);
            // std::cout<<"aftercurcollisionProb "<<curcollisionProb<<std::endl;
            //std::cout<<curcollisionProb<<std::endl;
            if(curcollisionProb == 1)curProb = -1000;
            else curProb+=pow(gamma,j)*log(1-curcollisionProb)+pow(alpha,j)*(-lastState.disCost(itState)/5.0);
            // else curProb = 0;
        //     //update uncertainty
            //std::cout<<"cur prob time is "<<evolution.getDeltaTime()<<std::endl;
            //evolution.clear();
            // std::cout<<"curObsSim size is "<<curObsSim.size()<<std::endl;
            // for(auto isl : curObsSim){
            //     std::cout<<isl.getCenter()<<std::endl;
            // }
            if(isFakeObs.size()!=0){
                if(isFakeObs[j]==1){
                    if (!curObsSim.empty()) {
                        curObsSim.pop_back(); 
                    }
                }
            }
            // std::cout<<"acurObsSim size is "<<curObsSim.size()<<std::endl;
            // for(auto isl : curObsSim){
            //     std::cout<<isl.getCenter()<<std::endl;
            // }
            // std::cout<<"beforeUncertainty"<<std::endl;
            for(auto obs:curObsSim){
                bool isvalid=isValid(itState, obs);
                obs.updateUncertainty(curtime,isvalid);
            }
            // std::cout<<"afterUncertainty"<<std::endl;
            //std::cout<<"cur valid time is "<<evolution.getDeltaTime()<<std::endl;
            
        
            j++;
            step++;
            lastState=itState;
        }
        
        //___________________________________________________________________________________
        // //std::cout<<"I m he"<<std::endl;
        if(curProb>bestProb){
            bestProb=curProb;
            bestDecisionPath=curDecisionPath;
            bestItem = i;
        }
        
        // std::cout<<"cur best prob is "<<bestProb<<std::endl;
        // std::cout<<"cur best Path is "<<std::endl;
        // for(int n=0;n<eachNum.size(); n++){
        //     std::cout<<eachNum[n]<<' ';
        // }
        // std::cout<<std::endl;
        // for(auto servos:bestDecisionPath){
        //     std::cout<<"("<<servos.getAngle1()<<" ,"<<servos.getAngle2()<<")"<<';';
        // }
        // std::cout<<std::endl;
        // itemadd=0;
        // FakeObsflag=0;
        // while(eachNum[itemadd]>=maxNum[itemadd]-1){
        //     eachNum[itemadd]=0;
        //     itemadd++;
        // }
        // eachNum[itemadd]++;
        
        
        // for(auto itt:curObsSim){
        //     delete itt;
        // }    
        
        // for(auto itt:fakeObs){
        //     delete itt;
        // }
        fakeObs.clear();
        // for(auto itt:curObsSim){
        //     delete itt;
        // }
        curObsSim.clear();
        
        //std::cout<<"BARK"<<std::endl;
        // if(i==ITtime-2){
        //     bestDecisionPath.clear();
        //     servoState testss(0,0);
        //     for(int i=0;i<5;i++){
        //         bestDecisionPath.push_back(testss);
        //     }
        //     std::cout<<"Wa"<<std::endl;
        //     return bestDecisionPath;
        // }
        
    }
    // std::cout<<"Best is "<<":";
    // for(auto ittt:bestNum){
    //     std::cout<<ittt<<' ';
    // }
    // std::cout<<std::endl;
    // 打开文件流，将输出重定向到文件
    std::ofstream outputFile("/home/xian/jaka_ws/src/active_vision_control/result/output.txt", std::ios_base::app);

    // 将 std::cout 的输出写入文件
    if (outputFile.is_open()) {
        outputFile << "Best prob is " << bestProb << std::endl;
        outputFile << "Best traj is " << std::endl;
        for (auto servos : bestDecisionPath) {
            outputFile << "(" << servos.getAngle1() << " ," << servos.getAngle2() << ")" << ';';
        }
        outputFile << std::endl;

        // 关闭文件流
        outputFile.close();
    } else {
        std::cerr << "Unable to open the output file." << std::endl;
    }


    return bestDecisionPath;
}

std::vector<std::vector<servoState>> MDP::genMarkovState(){
    // std::cout<<"MarkovState"<<std::endl;
    std::vector<std::vector<servoState>> MarkovState; 
    for(int i=0;i<timeDivide;i++){
        std::vector<servoState>initS;
        MarkovState.push_back(initS);
    }
    Trajp.clear();
    // std::cout<<"plannedTraj:"<<plannedTraj.size()<<std::endl;
    // Trajj.clear();
    //trajectory
    if(plannedTraj.size()==1){
        // std::cout<<"BARK1" <<std::endl;
        // std::vector<Eigen::Vector3d> trajDecision;
        // for(int i=0;i<timeDivide;i++){
        //     trajDecision.push_back(plannedTraj[0]);
        //     // Trajj.push_back(plannedTraj.getStart());
        // }
        for(int i=0;i<timeDivide;i++){  
            Trajp.push_back(plannedTraj[0]);
            MarkovState[i].push_back(safeSpace->getVision()->visionInverseKine(plannedTraj[0]));
            // std::cout<<endp<<std::endl;
            // std::cout<<safeSpace->getVision()->visionInverseKine(endp).getAngle1()<<' '<<safeSpace->getVision()->visionInverseKine(endp).getAngle2()<<std::endl;
        }
    }
    else if(plannedTraj.size()!=0){
        // std::cout<<plannedTraj.sizeOfTraj() <<std::endl;
        // std::cout<<"Init path is "<<std::endl;
        // for(auto pj:plannedTraj.getTraj()){
        //     for(int l=0;l<6;l++){
        //         std::cout<<pj.getAngle(l)<<',';
        //     }
        //     std::cout<<std::endl;
        // }
        // std::cout<<"after path is "<<std::endl;
        // std::vector<robotConfig> dpath=plannedTraj.CuttingTo(0.2);
        // for(auto pj:dpath){
        //     for(int l=0;l<6;l++){
        //         std::cout<<pj.getAngle(l)<<',';
        //     }
        //     std::cout<<std::endl;
        // }
        // std::vector<robotConfig> trajDecision;
        // for(int i=0;i<timeDivide;i++){
        //     int ind=double(i+1)/double(timeDivide)*double(dpath.size());
        //     if(ind < 1)ind =1;
        //     trajDecision.push_back(dpath[ind-1]);
        //     // Trajj.push_back(dpath[ind-1]);
        // }
        
        for(int i=0;i<timeDivide;i++){  
            Trajp.push_back(plannedTraj[i]);
            MarkovState[i].push_back(safeSpace->getVision()->visionInverseKine(plannedTraj[i]));
            // std::cout<<endp<<std::endl;
            // std::cout<<safeSpace->getVision()->visionInverseKine(endp).getAngle1()<<' '<<safeSpace->getVision()->visionInverseKine(endp).getAngle2()<<std::endl;
        }
    }
    // std::cout<<"BARK" <<std::endl;
    std::vector<envelopeShape::movingObs> simObs;
    // std::cout<<"previous obs "<<std::endl;
    for(auto obs:obsGroup){
        simObs.push_back(obs.clone());
    }
    // std::cout<<"now obs is "<<std::endl;
    if(simObs.size()!=0){
        for(int i=0;i<timeDivide;i++){
            Eigen::Vector3d obsCenter(0,0,0);
            for(auto obs:simObs){
                double curtime=double(i+1)*double(wholeTime)/double(timeDivide);
                
                obs.stateEvolution(curtime);
                
                obsCenter+=obs.getCenter();
            }
            obsCenter=obsCenter/simObs.size();
            // std::cout<<"now obs is "<<obsCenter<<std::endl;
            MarkovState[i].push_back(safeSpace->getVision()->visionInverseKine(obsCenter));
        }
    }
    
    // for(auto obs:simObs){
    //     delete obs;
    // }
    // std::cout<<"out of gen"<<std::endl;
    return MarkovState;
}
double MDP::getCollisionProb(servoState ss,int stp,Eigen::Vector3d trajC,std::vector<envelopeShape::movingObs> curObs){
    double pnotc=1;
    // collideJudge curj;
    // curj.setJAKA(myrob);
    double curtime=double(stp)*double(wholeTime)/double(timeDivide);
    Eigen::Vector3d endp = trajC;
    for(auto it:curObs){
        // fcl::CollisionObjectd* curObsCollide=it->getCollision();
        if((endp-it.getCenter()).norm()>it.getRadius()){
            if(isValid(ss, it)){
                pnotc*=it.getUncertainty();
                // std::cout<<"it->getUncertainty() "<<it->getUncertainty()<<std::endl;
            }
            else {
                pnotc=0;
                break;
            }
            
        }
        else{
            double pNorm=1.0/2.0*it.maxAcceleration*curtime*curtime;
            envelopeShape::movingObs fakeobss=it.clone();
            fakeobss.addRadius(pNorm);
            // fcl::CollisionObjectd* curObsCollide=fakeobss->getCollision();
            if((endp-it.getCenter()).norm()>fakeobss.getRadius()) {
                
                pnotc*=1;
            }
            else{
                pnotc*=it.getUncertainty();
                // std::cout<<"it->getUncertainty() "<<it->getUncertainty()<<std::endl;
            }
            //delete fakeobss;
        }
    }
    return (1.0-pnotc);
}
// double MDP::getCollisionProb(servoState ss,int stp,std::vector<envelopeShape::movingObs> curObs){
//     double pnotc=1;
//     collideJudge curj;
//     curj.setJAKA(myrob);
//     double curtime=double(stp)*double(wholeTime)/double(timeDivide);
//     for(auto it:curObs){
//         fcl::CollisionObjectd* curObsCollide=it.getCollision();
//         if(!curj.collideCheck(*curObsCollide)) {
//             if(isValid(ss, it)){
//                 pnotc*=(1.0-it.getUncertainty());
                
//             }
//             else pnotc=0;
//             break;
//         }
//         else{
//             double pNorm=1.0/2.0*it.maxAcceleration*curtime*curtime;
//             envelopeShape::movingObs fakeobss=it.clone();
//             fakeobss.addRadius(pNorm);
//             fcl::CollisionObjectd* curObsCollide=fakeobss.getCollision();
//             if(!curj.collideCheck(*curObsCollide)) {
//                 pnotc*=1;
//             }
//             else{
//                 pnotc*=(1.0-it.getUncertainty());
//                 // std::cout<<"it->getUncertainty() "<<it->getUncertainty()<<std::endl;
//             }
//         }
//     }
//     return (1.0-pnotc);
// }


bool MDP::isValid(servoState nextView, envelopeShape::movingObs watchObs){
    //without think of block
    // std::cout<<"In isValid"<<std::endl;
    Eigen::Vector4d blockpp(watchObs.getCenter()(0),watchObs.getCenter()(1),watchObs.getCenter()(2),1);
    // std::cout<<"aftergetCenter"<<std::endl;
    //转换坐标系
    Eigen::Matrix4d endvision=safeSpace->getVision()->visionForwardKine(nextView);
    Eigen::Vector3d endInWorld=endvision.block<3,1>(0,3);
    Eigen::Vector4d blockInEnd=endvision.inverse()*blockpp;
    // std::cout<<nextView.getAngle1()<<' '<<nextView.getAngle2()<<std::endl;
    //std::cout<<blockInEnd<<std::endl;
    double dis1=sqrt(blockInEnd(0)*blockInEnd(0)+blockInEnd(1)*blockInEnd(1));
    double dis2=blockInEnd(2);
    if(blockInEnd(2)<=0)return true;
    else{
        double ang=atan(dis1/dis2);
        if(ang>safeSpace->getVision()->getVisionAngle()/2.0)return true;
        else return true;
    }
}
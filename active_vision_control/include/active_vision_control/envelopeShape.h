#ifndef ENVELOPESHAPE_SHAPE_H
#define ENVELOPESHAPE_SHAPE_H

#include <Eigen/Dense>
#include <cmath>
#define FCL_EXPORT
#include <fcl/fcl.h>
#include <fcl/narrowphase/collision.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

namespace envelopeShape {

    class Shape {
    public:
        Shape() = default;
        virtual ~Shape() = default;

        // Pure virtual constructor
        virtual Shape* clone() const = 0;

        // Virtual functions to get and set center of mass position and rotation matrix
        virtual void getPose(Eigen::Vector3d& com, Eigen::Matrix3d& rotation) const = 0;
        virtual void setPose(const Eigen::Vector3d& com, const Eigen::Matrix3d& rotation) = 0;

        // Virtual function to get shape parameters
        virtual void getShapeParameters(std::vector<double>& params) const = 0;
        // Virtual function to set shape parameters
        virtual void setShapeParameters(const std::vector<double>& params) = 0;

        // Pure virtual method to convert this shape to a collision object in the FCL library
        virtual fcl::CollisionObjectd* toCollisionObject() const = 0;
        
        virtual visualization_msgs::Marker genCylinderRviz(int id) const = 0;

        virtual visualization_msgs::Marker genCylinderRviz(int id, double certainty) const = 0;
    };

    class Cylinder : public Shape {
    public:
        Cylinder(double radius, double height) : radius(radius), height(height) {}

        // Clone method to create a copy of the Cylinder object
        Shape* clone() const override {
            return new Cylinder(*this);
        }

        // Function to get center of mass position and rotation matrix (overrides Shape's method)
        void getPose(Eigen::Vector3d& com, Eigen::Matrix3d& rotation) const override {
            com = centerOfMass;
            rotation = rotationMatrix;
        }

        // Function to set center of mass position and rotation matrix
        void setPose(const Eigen::Vector3d& com, const Eigen::Matrix3d& rotation) override {
            centerOfMass = com;
            rotationMatrix = rotation;
        }

        // Method to get shape parameters (overrides Shape's method)
        void getShapeParameters(std::vector<double>& params) const override {
            params = {radius, height};
        }

        // Method to set shape parameters
        void setShapeParameters(const std::vector<double>& params) override {
            if (params.size() != 2) {
                throw std::invalid_argument("Cylinder requires two parameters: radius and height.");
            }
            radius = params[0];
            height = params[1];
        }

        // Method to convert this shape to a collision object in the FCL library (overrides Shape's method)
        fcl::CollisionObjectd* toCollisionObject() const override {
            fcl::CollisionGeometryd* geometry = new fcl::Cylinderd(radius, height);
            fcl::CollisionObjectd* collisionObject = new fcl::CollisionObjectd(std::shared_ptr<fcl::CollisionGeometryd>(geometry));

            Eigen::Isometry3d transform = Eigen::Isometry3d::Identity();
            transform.linear() = rotationMatrix;
            transform.translation() = centerOfMass;
            collisionObject->setTransform(transform);

            return collisionObject;
        }

        visualization_msgs::Marker genCylinderRviz(int id) const override {
            visualization_msgs::Marker cylinder_marker;
            cylinder_marker.type = visualization_msgs::Marker::CYLINDER;
            cylinder_marker.header.frame_id = "world"; // 你的场景坐标系
            cylinder_marker.header.stamp = ros::Time::now();
            cylinder_marker.id = id;
            cylinder_marker.pose.position.x = centerOfMass.x();
            cylinder_marker.pose.position.y = centerOfMass.y();
            cylinder_marker.pose.position.z = centerOfMass.z();
            Eigen::Quaterniond orientation_quat(rotationMatrix);
            cylinder_marker.pose.orientation.x = orientation_quat.x();
            cylinder_marker.pose.orientation.y = orientation_quat.y();
            cylinder_marker.pose.orientation.z = orientation_quat.z();
            cylinder_marker.pose.orientation.w = orientation_quat.w();
            cylinder_marker.scale.x = 2.0 * radius; // 直径
            cylinder_marker.scale.y = 2.0 * radius; // 直径
            cylinder_marker.scale.z = height;
            cylinder_marker.color.r = 0.0; // 红色
            cylinder_marker.color.g = 1.0;
            cylinder_marker.color.b = 0.0;
            cylinder_marker.color.a = 1.0; // 不透明
            return cylinder_marker;
        }

        visualization_msgs::Marker genCylinderRviz(int id, double certainty) const override {
            if(certainty > 1.0)certainty = 1.0;
            else if (certainty < 0.0) certainty = 0.0;
            
            visualization_msgs::Marker cylinder_marker;
            cylinder_marker.type = visualization_msgs::Marker::CYLINDER;
            cylinder_marker.header.frame_id = "world"; // 你的场景坐标系
            cylinder_marker.header.stamp = ros::Time::now();
            cylinder_marker.id = id;
            cylinder_marker.pose.position.x = centerOfMass.x();
            cylinder_marker.pose.position.y = centerOfMass.y();
            cylinder_marker.pose.position.z = centerOfMass.z();
            Eigen::Quaterniond orientation_quat(rotationMatrix);
            cylinder_marker.pose.orientation.x = orientation_quat.x();
            cylinder_marker.pose.orientation.y = orientation_quat.y();
            cylinder_marker.pose.orientation.z = orientation_quat.z();
            cylinder_marker.pose.orientation.w = orientation_quat.w();
            cylinder_marker.scale.x = 2.0 * radius; // 直径
            cylinder_marker.scale.y = 2.0 * radius; // 直径
            cylinder_marker.scale.z = height;
            cylinder_marker.color.r = 1.0-certainty; // 红色
            cylinder_marker.color.g = certainty;
            cylinder_marker.color.b = 0.0;
            cylinder_marker.color.a = 1.0; // 不透明
            return cylinder_marker;
        }

    private:
        double radius;
        double height;
        Eigen::Vector3d centerOfMass;
        Eigen::Matrix3d rotationMatrix;
    };

    

    class angle {
    private:
        double data;

    public:
        // Constructor to initialize the angle with a given value
        angle(double value = 0.0) {
            update(value);
        }

        // Function to update the angle value while ensuring it stays within [0, 2pi)
        void update(double value) {
            data = std::fmod(value, 2 * M_PI);
            if (data < 0) {
                data += 2 * M_PI;
            }
        }

        // Function to get the angle value
        double get() const {
            return data;
        }

        // Operator overloading for '+' to add angles
        angle operator+(const angle& other) const {
            double sum = data + other.data;
            return angle(sum);
        }

        // Operator overloading for '-' to subtract angles
        angle operator-(const angle& other) const {
            double diff = data - other.data;
            if (diff < 0) {
                diff += 2 * M_PI;
            }
            return angle(diff);
        }
};

} // namespace envelopeShape

#endif // ENVELOPESHAPE_SHAPE_H

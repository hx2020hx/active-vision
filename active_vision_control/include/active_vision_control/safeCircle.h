#ifndef SAFECIRCLE_H
#define SAFECIRCLE_H

#include <iostream>
#include "envelopeShape.h"
#include "movingObs.h"
#include "activeVision.h"
#include "trajWithSpeed.h"
#include <vector>
#include <chrono>

class safeCircle{
private:
    std::vector<double> safeZone;
    std::vector<double> infectionDepth;
    //0--未进行操作，1--拓宽了，2--收缩，3--左边扩散，4--右边扩散，5--上面扩散，6--下面扩散
    std::vector<int> safeZoneState;
    ActiveVision* visionDevice;
    const int dividingtimes;
    const double maxVelocity;
    double innerClock;
    visualization_msgs::Marker createPolygonMarker(const std::vector<geometry_msgs::Point>& points, const std::string& frame_id, const int marker_id, double danger_ratio);
    ros::NodeHandle nh;
    ros::Publisher safeCirclePub;
    ros::Publisher safeDataPub;
public:
    safeCircle(const int dt);
    safeCircle(ActiveVision* visionDevice,const int dt);
    safeCircle(ActiveVision* visionDevice,const int dt, ros::NodeHandle& nh_);

    std::vector<double> getSafeZone(){
        return safeZone;
    }
    
    safeCircle* clone(){
        safeCircle* newsafe =new safeCircle (visionDevice, dividingtimes);
        newsafe->setSafeZone(safeZone);
        return newsafe;
    }
    void setVision(ActiveVision* visionDevice){
        this->visionDevice=visionDevice;
    }
    void setSafeZone(std::vector<double> sz){
        safeZone=sz;
    }
    void clearState(){
        for (size_t i = 0; i < safeZoneState.size(); ++i) {
            safeZoneState[i] = 0;
        }
    }
    ActiveVision* getVision(){return visionDevice;}

    void setTime(double ic){innerClock=ic;}
    void addTime(double ic){innerClock+=ic;}
    double getTime(){return innerClock;}
    
    int angleToId(servoState ss){
        int times1=double(ss.getAngle1())/(2*M_PI)*double(dividingtimes);
        int times2=double(ss.getAngle2())/(M_PI)*double(dividingtimes);
        int index=times1*dividingtimes+times2;
        return index;
    }
    servoState idToAngle(int index){
        int times1=double(index)/double(dividingtimes);
        int times2=index%dividingtimes;

        servoState ss(double(times1)*(2*M_PI)/double(dividingtimes), double(times2)*(M_PI)/double(dividingtimes));
        return ss;
    }
    double getAngleDepth(servoState ss){return safeZone[angleToId(ss)];}
    int getLeft(int index){
        int times1=double(index)/double(dividingtimes);
        int times2=index%dividingtimes;

        times2--;
        if(times2<0)times2+=dividingtimes;
        return times1*dividingtimes+times2;
    }
    int getRight(int index){
        int times1=double(index)/double(dividingtimes);
        int times2=index%dividingtimes;

        times2++;
        if(times2>=dividingtimes)times2-=dividingtimes;

        return times1*dividingtimes+times2;
    }
    int getUp(int index){
        int times1=double(index)/double(dividingtimes);
        int times2=index%dividingtimes;

        times1++;
        if(times1>=dividingtimes)times1-=dividingtimes;

        return times1*dividingtimes+times2;
    }
    int getDown(int index){
        int times1=double(index)/double(dividingtimes);
        int times2=index%dividingtimes;

        times1--;
        if(times1<0)times1+=dividingtimes;

        return times1*dividingtimes+times2;
    }
    bool isInCircle(int id, double angrad, int center){
        if(safeZoneState[id]!=0&&safeZoneState[id]!=2)return false;
        if(safeZone[id]<safeZone[center])return false;

        servoState cs=idToAngle(center);
        servoState anos=idToAngle(id);
        double angledif=cs.disCost(anos);
        return angledif<angrad;

    }
    //周向危险传播
    void infection(double time,int index);
    //传染函数迭代器
    void infectI(int id, double angrad, int center);
    //对外接口：根据停滞时间更新危险区
    void updateSafeZone(double time, servoState s1);
    //对外接口：判断是否处于危险之中
    bool isInDanger(Eigen::Vector3d dangerPoint);
    //对外接口：根据危险点生成移动障碍物
    envelopeShape::movingObs simObsGene(Eigen::Vector3d dangerPoint, double time);
    //对外接口：可视化当前的safeCircle
    void visualize_safeCircle();


};

class safeTimer{
private:
    // Declare the global start variable
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
public:
    safeTimer(){
        start = std::chrono::high_resolution_clock::now();
    }
    void clear(){
        start = std::chrono::high_resolution_clock::now();
    }
    double getDeltaTime(bool isReset=true) {
        // Get the end time
        auto end = std::chrono::high_resolution_clock::now();

        // Calculate the duration in seconds as a double
        std::chrono::duration<double> duration = end - start;

        // Get the duration value in seconds as a double
        double durationMs = duration.count();
        if(isReset)start = std::chrono::high_resolution_clock::now();

        return durationMs;
    }

};

#endif // SAFECIRCLE_H

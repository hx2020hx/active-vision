#include "movingObs.h"
#include <ros/ros.h>
#include <chrono>
#include <rviz_visual_tools/rviz_visual_tools.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>
#include <std_msgs/Float64MultiArray.h>
class SimulationPeople{
public:
    envelopeShape::movingObs* body;
    envelopeShape::movingObs* right_arm;
    envelopeShape::movingObs* right_hand;
    envelopeShape::movingObs* left_arm;
    envelopeShape::movingObs* left_hand;

    double handp;
    double rhand;

    double handRotationOmega;
    std::chrono::high_resolution_clock::time_point start_time_;
    ros::NodeHandle nhe;
    ros::Publisher peopleStatePub;
    ros::Publisher markerPub;
    SimulationPeople();
    SimulationPeople(ros::NodeHandle &nh);
    void* peopleSimulationStart(void*);

private:
    void initialize();
    Eigen::Matrix3d rotationAxis(Eigen::Vector3d, Eigen::Matrix3d);
    void armRotationmotion();
    void baseMoving();
    std_msgs::Float64MultiArray genMsgFromCylinder(envelopeShape::movingObs* );
    void vel_clear();
    void publishMsg();
    
};
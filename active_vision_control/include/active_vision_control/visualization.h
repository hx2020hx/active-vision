#ifndef VISUALIZATION_H
#define VISUALIZATION_H

#include <ros/ros.h>
#include <mdp.h>
#include "moveitRobot.h"
#include <std_msgs/Float64.h>
#include <geometry_msgs/PointStamped.h>

class Visualization {
public:
    Visualization(ros::NodeHandle &nh_,moveit::planning_interface::MoveGroupInterface &arm_,const std::string &PLANNING_GROUP_, int mode_);
    //主函数，直接调用
    void spin();

private:
    int mode;
    double curtime;
    std::vector<envelopeShape::movingObs> SCEs;
    Eigen::Matrix4d cam1pose, cam2pose;
    bool cam1ok, cam2ok;
    double avg_PoE;
    int times ; 
    std::vector<double> cp;

    ros::NodeHandle nh;
    ros::Subscriber obsSub, cam1Sub, cam2Sub;
    ros::Publisher SCEPub;
    ros::Publisher PoEPub;
    JAKArobot* testRobot;
    MoveitRobot* mRobot;
    ActiveVision* curVision1;
    ActiveVision* curVision2;  

    void obsCallback(const std_msgs::Float64MultiArray msg);
    void cam1Callback(const geometry_msgs::PoseStamped& msg);
    void cam2Callback(const geometry_msgs::PoseStamped& msg);
    //geo->matrix
    Eigen::Matrix4d poseStampedToMatrix4d(const geometry_msgs::PoseStamped& poseStamped) {
        Eigen::Matrix4d matrix = Eigen::Matrix4d::Identity();

        // 从 PoseStamped 中获取位置和姿态信息
        const geometry_msgs::Point& position = poseStamped.pose.position;
        const geometry_msgs::Quaternion& orientation = poseStamped.pose.orientation;

        // 设置位置信息
        matrix(0, 3) = position.x;
        matrix(1, 3) = position.y;
        matrix(2, 3) = position.z;

        // 设置姿态信息
        Eigen::Quaterniond quat(orientation.w, orientation.x, orientation.y, orientation.z);
        matrix.block<3, 3>(0, 0) = quat.toRotationMatrix();

        return matrix;
    }
    //碰撞概率计算
    bool isVaildPoint(Eigen::Vector3d cyp, Eigen::Matrix4d endvision, std::vector<robotCylinder>&nextCover);
    std::vector<Eigen::Vector3d> genPointsInCylinder(envelopeShape::movingObs &myCylinder, int num_points);
    bool collideJ(envelopeShape::movingObs obs, std::vector<robotCylinder>&nextCover);
    bool isValid(envelopeShape::movingObs obs, std::vector<robotCylinder>&nextCover);
    double collideProb(envelopeShape::movingObs obs);
    //可视化
    void visualize_SCE();
    //数据导出
    void data_collide();

};

#endif // VISUALIZATION_H

#include "movingObs.h"
#include "moveitRobot.h"
#include "tf/tf.h"
#include "tf/transform_listener.h"
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>
#include <geometry_msgs/TransformStamped.h>
#include <std_msgs/Float64MultiArray.h>
#include <tf_conversions/tf_eigen.h>
#include <chrono>
#include <random>
class detector{
public:
    envelopeShape::movingObs* body;
    envelopeShape::movingObs* right_arm;
    envelopeShape::movingObs* right_hand;
    envelopeShape::movingObs* left_arm;
    envelopeShape::movingObs* left_hand;
    Eigen::Matrix4d left_camera;
    Eigen::Matrix4d right_camera;
    MoveitRobot* testRobot;
    ros::NodeHandle nh;
    std::unique_ptr<ros::AsyncSpinner> spinner;
    ros::Publisher simStatePub;
    ros::Subscriber peopleListener;
    detector(ros::NodeHandle &nh,moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP);
    void peopleStateCallback(const std_msgs::Float64MultiArray);
    void* detectorThread(void*);
    void updateVision();
    Eigen::Matrix4d getCamera(int id){
        if(id==1)return left_camera;
        else return right_camera;
    }
    bool isVisiblePoint(Eigen::Vector3d cyp, Eigen::Matrix4d endvision){
        std::vector<robotCylinder> nextCover = testRobot->getCover();
        // for(auto cylin:nextCover){
        //     std::cout<<"center is "<<cylin.getCenter_trans()<<std::endl;
        // }
        return isVisiblePoint(cyp, endvision, nextCover);
    }

private:
    double isVisible(envelopeShape::movingObs* , std::vector<robotCylinder>&);//返回可视度
    bool isVisiblePoint(Eigen::Vector3d cyp, Eigen::Matrix4d endvision, std::vector<robotCylinder>&nextCover);//返回可视度
    void publishCylinder(std::vector<bool> );
    std::vector<Eigen::Vector3d> genPointsInCylinder(envelopeShape::movingObs* , int num_points=100);
    Eigen::Matrix4d getTransformMatrix(const std::string& target_frame, const std::string& source_frame);
    std_msgs::Float64MultiArray genMsgFromCylinder(envelopeShape::movingObs* );
    
};
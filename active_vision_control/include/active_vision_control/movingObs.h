#ifndef MOVINGOBS_H
#define MOVINGOBS_H

#include "envelopeShape.h"
#include "Gaussian3D.h"
#include <Eigen/Dense>

namespace envelopeShape {

    class movingObs {
    public:
        movingObs(bool isf):isFake(isf),positionRadius(0.02),orientationRadius(0.02){}
        movingObs(Shape* shape, const Eigen::Vector3d& velocity, const Eigen::Vector3d& angularVelocity, double uncertainty,bool isf)
            : shape(shape), velocity(velocity), angularVelocity(angularVelocity), uncertainty(uncertainty),isFake(isf),
            positionCovariance(Eigen::Matrix3d::Identity()*0.2),orientationCovariance(Eigen::Matrix3d::Identity()*0.2),velocityCovariance(Eigen::Matrix3d::Identity()*0.2),angularVelocityCovariance(Eigen::Matrix3d::Identity()*0.2),positionRadius(0.02),orientationRadius(0.02){}
        movingObs(Shape* shape, const Eigen::Vector3d& velocity, const Eigen::Vector3d& angularVelocity, double uncertainty,
                  const Eigen::Matrix3d& velocityCovariance, const Eigen::Matrix3d& angularVelocityCovariance, const Eigen::Matrix3d& positionCovariance, const Eigen::Matrix3d& orientationCovariance,bool isf)
            : shape(shape), velocity(velocity), angularVelocity(angularVelocity), uncertainty(uncertainty),
              positionCovariance(positionCovariance),orientationCovariance(orientationCovariance),velocityCovariance(velocityCovariance), angularVelocityCovariance(angularVelocityCovariance),isFake(isf),positionRadius(0.02) ,orientationRadius(0.02){}
        ~movingObs() {
            delete shape; // 释放 Shape 对象的内存
        }
        movingObs(const movingObs& other) 
        : shape(other.shape->clone()), 
          positionRadius(other.positionRadius), 
          orientationRadius(other.orientationRadius),
          velocity(other.velocity),
          angularVelocity(other.angularVelocity),
          uncertainty(other.uncertainty),
          isFake(other.isFake),
          positionCovariance(other.positionCovariance),
          orientationCovariance(other.orientationCovariance),
          velocityCovariance(other.velocityCovariance),
          angularVelocityCovariance(other.angularVelocityCovariance) {
        }

        // 赋值运算符
        movingObs& operator=(const movingObs& other) {
            if (this == &other) {
                return *this; // 自我赋值检查
            }

            // 删除当前的 shape 对象
            delete shape;

            // 深拷贝其他成员
            shape = other.shape->clone();
            positionRadius = other.positionRadius;
            orientationRadius = other.orientationRadius;
            velocity = other.velocity;
            angularVelocity = other.angularVelocity;
            uncertainty = other.uncertainty;
            isFake = other.isFake;
            positionCovariance = other.positionCovariance;
            orientationCovariance = other.orientationCovariance;
            velocityCovariance = other.velocityCovariance;
            angularVelocityCovariance = other.angularVelocityCovariance;

            return *this;
        }
        // Function to update the shape centroid, rotation matrix.
        Shape* getShape(){
            return shape;
        }
        void updateShape(const Eigen::Vector3d& centroid, const Eigen::Matrix3d& rotationMatrix) {
            shape->setPose(centroid, rotationMatrix);
        }
        void updateRadius(const double r) {
            std::vector<double> param;
            shape->getShapeParameters(param);
            param[0]=r;
            shape->setShapeParameters(param);
        }
        void addRadius(const double r) {
            std::vector<double> param;
            shape->getShapeParameters(param);
            param[0]+=r;
            shape->setShapeParameters(param);
        }

        void addHeight(const double h) {
            std::vector<double> param;
            shape->getShapeParameters(param);
            param[1]+=h;
            shape->setShapeParameters(param);
        }
        // Function to update the velocity and angular velocity.
        void updateVelocity(const Eigen::Vector3d& velocity, const Eigen::Vector3d& angularVelocity) {
            this->velocity = velocity;
            this->angularVelocity = angularVelocity;
        }

        // Function to update the uncertainty.
        void updateUncertainty(double uncertainty) {
            this->uncertainty = uncertainty;
        }

        // Function to update the velocity covariance matrix.
        void updateVelocityCovariance(const Eigen::Matrix3d& velocityCovariance) {
            this->velocityCovariance = velocityCovariance;
        }

        // Function to update the angular velocity covariance matrix.
        void updateAngularVelocityCovariance(const Eigen::Matrix3d& angularVelocityCovariance) {
            this->angularVelocityCovariance = angularVelocityCovariance;
        }
        // Functions to get shape centroid, rotation matrix, velocity, angular velocity, uncertainty,
        // velocity covariance matrix, and angular velocity covariance matrix.
        void getPose(Eigen::Vector3d& centroid, Eigen::Matrix3d& rotationMatrix) const {
            shape->getPose(centroid, rotationMatrix);
        }
        double getRp(){
            return positionRadius;
        }
        double getRo(){
            return orientationRadius;
        }
        Eigen::Vector3d getCenter() const {
            Eigen::Vector3d centerPose;
            Eigen::Matrix3d rotationMatrix;
            shape->getPose(centerPose, rotationMatrix);
            return centerPose;
        }

        Eigen::Matrix3d getOrientation() const {
            Eigen::Vector3d centerPose;
            Eigen::Matrix3d rotationMatrix;
            shape->getPose(centerPose, rotationMatrix);
            return rotationMatrix;
        }
        Eigen::Vector3d getz() const {
            Eigen::Matrix3d rota = getOrientation();
            return rota.block<3,1>(0,2);
        }
        double getRadius(){
            std::vector<double> param;
            shape->getShapeParameters(param);
            return param[0];
        }
        double getHeight(){
            std::vector<double> param;
            shape->getShapeParameters(param);
            return param[1];
        }
        Eigen::Vector3d getVelocity() const {
            return velocity;
        }

        Eigen::Vector3d getAngularVelocity() const {
            return angularVelocity;
        }

        double getUncertainty() const {
            return uncertainty;
        }

        Eigen::Matrix3d getVelocityCovariance() const {
            return velocityCovariance;
        }

        Eigen::Matrix3d getAngularVelocityCovariance() const {
            return angularVelocityCovariance;
        }

        fcl::CollisionObjectd* getCollision(){
            return shape->toCollisionObject();
        }

        // State evolution function: updates the centroid position and rotation matrix of the shape
        // based on the velocity and angular velocity.
        void stateEvolution(double time) {
            Eigen::Vector3d centroid;
            Eigen::Matrix3d rotationMatrix;
            shape->getPose(centroid, rotationMatrix);

            Eigen::Vector3d translation = velocity * time;
            centroid += translation;

            double angle = angularVelocity.norm() * time;
            Eigen::Matrix3d rotationUpdate;
            rotationUpdate = Eigen::AngleAxisd(angle, angularVelocity.normalized());
            rotationMatrix = rotationUpdate * rotationMatrix;

            shape->setPose(centroid, rotationMatrix);
        }
        movingObs clone(){
            return movingObs(shape->clone(), velocity, angularVelocity,uncertainty, velocityCovariance, angularVelocityCovariance,positionCovariance, orientationCovariance, isFake);
        } 
        //update the posterior probability of the obs(need improvement)      
        void updateUncertainty(double time, bool isValidObservation) {
            // std::cout<<"In Uncertainty"<<std::endl;
            Gaussian3D positionGua(getCenter(), positionCovariance);
            // std::cout<<"getCenter"<<std::endl;
            Gaussian3D orientGau(getz(), orientationCovariance);
            // std::cout<<"getz"<<std::endl;
            int dispCoff=3;
            if(isValidObservation){
                double condition_cur_p=positionGua.probabilityInBall(getCenter(),positionRadius);
                double condition_cur_o=orientGau.probabilityInBall(getz(),orientationRadius);
                uncertainty=condition_cur_p*condition_cur_o;

            }
            else{
                uncertainty=positionGua.probabilityInBall(getCenter(),positionRadius)*orientGau.probabilityInBall(getz(),orientationRadius);
                //covariance diverges
                velocityCovariance=positionGua.updateVelocityCovariance(velocity,maxAcceleration*time*Eigen::Vector3d(1,1,1),velocityCovariance);
            }
        }

        // Static members for maximum acceleration, maximum angular velocity, and maximum velocity
        static double maxAcceleration;
        static double maxAngularVelocity;
        static double maxVelocity;

    private:
        Shape* shape;
        double positionRadius;
        double orientationRadius;
        Eigen::Vector3d velocity;
        Eigen::Vector3d angularVelocity;
        double uncertainty;
        bool isFake;
        Eigen::Matrix3d velocityCovariance;
        Eigen::Matrix3d angularVelocityCovariance;
        Eigen::Matrix3d positionCovariance;
        Eigen::Matrix3d orientationCovariance;
    };

    
} // namespace envelopeShape

#endif // MOVINGOBS_H

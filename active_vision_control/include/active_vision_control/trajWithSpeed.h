#ifndef _ROBOTTRAJ_H
#define _ROBOTTRAJ_H

#include <iostream>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <chrono>
#include <unistd.h>  // 包含用于 getcwd 函数的头文件
#include <vector>
#include <Eigen/Dense>
#define FCL_EXPORT
#include <fcl/fcl.h>
#include <fcl/narrowphase/collision.h>
#include <octomap/octomap.h>
#include <octomap/OcTree.h>
#include <octomap_msgs/Octomap.h>
#include <octomap_msgs/conversions.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <geometry_msgs/PoseStamped.h>
#include "active_vision_control/robot.h"
#include  <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>

#define PI 3.1415926

void RVIZ_Joint_Init(JointValue& joint);
void* Robot_State_Thread(void *threadid);

int sgn(double x);
//限制在0-2pi上的位形
class JAKArobot;

class robotConfig{
private:
    double jointAngle[6];
public:
    robotConfig(){
        for(int i=0;i<6;i++){
            jointAngle[i]=0;
        }
    }
    //取前六个
    robotConfig (std::vector<double>& jvalue){
        if(jvalue.size()<6) {
            std::cout<<"bad init"<<std::endl;
            for(int i=0;i<6;i++){
                jointAngle[i]=0;
            }
        }
        else{
            for(int i=0;i<6;i++){
                while(jvalue[i]>2*PI){
                    jvalue[i]-=2*PI;
                }
                while(jvalue[i]<0){
                    jvalue[i]+=2*PI;
                }
                jointAngle[i]=jvalue[i];
            }
        }
        
    }

    void setAngle(std::vector<double>& jvalue){
        if(jvalue.size()<6) {
            std::cout<<"bad set"<<std::endl;
        }
        else{
            for(int i=0;i<6;i++){
                while(jvalue[i]>2*PI){
                    jvalue[i]-=2*PI;
                }
                while(jvalue[i]<0){
                    jvalue[i]+=2*PI;
                }
                jointAngle[i]=jvalue[i];
            }
        }    
    }

    void setAngle(int id, double angle){
        if(id>6||id<0){
            std::cout<<"bad set"<<std::endl;
            return;
        }
        else{
            while(angle>2*PI){
                angle-=2*PI;
            }
            while(angle<0){
                angle+=2*PI;
            }
            jointAngle[id]=angle;
            
        }
    }

    std::vector<double> getAngle(){
        std::vector<double> ang;
        for(int i=0;i<6;i++){
            ang.push_back(jointAngle[i]);
        }
        return ang;
    }
    //从0开始
    double getAngle(int id){
        return jointAngle[id];
    }

    //C空间一范数
    double norm1(robotConfig);
    //C空间二范数计算
    double norm2(robotConfig );
    //C空间偏置(绝对值)
    robotConfig displacement(robotConfig);
    //C空间减法(带正负号)
    std::vector<double> substract(robotConfig);
};


//位形空间轨迹
class robotTraj{
private:
    std::vector<robotConfig> jtraj;
    int endPath;
public:
    robotTraj(){endPath=7;jtraj.clear();};
    robotTraj(std::vector<robotConfig>& jt){jtraj=jt;endPath=7;}
    robotTraj(robotConfig c1, robotConfig c2){jtraj.clear();jtraj.push_back(c1); jtraj.push_back(c2);endPath=7;}
    void setTraj(std::vector<robotConfig>& jt){
        jtraj=jt;
    }
    void addTraj(robotConfig rc){
        jtraj.push_back(rc);
    }
    void addFrontTraj(robotConfig rc){
        jtraj.insert(jtraj.begin(),rc);
    }
    void clearTraj(){
        jtraj.clear();
    }
    int sizeOfTraj(){
        return jtraj.size();
    }
    std::vector<robotConfig> getTraj(){
        return jtraj;
    }
    robotConfig getStart(){
        return jtraj[0];
    }
    robotConfig getTerminal(){
        return jtraj[jtraj.size()-1];
    }
    //根据frame进行剪裁，依赖于两点之间范数距离
    std::vector<robotConfig>CuttingTo(double);
    //以一个统一标准进行剪裁，即轨迹插补
    std::vector<robotConfig>Diliting();
    //根据frame进行剪裁，依赖于两点之间范数距离，带有速度规划
    std::vector<robotConfig>SmoothCuttingTo(double);
    //以一个统一标准进行剪裁，即轨迹插补，带有速度规划
    std::vector<robotConfig>SmoothDiliting(double );
    //笛卡尔空间轨迹生成
    std::vector<robotConfig>Cart_CuttingTo(double, JAKArobot*);
    //渐慢插补
    std::vector<robotConfig>Slow_CuttingTo(double);

    //三次样条裁剪
    std::vector<robotConfig>Cutting3To(double);
    //以一个统一标准进行三次样条裁剪，即轨迹插补
    std::vector<robotConfig>Diliting3();
    //三次样条渐慢插补
    std::vector<robotConfig>Slow_Cutting3To(double);

};

//三次样条类
class CubicSplineInterpolation {
public:
    CubicSplineInterpolation(robotTraj data);
    double Interpolate(double x, int angleId);
    std::vector<robotConfig> GetInterpolatedPoints(double step_v);
    std::vector<robotConfig> GetTrapInterpolatedPoints(double step_v);

private:
    struct Coefficients {
        double a[6];
        double b[6];
        double c[6];
        double d[6];
    };
    std::vector<std::array<double, 6>> processInWhole;
    robotTraj data_;
    std::vector<Coefficients> coeffs_;
    int n_;
    double pubTime = 8e-3;

    void ComputeCoefficients();

    double angleSub(double suber, double subee);
};


//机器人控制类，包括仿真和实际机器人控制
class JAKArobot{
private:
    moveit::planning_interface::MoveGroupInterface *arm_;
    robot_state::RobotStatePtr kinematic_state;
    const robot_state::JointModelGroup *joint_model_group;
    ros::NodeHandle nodeh;
    bool isSimulator;
    std::vector<double>radius;
    std::vector<double>height;
    robotConfig curConfig;
public:
    JAKArobot(ros::NodeHandle &nh);
    JAKArobot(ros::NodeHandle &nh,moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP);
    JAKArobot(ros::NodeHandle &nh,moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP,const char*robotip);
    ~JAKArobot(){}

    void setRobot(moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP){
        this->arm_=&arm;
        kinematic_state=arm_->getCurrentState();
        joint_model_group=arm_->getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    }
    moveit::planning_interface::MoveGroupInterface* getarm(){return arm_;}
    robotConfig getConfig(){
        std::vector<double> current_joint_values = arm_->getCurrentJointValues();
        return robotConfig(current_joint_values);
    }
    void updateCurConfig(){
        curConfig=getConfig();
    }
    Eigen::Matrix4d getEnd(){
        Eigen::Matrix4d tranf;
        tranf=forwardKineEnd(getConfig());
        return tranf;
    }
    Eigen::Matrix4d forwardKineEnd(robotConfig jointValue){
        kinematic_state->setJointGroupPositions(joint_model_group,  jointValue.getAngle());
        const Eigen::Affine3d & Link6pos = kinematic_state->getGlobalLinkTransform("link_6");
        Eigen::Matrix4d endpose= Link6pos.matrix();
        return endpose;
    }
    //获得某个位形下的圆柱中心点坐标系
    std::vector<geometry_msgs::PoseStamped> axis_pose(robotConfig );
    //获得某个位形下的碰撞包络
    std::vector<fcl::CollisionObjectd> forwardKine(robotConfig );
    //逆运动学，以某个位形进行诱导
    //需要保证无解的情况下输出无解
    robotConfig inverseKine(Eigen::Matrix4d ,robotConfig, bool &);
    //获得现在的碰撞包络
    std::vector<fcl::CollisionObjectd> getCover();

    //真实机械臂上使能
    void enableRobot();
    //真实机械臂下使能
    void forbidRobot();
    //获得传感器得到的角度值
    void getCurrentJointValue(JointValue &);
    //moveit发布joint_msg
    void pubJointState(sensor_msgs::JointState );
    //发布现在的jointStates,避免moveit宕机
    void pubCurJointStates();
    //发布轨迹话题，主动视觉接口
    void pubTraj(std_msgs::Float64MultiArray );
    //servoj
    void servojoint(JointValue &joint_goal);

    //保持静止，调试用
    void keepStatic(robotConfig );
    //位形空间伺服运动，自带延迟
    void singleExecute(robotConfig );
    //笛卡尔空间伺服运动，存在多解，待测试
    void singleExecute(Eigen::Matrix4d , robotConfig);
    //顺滑轨迹执行
    void smoothTraj(robotTraj, double speed =1.0);
    //主动视觉顺滑轨迹
    void active_smoothTraj(robotTraj, robotTraj, double speed =1.0);
    //执行轨迹（弃用）
    void executeTraj(robotTraj, double speed =1.0);
    //servoj指令
    void goToConf(robotConfig, double speed =1.0);
    //gohome指令-回到预设点
    void gohome(double speed =1.0);
};

class collideJudge{
private:
    JAKArobot* jrob;
    octomap::OcTree* blockMap;
    std::vector<fcl::CollisionObject<double>*> boxes;

public:
    collideJudge(){}
    collideJudge(JAKArobot* jr, octomap::OcTree* bm):jrob(jr),blockMap(bm){}
    void generateBoxesFromOctomap(fcl::OcTree<double>& tree);
    void setJAKA(JAKArobot* jr){jrob=jr;}
    void setObs(octomap::OcTree* bm){
        if(bm!=nullptr){
            blockMap=bm;
            auto octree = std::shared_ptr<const octomap::OcTree>(blockMap);
            fcl::OcTree<double>* tree = new fcl::OcTree<double>(octree);
            generateBoxesFromOctomap(*tree);
        }
    }
    bool collideCheck (const fcl::CollisionObjectd& obj);
    JAKArobot* getRobot(){return jrob;}
    octomap::OcTree* getObs(){return blockMap;}
    //姿态碰撞检测，有障碍物输出false
    bool collideLogical(robotConfig tarpose, bool isDis=false);
    //路径碰撞检测，有障碍物输出false
    bool collideTraj(robotTraj mytraj, bool isDis=false);
};
//虚假jointState发布线程
void* Fake_State_Thread(void* rob);


#endif
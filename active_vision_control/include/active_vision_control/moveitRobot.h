#ifndef _MOVEITROBOT_H
#define _MOVEITROBOT_H

#include <iostream>
#include <Eigen/Dense>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
//机器人圆柱包络类，以圆柱中心为原点，z轴沿着母线
class robotCylinder{
private:
    double radius;
    double height;
    Eigen::Matrix3d rotation;
    Eigen::Vector3d center_trans;
public:
    robotCylinder(double rr, double hh):radius(rr),height(hh){}
    robotCylinder(double rr, double hh,Eigen::Matrix3d mm,Eigen::Vector3d cc):radius(rr),height(hh),rotation(mm),center_trans(cc){}
    ~robotCylinder(){}

    double getRadius(){return radius;}
    double getHeight(){return height;}
    
    Eigen::Matrix3d getRotation(){return rotation;}
    Eigen::Vector3d getCenter_trans(){return center_trans;}

    void setRotation(Eigen::Matrix3d mm){rotation=mm;}
    void setTranslation(Eigen::Vector3d cc){center_trans=cc;}
};

//机器人包络类
class MoveitRobot{
private:
    moveit::planning_interface::MoveGroupInterface *arm_;
    robot_state::RobotStatePtr kinematic_state;
    const robot_state::JointModelGroup *joint_model_group;
    std::vector<robotCylinder> robotCover;

    std::vector<double>radius;
    std::vector<double>height;

public:
    MoveitRobot(){radius={0.065,0.07,0.06,0.06,0.05,0.03};height={0.2,0.425,0.24,0.15,0.21,0.18};}
    MoveitRobot(robot_state::RobotStatePtr ,const robot_state::JointModelGroup * ,moveit::planning_interface::MoveGroupInterface &arm);
    MoveitRobot(moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP);
    ~MoveitRobot(){}

    
    void setRobot(moveit::planning_interface::MoveGroupInterface &arm,const std::string &PLANNING_GROUP){
        this->arm_=&arm;
        kinematic_state=arm_->getCurrentState();
        joint_model_group=arm_->getCurrentState()->getJointModelGroup(PLANNING_GROUP);
    }
    
    /*
    得到这种joint state下的机器人包络
    @param joint state六个角度值
    @return 六个机器人cylindar包络
    */
    std::vector<robotCylinder> forwardKine(std::vector<double>& );
    /*
    得到这种joint state下的机器人末端位姿
    @param joint state六个角度值
    @return 末端在世界坐标系下齐次变换矩阵
    */
    Eigen::Matrix4d forwordKineEnd(std::vector<double>&);
    /*
    生成现在机器人状态下的圆柱包络
    */
    void genCurrentCover();
    std::vector<robotCylinder> getCover(){genCurrentCover();return robotCover;}
    

};
#endif
#ifndef _ACTIVEVISON_H
#define _ACTIVEVISON_H

#include <iostream>
#include <thread>
#include <ros/ros.h>
#include <std_msgs/Float64.h>

#include <octomap/octomap.h>
#include <octomap/OcTree.h>
#include <octomap_msgs/Octomap.h>
#include <octomap/OcTreeBaseImpl.h>
#include <octomap_msgs/conversions.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include "active_vision_control/moveitRobot.h"
#include "active_vision_control/SCServo.h"
class ActiveVision;
class VisionDecision;

//主动视觉机构角度状态
class servoState{
private:
    double servo1angle;
    double servo2angle;
    //rad
    //constrain in [0,2pi]
public:
    servoState(){};
    servoState(double ang1,double ang2){
        while(ang1>2*3.1415926){
            ang1-=2*3.1415926;
        }
        while(ang1<0){
            ang1+=2*3.1415926;
        }
        while(ang2>2*3.1415926){
            ang2-=2*3.1415926;
        }
        while(ang2<0){
            ang2+=2*3.1415926;
        }
        servo1angle=ang1;
        servo2angle=ang2;
    }
    
    void setAngle1(double ang1){
        while(ang1>2*3.1415926){
            ang1-=2*3.1415926;
        }
        while(ang1<0){
            ang1+=2*3.1415926;
        }
        servo1angle=ang1;
    }
    void setAngle2(double ang2){
        while(ang2>2*3.1415926){
            ang2-=2*3.1415926;
        }
        while(ang2<0){
            ang2+=2*3.1415926;
        }
        servo2angle=ang2;
    }
    servoState(const servoState& other) {
        servo1angle = other.servo1angle;
        servo2angle = other.servo2angle;
    }
    double getAngle1(){return servo1angle;}
    double getAngle2(){return servo2angle;}
    /*
    State空间下的二范数计算
    */
    double disCost(servoState);
    /*
    State空间下的减法计算
    */
    std::vector<double> substract(servoState otherConf){
        std::vector<double> res;

        double deltaAngle1=servo1angle-otherConf.servo1angle;
        if(deltaAngle1<-M_PI)deltaAngle1=2*M_PI+deltaAngle1;
        if(deltaAngle1>M_PI)deltaAngle1=-(2*M_PI-deltaAngle1);
        res.push_back(deltaAngle1);

        double deltaAngle2=servo2angle-otherConf.servo2angle;
        if(deltaAngle2<-M_PI)deltaAngle2=2*M_PI+deltaAngle2;
        if(deltaAngle2>M_PI)deltaAngle2=-(2*M_PI-deltaAngle2);
        res.push_back(deltaAngle2);

        return res;
    }
};
/*
    链接Octomap的代价栅格地图
*/
class costBlock{
private:
    octomap::OcTree* ocTree;
    double directionProb[4];//0代表x正，1代表x负，2代表y正，3代表y负
    double resolution;//一个格点的大小
    double height;//为上下
    double width;//为左右
    double length; //为前后
    //单位全是米
    double costCoff[4];//0代表轨迹，1代表外围，2代表方向，3代表障碍
    //ActiveVision visionDevice;
public:
    costBlock(){
        resolution=0.1;
        height=1;
        width=5.6;
        length=5.6;
        for(int i=0;i<4;i++)directionProb[i]=0;

        costCoff[0]=0.4;
        costCoff[1]=0.1;
        costCoff[2]=0;
        costCoff[3]=0.5;
        ocTree=new octomap::OcTree(0.1);
       }
    costBlock(octomap::OcTree* ocT){
        resolution=0.1;
        height=1;
        width=5.6;
        length=5.6;
        for(int i=0;i<4;i++)directionProb[i]=0;

        costCoff[0]=0.4;
        costCoff[1]=0.1;
        costCoff[2]=0;
        costCoff[3]=0.5;
        ocTree=ocT;
        }
    void setResolution(double res){resolution=res;}
    void setHeight(double hh){height=hh;}
    void setWidth(double ww){width=ww;}
    void setLength(double ll){ length=ll; }
    void setOcT(octomap::OcTree* ocT){ocTree=ocT;}
    /*
    判断是否遮挡
    */
    bool isBlocked(int i, int j, int k,servoState nextView,std::vector<robotCylinder>&, ActiveVision *);
    /*
    3个障碍物中心位置
    */
    std::vector<Eigen::Vector3d> blockCoff();
    /*
    这种状态下的全部cost计算
    */
   double costOfState(servoState, std::vector<robotCylinder>,std::vector<Eigen::Vector3d> &Traj, ActiveVision *);
    /*
    对于键值是(i,j,k)的格点计算cost
    */
    double costOfBlock(int i, int j, int k,servoState nextView,std::vector<robotCylinder>,std::vector<Eigen::Vector3d> &Traj,ActiveVision *);
};
/*
    视觉决策类
    */
class VisionDecision{
private:
    std::vector<servoState> actionOfWatchTraj;//轨迹观察视点
    std::vector<servoState> actionOfWatchMoving;//动态障碍物观察视点
    std::vector<servoState> actionOfSearching;//未知环境观察视点
    int decisionChoices;//共有几个决策方案

public:
    VisionDecision(){decisionChoices=3;}
    ~VisionDecision(){}

    std::vector<servoState> getTrajState(){return actionOfWatchTraj;}
    std::vector<servoState> getMovingState(){return actionOfWatchMoving;}
    std::vector<servoState> getSearchingState(){return actionOfSearching;}
    /*
    生成轨迹决策
    */
    void genTrajAction(ActiveVision *,std::vector<Eigen::Vector3d> &Traj);
    /*
    生成移动障碍物决策
    */
    void genMovingAction(ActiveVision *);
    /*
    生成位置区域决策（暂不开放）
    */
    void genSerchingAction(ActiveVision *);

};
/*
    主动视觉轨迹类
    */
class visionTraj{
private:
    std::vector<servoState> jtraj;
public:
    visionTraj(){};
    visionTraj(std::vector<servoState>& jt){jtraj=jt;}
    visionTraj(servoState c1, servoState c2){jtraj.clear();jtraj.push_back(c1); jtraj.push_back(c2);}
    void setTraj(std::vector<servoState>& jt){
        jtraj=jt;
    }
    void addTraj(servoState rc){
        jtraj.push_back(rc);
    }
    void addFrontTraj(servoState rc){
        jtraj.insert(jtraj.begin(),rc);
    }
    void clearTraj(){
        jtraj.clear();
    }
    int sizeOfTraj(){
        return jtraj.size();
    }
    std::vector<servoState> getTraj(){
        return jtraj;
    }
    servoState getStart(){
        return jtraj[0];
    }
    servoState getTerminal(){
        return jtraj[jtraj.size()-1];
    }
    //根据frame进行剪裁，依赖于两点之间范数距离
    std::vector<servoState>CuttingTo(double);
    //以一个统一标准进行剪裁，即轨迹插补
    std::vector<servoState>Diliting();
};
class ftservo{
private:
    SMS_STS sm_st;
    uint8_t ID[4] = {1, 2, 3, 4};
    uint8_t rxPacket[4];
    int16_t Position[4] = {0, 0, 0, 0};
    int16_t Speed[4] = {0, 0, 0, 0};
    int Constrain_speed = 1100;

    int angle_trans(double realangle) {
        int res = realangle * 2048 / 180;
        return res;
    }
    double trans_back(int step){
        double ang = double(step)*180.0/2048.0;
        return ang;
    }
    void updatePosVel() {
        sm_st.syncReadPacketTx(ID, sizeof(ID), SMS_STS_PRESENT_POSITION_L, sizeof(rxPacket));
        for (uint8_t i = 0; i < sizeof(ID); i++) {
            if (!sm_st.syncReadPacketRx(ID[i], rxPacket)) {
                std::cout << "ID:" << (int) ID[i] << " sync read error!" << std::endl;
                continue;
            }
            Position[i] = sm_st.syncReadRxPacketToWrod(15);
            Speed[i] = sm_st.syncReadRxPacketToWrod(15);
            std::cout << "ID:" << int(ID[i]) << " Position:" << Position[i] << " Speed:" << Speed[i] << std::endl;
        }
    }

    
public:
    ftservo(int argc, char **argv){
        if (argc < 2) {
            std::cout << "argc error!" << std::endl;
            return;
        }
        if (!sm_st.begin(1000000, argv[1])) {
            std::cout << "Failed to init sms/sts motor!" << std::endl;
            return;
        }
        sm_st.WritePosEx(1, 2048, Constrain_speed, 50);
        sm_st.WritePosEx(2, 0, Constrain_speed, 50);
        sm_st.WritePosEx(3, 4096, Constrain_speed, 50);
        sm_st.WritePosEx(4, 0, Constrain_speed, 50);
    }
    double read_angle(int id){
        int stp = sm_st.ReadPos(id); 
        double ang;
        if(id == 1){
            ang = -trans_back(stp-2048);
        }
        else if(id == 2){
            ang = trans_back(stp);
        }
        else if(id == 3){
            ang = -trans_back(stp-4096);
        }
        else if(id == 4){
            ang = trans_back(stp);
        }
        return ang;
    }
    
    void servoControl(double angle, int id){
        int servo_id = id;
        if(id == 1){
            if(angle>180){
                angle = angle-360;
            }
            int step=-angle_trans(angle);
            sm_st.WritePosEx(1, step+2048, Constrain_speed, 50);
        }
        else if(id ==2){
            int step=angle_trans(angle);
		    sm_st.WritePosEx(2, step, Constrain_speed, 50);

        }
        else if(id ==3){
            int step=-angle_trans(angle);
		    sm_st.WritePosEx(3, 4096+step, Constrain_speed, 50);

        }
        else if (id ==4){
            int step=angle_trans(angle);
		    sm_st.WritePosEx(4, step, Constrain_speed, 50);

        }
        
        //read_angle();
    }
    void servoControl(std::vector<double> servoangle){
        if(servoangle.size()!=4){
            std::cout<<"Wrong input of servoangle"<<std::endl;
            return;
        }
        for(int i=0; i<4;i++){
            servoControl(servoangle[i], i+1);
        }
        //read_angle();
    }
    
};

/*
    主动视觉机构类
    */
class ActiveVision{
private:
    servoState visionState;
    costBlock* blockMap;
    VisionDecision *myDecision;
    ftservo* servo;
    ros::NodeHandle nh;
    /*
    三个建模参数
    */
    double l1;
    double l2;
    double l3;
    /*
    视觉张角（视野范围）
    */
    double visionAngle;//rad
    /*
    视野深度
    */
    double visionDepth;
    Eigen::Vector3d base;//主动视觉基坐标系在世界坐标系下的位置
    int id;
    Eigen::Vector2d delta_s;

    std::thread updateThread;

public:
    ActiveVision(servoState ss):visionState(ss),base(Eigen::Vector3d(0,-0.5,-0.8)),id(1){blockMap=new costBlock;l1=0.069;l2=0.02;l3=0.061;
                            visionAngle=2.0/3.0*3.14159;visionDepth=1.2;myDecision=new VisionDecision;delta_s = Eigen::Vector2d(0,0);}
    ActiveVision(servoState ss, Eigen::Vector3d bb,int ID):visionState(ss),base(bb),id(ID){blockMap=new costBlock;l1=0.069;l2=0.02;l3=0.061;visionAngle=2.0/3.0*3.14159;visionDepth=1.2;myDecision=new VisionDecision;delta_s = Eigen::Vector2d(0,0);}
    ActiveVision(servoState ss, ros::NodeHandle& _nh);
    ActiveVision(servoState ss, Eigen::Vector3d bb,int ID, ros::NodeHandle& _nh);
    ActiveVision(servoState ss, Eigen::Vector3d bb,int ID, ros::NodeHandle& _nh, int argc, char** argv);
    //适用于飞特舵机的参数
    ~ActiveVision(){if (updateThread.joinable()) {
            updateThread.join();
        }}

    double getVisionDepth(){return visionDepth;}
    double getVisionAngle(){return visionAngle;}
    int getId(){return id;}
    costBlock* getBlockMap(){return blockMap;}
    void setVisionState(servoState ss){visionState=ss;}
    void setVisionDecision(VisionDecision *vv){myDecision=vv;}
    servoState getVisionState(){return visionState;}
    VisionDecision* getVisionDecision(){return myDecision;}
    double getl1(){return l1;}
    void setl1(double l1_){l1 = l1_;}
    void setl2(double l2_){l2 = l2_;}
    void setl3(double l3_){l3 = l3_;}
    void setDeltas(Eigen::Vector2d ds){delta_s = ds;}
    Eigen::Vector3d getBase(){return base;}
    void updateBlockMap(costBlock* curMap){blockMap=curMap;}
    void initservo(int argc, char** argv){servo = new ftservo(argc, argv);}
    /*
    广播TF线程
    */
    void broadCastThreadFun();
    /*
    广播现在自己的状态
    */
    void broadcastTF();
    //主动视觉运动学
    /*
    主动视觉正运动学
    @param 目标状态
    @return 末端在世界坐标系下变换矩阵
    */
    Eigen::Matrix4d visionForwardKine(servoState ss);
    /*
    主动视觉正运动学,以自身状态为输入
    @return 末端在世界坐标系下变换矩阵
    */
    Eigen::Matrix4d visionForwardKine(){return visionForwardKine(visionState);}
    /*
    主动视觉逆运动学
    @param 跟踪的三维世界点
    @return 目标状态
    */
    servoState visionInverseKine(Eigen::Vector3d endPoint);
    /*
    获得下一个视点的优化函数值
    @param 下一个状态
    @param 下一个机器人位置表征
    @param 机器人规划路径
    @return 目标状态
    */
    double getCost(servoState nextView,  std::vector<robotCylinder>&,std::vector<Eigen::Vector3d> &Traj);
    /*
    主动视觉机构全部的接口，返回各个action的Profit，在eachstate中存放各个Decision最好的state
    @param 机器人规划路径
    @param 下一个机器人位置表征
    @param 八叉树地图
    @param 引用，用于返回决策对应的状态
    @return 各个决策对应的profit
    */
    std::vector<double>profitOfEachDecision(std::vector<Eigen::Vector3d> &Traj, std::vector<robotCylinder>&, octomap::OcTree* preMap,std::vector<servoState> &eachState);
    /*
    控制接口，使用内部PID算法轨迹生成
    @param 目标位置
    */
    void moveCameraTo(servoState);
    /*
    初始化
    */
    void initialState();
    /*
    控制接口，用于L1速度下的轨迹生成
    @param 轨迹执行
    @param 执行速度,1.0相当于1s走90度
    */
    void executeVisionTraj(visionTraj myVisionTraj, double speed = 1.0);
    /*
    控制接口，用于L1速度下的轨迹生成
    @param 目标位置
    @param 执行速度,1.0相当于1s走90度
    */
    void goToState(servoState targetState, double speed = 1.0);

};





#endif
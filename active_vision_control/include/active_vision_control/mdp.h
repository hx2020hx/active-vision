#ifndef MDP_H
#define MDP_H

#include <vector>
#include <fstream>
#include "activeVision.h"
#include "movingObs.h"
#include "safeCircle.h"
#include "moveitRobot.h"


class MDP {
public:
    // Constructor
    MDP(){timeDivide=5;gamma=0.8;alpha=0.01;}
    // set
    void setRobotTraj(std::vector<Eigen::Vector3d> pt){plannedTraj=pt;}
    void setWholeTime(double wt){wholeTime=wt;}
    void addObs(envelopeShape::movingObs mo){obsGroup.push_back(mo);}
    void clearObs(){
        // for(auto obs:obsGroup){
        //     delete obs;
        // }
        obsGroup.clear();
        }
    int sizeOfObs(){return obsGroup.size();}
    // void setMoveitRobot(JAKArobot* mr){myrob=mr;}
    void setSafeCircle(safeCircle* sc){safeSpace=sc;}
    //get
    std::vector<Eigen::Vector3d> getRobotTraj(){return plannedTraj;}
    double getWholeTime(){return wholeTime;}
    std::vector<envelopeShape::movingObs >getObs(){return obsGroup;}
    safeCircle* getSafeCircle(){return safeSpace;}
    // JAKArobot* getRobot(){return myrob;}
    //get time interval servoState
    std::vector<std::vector<servoState>> genMarkovState();
    //get 'step' time the collisionProb of robot to all obs
    // double getCollisionProb(servoState ,int ,std::vector<envelopeShape::movingObs>);
    double getCollisionProb(servoState ,int, Eigen::Vector3d ,std::vector<envelopeShape::movingObs>);
    //is this servoState a valid observation
    bool isValid(servoState, envelopeShape::movingObs);
    //interface: solve the MDP problem
    std::vector<servoState> solveMDP();

private:
    int timeDivide;
    double gamma;
    double alpha;
    std::vector<Eigen::Vector3d> plannedTraj;
    std::vector<Eigen::Vector3d> Trajp;
    // std::vector<robotConfig> Trajj;
    double wholeTime;
    std::vector<envelopeShape::movingObs >obsGroup;
    safeCircle* safeSpace;
    //JAKArobot* myrob;

};

#endif // MDP_H

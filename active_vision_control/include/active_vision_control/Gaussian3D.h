#ifndef GAUSSIAN3D_H
#define GAUSSIAN3D_H

#include <Eigen/Dense>
#include <vector>

class Gaussian3D {
private:
    Eigen::VectorXd mean;
    Eigen::MatrixXd covariance;

public:
    Gaussian3D(const Eigen::VectorXd& mean, const Eigen::MatrixXd& covariance);

    void setMean(const Eigen::VectorXd& mean);
    void setCovariance(const Eigen::MatrixXd& covariance);
    Eigen::VectorXd getMean() const;
    Eigen::MatrixXd getCovariance() const;

    double probabilityInBall(const Eigen::VectorXd& point, double radius) const;
    double probabilityInTwoBall(const Eigen::VectorXd& point1, const Eigen::VectorXd& point2,double radius) const;
    Eigen::MatrixXd updateVelocityCovariance(const Eigen::VectorXd& velocity,
                                         const Eigen::VectorXd& delta_velocity,
                                         const Eigen::MatrixXd& covariance_matrix);
};

#endif // GAUSSIAN3D_H

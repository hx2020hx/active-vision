/*
舵机出厂速度单位是0.0146rpm，速度改为V=2400
*/

#include <iostream>
#include <ros/ros.h>
#include "SCServo.h"

SMS_STS sm_st;

int angle_trans(double realangle){
	int res=realangle*2048/180;
	return res;
}
int main(int argc, char **argv)
{
	ros::init(argc,argv,"aeb_node");
  	ros::NodeHandle nh("~");
	std::string port;
	double angle1;
	double angle2;
	double angle3;
	double angle4;
	nh.getParam("port",port);
	nh.getParam("servo1angle",angle1);
	nh.getParam("servo2angle",angle2);
	nh.getParam("servo3angle",angle3);
	nh.getParam("servo4angle",angle4);
	const char* myPort = port.c_str();
    if(!sm_st.begin(100000, myPort)){
        std::cout<<"Failed to init sms/sts motor!"<<std::endl;
        return 0;
    }

	
	// while(1){
	// 	sm_st.WritePosEx(1, 4095, 2400, 50);//舵机(ID1)以最高速度V=2400(步/秒)，加速度A=50(50*100步/秒^2)，运行至P1=4095位置
	// 	std::cout<<"pos = "<<4095<<std::endl;
	// 	usleep(2187*1000);//[(P1-P0)/V]*1000+[V/(A*100)]*1000
  
	// 	sm_st.WritePosEx(1, 0, 2400, 50);//舵机(ID1)以最高速度V=2400(步/秒)，加速度A=50(50*100步/秒^2)，运行至P0=0位置
	// 	std::cout<<"pos = "<<0<<std::endl;
	// 	usleep(2187*1000);//[(P1-P0)/V]*1000+[V/(A*100)]*1000
	// }


	int step1=2048-angle_trans(angle1);
	int step2=2048-angle_trans(angle2);
	int step3=4096-angle_trans(angle3);
	int step4=angle_trans(angle4);

	sm_st.WritePosEx(1, step1, 2400, 50);
	sm_st.WritePosEx(2, step2, 2400, 50);
	sm_st.WritePosEx(3, step3, 2400, 50);
	sm_st.WritePosEx(4, step4, 2400, 50);
	//sm_st.WritePosEx(3, 1024, 2400, 50);
	//sm_st.WritePosEx(4, 1024, 2400, 50);
	sm_st.end();
	return 0;
}

